from nutils_pbox.THB_coarse_refine_functions import *
import numpy as np
import unittest

class TestRefinementCoarsening(unittest.TestCase):
    def test_multi_level_support(self):
        nelems = [2,2]
        degree = [2,2]

        level = 4
        # [32, 32] level 4 elements
        elem_list = [0, 15*32+15]

        support_levels = [2,4]

        # individual solutions
        sol = [   [[[0, 0], [1, 0], [2, 0], # solutions elem = 0, support level 2
                    [0, 1], [1, 1], [2, 1],
                    [0, 2], [1, 2], [2, 2]],
                   [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1],  # solutions elem = 15*32+15, support level 2
                    [1, 2], [2, 2], [3, 2], [4, 2], [5, 2],
                    [1, 3], [2, 3], [3, 3], [4, 3], [5, 3],
                    [1, 4], [2, 4], [3, 4], [4, 4], [5, 4],
                    [1, 5], [2, 5], [3, 5], [4, 5], [5, 5]]],
                   [[[0, 0], [1, 0], [2, 0],  # solutions elem = 0, support level 4
                     [0, 1], [1, 1], [2, 1],
                     [0, 2], [1, 2], [2, 2]],
                    [[13, 13], [14, 13], [15, 13], [16, 13], [17, 13],  # solutions elem = 15*32+15, support level 4
                     [13, 14], [14, 14], [15, 14], [16, 14], [17, 14],
                     [13, 15], [14, 15], [15, 15], [16, 15], [17, 15],
                     [13, 16], [14, 16], [15, 16], [16, 16], [17, 16],
                     [13, 17], [14, 17], [15, 17], [16, 17], [17, 17]]] ]

        for j, sup_level in enumerate(support_levels):
            for i, elem in enumerate(elem_list):
                support = multi_level_support_list(nelems, degree, [elem], level, sup_level)
                assert numpy.array_equal(support, numpy.array(sol[j][i]))

        # combined solutions
        sol = [    [[0, 0], [1, 0], [2, 0], # solutions support level 2
                    [0, 1], [1, 1], [2, 1],
                    [0, 2], [1, 2], [2, 2],
                    [1, 1], [2, 1], [3, 1], [4, 1], [5, 1],
                    [1, 2], [2, 2], [3, 2], [4, 2], [5, 2],
                    [1, 3], [2, 3], [3, 3], [4, 3], [5, 3],
                    [1, 4], [2, 4], [3, 4], [4, 4], [5, 4],
                    [1, 5], [2, 5], [3, 5], [4, 5], [5, 5]],
                   [[0, 0], [1, 0], [2, 0],  # solutions support level 4
                     [0, 1], [1, 1], [2, 1],
                     [0, 2], [1, 2], [2, 2],
                     [13, 13], [14, 13], [15, 13], [16, 13], [17, 13],
                     [13, 14], [14, 14], [15, 14], [16, 14], [17, 14],
                     [13, 15], [14, 15], [15, 15], [16, 15], [17, 15],
                     [13, 16], [14, 16], [15, 16], [16, 16], [17, 16],
                     [13, 17], [14, 17], [15, 17], [16, 17], [17, 17]] ]

        for j, sup_level in enumerate(support_levels):
            support = multi_level_support_list(nelems, degree, elem_list, level, sup_level)
            assert numpy.array_equal(support, numpy.array(sol[j]))

    def test_refinement_neigh_list(self):
        nelems = [2,2]
        degree = [2,2]

        level = 4
        # [32, 32] level 4 elements
        elem_list = [0, 15*32+15]

        admissibility_class = [2,4]

        sol = [    [[0, 1, 16, 17],
                    [102, 103, 104, 118, 119, 120, 134, 135, 136]] ,
                   [[0, 1, 4, 5],
                    [0, 1, 2, 4, 5, 6, 8, 9, 10]] ]

        for j, m in enumerate(admissibility_class):
            for i, elem in enumerate(elem_list):
                neigh = refinement_neigh_list(nelems, degree, [elem], level, m)
                assert numpy.array_equal(neigh, sol[j][i])

        sol = [[0, 1, 16, 17, 102, 103, 104, 118, 119, 120, 134, 135, 136],
               [0, 1, 2, 4, 5, 6, 8, 9, 10]]

        for j, m in enumerate(admissibility_class):
            neigh = refinement_neigh_list(nelems, degree, elem_list, level, m)
            assert numpy.array_equal(neigh, sol[j])

    def test_periodic_refinement(self):
        nelems = (3,3)
        degree = (1,1)

        # start with only bottom left element refined
        indices_per_level = [np.array([1,2,3,4,5,6,7,8]),np.array([0,1,6,7])]
        offsets = np.array([0,8,12])

        marked_elements = [8]
        m=2

        periodic_trials = [(),(0,),(1,),(0,1)]
        sol = [ [np.array([1, 2, 3, 4, 5, 6, 7, 8]),np.array([1, 6, 7]),np.array([ 0,  1, 12, 13])] ,
                [np.array([1, 2, 3, 4, 5, 7, 8]),np.array([ 1,  6,  7, 24, 25, 30, 31]),np.array([ 0,  1, 12, 13])],
                [np.array([1, 3, 4, 5, 6, 7, 8]),np.array([ 1,  4,  5,  6,  7, 10, 11]),np.array([ 0,  1, 12, 13])],
                [np.array([1, 3, 4, 5, 7]),np.array([ 1,  4,  5,  6,  7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]),np.array([ 0,  1, 12, 13])]]

        for i,periodic in enumerate(periodic_trials):

            indices_per_level_ref = refine(indices_per_level, offsets, nelems, degree, marked_elements, m, periodic=periodic)
            assert all([ np.array_equal(sol[i][j], indices_per_level_ref_j ) for j, indices_per_level_ref_j in enumerate(indices_per_level_ref) ])

    def test_periodic_coarsening(self):
        nelems = (3, 3)
        degree = (1, 1)

        # start with only bottom left element twice refined with periodicity in both directions
        indices_per_level = [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])]
        offsets = np.array([0, 5, 20, 23])

        # try to coarsen the following elements
        marked_elements_TL = [6,7,10,11]
        marked_elements_BR = [12,13,16,17]
        marked_elements_TR = [14,15,18,19]
        m = 2
        periodic_trials = [(),(0,),(1,),(0,1)]

        sol_TL= [ [np.array([1, 2, 3, 4, 5, 7]), np.array([1, 6, 7, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])] ,
                [np.array([1, 2, 3, 4, 5, 7]), np.array([1, 6, 7, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])],
                [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])],
                [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])]]

        sol_BR = [[np.array([1, 3, 4, 5, 6, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 28, 29, 34, 35]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 6, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 28, 29, 34, 35]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])]]

        sol_TR = [[np.array([1, 3, 4, 5, 7, 8]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 30, 31]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 7, 8]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 30, 31]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 7, 8]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 30, 31]), np.array([0, 1, 12, 13])],
                  [np.array([1, 3, 4, 5, 7]), np.array([1, 4, 5, 6, 7, 10, 11, 24, 25, 28, 29, 30, 31, 34, 35]), np.array([0, 1, 12, 13])]]


        for marked_elements, sol in zip([marked_elements_TL, marked_elements_BR,marked_elements_TR],[sol_TL,sol_BR,sol_TR]):
            for i, periodic in enumerate(periodic_trials):
                indices_per_level_cor = coarsen(indices_per_level, offsets, nelems, degree, marked_elements, m, periodic=periodic)
                assert all([np.array_equal(sol[i][j], indices_per_level_cor_j) for j, indices_per_level_cor_j in enumerate(indices_per_level_cor)])