import numpy
from nutils.expression_v2 import Namespace
import unittest
from nutils_pbox import *
import treelog

class TestConvergenceRatePbox2D(unittest.TestCase):

    def test_convergence(self):
        func = f'sin( {numpy.pi} x_0 ) sin( {numpy.pi} x_1 )'
        for method in ['thb', 'bern','bern_numba','global']:
            for degree in [1,2,3]:
                print(f'running {method} for degree {degree}')
                error = [0, 0]
                for i, nelems in enumerate([2, 8]):
                    pbox, geometry = mesh_pbox.pbox([degree] * 2, [nelems]*2)
                    ns = Namespace()
                    ns.x = geometry
                    ns.func = func
                    ns.basis = pbox.basis()
                    with treelog.set(treelog.FilterLog(treelog.current, minlevel=treelog.proto.Level.user)):
                        args = {'phi' : pbox.project(func = ns.func,onto = ns.basis, geometry = geometry, method = method)[0]}
                    error[i] = AdaptiveRefinement.AdaptiveRefinement(pbox).ErrorIteration( geometry, args, ns.func)[1]
                convergence_rate = numpy.log2(error[0]/error[1])/2
                self.assertTrue(numpy.abs(convergence_rate - degree - 1)<0.25)

if __name__ == '__main__':
    unittest.main()