from nutils_pbox.mesh_pbox import pbox
import numpy as np
import unittest

class TestPboxes(unittest.TestCase):
    def test_adding_pboxes(self):
        nelems = [2,2]
        degree = [2,2]

        topo, _ = pbox(degree, nelems)

        topo1 = topo.refined_by_pbox([0])
        topo2 = topo.refined_by_pbox([3])
        topo2 = topo2.refined_by_pbox([6])

        topo3 = topo1 + topo2

        sol = [np.array([1,2]),np.array([0,1,4,5,10,11,14]),np.array([54,55,62,63])]

        assert len(sol) == len(topo3.active_pboxes_per_level)
        for soli, topo3i in zip(sol, topo3.active_pboxes_per_level):
            assert np.array_equal(soli, topo3i)

