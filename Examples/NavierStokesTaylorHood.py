import time
import pickle

import matplotlib.colors
import numpy as np
from nutils import solver, function, export
from nutils.expression_v2 import Namespace
from nutils_pbox import mesh_pbox, AdaptiveRefinement
import numpy
import matplotlib.pyplot as plt

degree = 2
regularity = 1
nelems = 5 * degree
dim = 2
theta_refinement = 0.25
theta_coarsening = 1e-2

# best 2e-5

error_target_ref = 1e-4  * np.sqrt(degree ** dim)
error_target_picard = error_target_ref

reynolds = 1e3

dt = 2e-1
T = 5e0


assert dim == 2
assert degree > regularity



topo, geom = mesh_pbox.pboxNew( (degree+1,)*dim, (round(nelems/degree),) * dim , MaxL=4, admissibility_class=2 )

ns = Namespace()
ns.δ = function.eye(topo.ndims)
ns.ε = function.levicivita(2)
ns.sum = function.ones([topo.ndims])
ns.x = geom
ns.ν = 1/reynolds
ns.dt = dt
ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))

def setup(ns, topo):
    ns.ubasisComp = topo.basis('th-spline', degree=(degree+1,degree+1),continuity=(regularity,regularity))
    ns.ubasis = function.vectorize([ns.ubasisComp, ns.ubasisComp])
    ns.pbasis = topo.basis('th-spline', degree=(degree,degree),continuity=(regularity,regularity))[1:]

    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top'), 0])
    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top') * (1-topo.topology.boundary.indicator('left'))  * (1-topo.topology.boundary.indicator('right')), 0])


    ns.add_field(['u','uPrev','v'], ns.ubasis)
    ns.add_field(['uold'], ns.uoldbasis)
    ns.add_field(['p','q'], ns.pbasis)

    ns.hbasis = topo.basis('discont', degree=(0,0))
    ns.add_field(['h','η'], ns.hbasis)
    args = {'h' : np.sqrt(topo.integrate_elementwise('dV' @ ns, degree = 0))}

    int_degree = degree + 1

    cons_res  = topo.boundary.integral(' ( sum_i ( u_i - uwall_i )^2 ) dS ' @ ns, degree = 2*int_degree)
    cons_res += topo.boundary.integral(' ( sum_i ( v_i )^2 ) dS ' @ ns, degree = 2*int_degree)
    cons  = solver.optimize('u,v,', cons_res, droptol=1e-15)

    res  = topo.integral(' ( ν ∇_i(u_j) ∇_i(v_j) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( uPrev_i ∇_i(u_j) v_j ) dV ' @ ns, degree = 3*int_degree)
    res -= topo.integral(' ( p ∇_i(v_i) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( q ∇_i(u_i) ) dV ' @ ns, degree = 2*int_degree)

    # res += 1/dt * topo.integral(' ( ( u_i - uold_i )  v_i ) dV ' @ ns, degree= 2*int_degree)


    return ns, (args, cons, res)



def solve(topo, args, problem):
    args0, cons, res = problem

    args = solver.solve_linear('u:v,p:q', res, arguments=args | args0, constrain=cons)
    uerror = np.linalg.norm(args['u']-args['uPrev'],np.inf)

    # print(args.keys())
    # print([ ( key, len(args[key]) ) for key in args.keys()])
    uerror = topo.integrate_elementwise(' ( h h ∇_i( u_j - uPrev_j ) ∇_i( u_j - uPrev_j ) ) dV ' @ ns, degree=2 * degree, arguments=args)
    uerror = np.sqrt(np.sum(uerror))

    iter = 1
    while uerror >= error_target_picard:
        print(f'iter: {iter:3d}, picard step size: {uerror:.1e} / {error_target_picard:.1e}')
        args['uPrev'] = args['u']
        args = solver.solve_linear('u:v,p:q', res, arguments=args, constrain=cons)
        uerror = np.linalg.norm(args['u'] - args['uPrev'],np.inf)

        uerror = topo.integrate_elementwise(' ( h h ∇_i( u_j - uPrev_j ) ∇_i( u_j - uPrev_j ) ) dV ' @ ns, degree = 2*degree, arguments=args)
        uerror = np.sqrt(np.sum(uerror))

        # _, pbox_error = estimate(args, ns, topo)
        # uerror = max(pbox_error)

        iter += 1
    print(f'iter: {iter:3d}, picard step size: {uerror:.1e} / {error_target_picard:.1e}')
    return args

def estimate(args, ns, topo):
    ns.Rm_i = ' - ν ∇_j(∇_j(u_i)) + u_j ∇_j(u_i) + ∇_i(p) '
    ns.Rc   = '∇_i(u_i)'

    elem_error   = (np.sqrt(topo.integrate_elementwise(' ( h h Rm_i Rm_i ) dV ' @ ns, degree = 4 * (degree + 1), arguments = args))
                 + np.sqrt(topo.integrate_elementwise(' ( h h abs(Rc) ) dV ' @ ns, degree=2 * (degree + 1), arguments = args)))

    args['η'] = np.log10(elem_error)

    # map element error to pbox error
    pbox_error = np.sqrt(topo._elementwise_pboxwise(np.square(elem_error)))

    if topo.L == topo.MaxL:
        pbox_error[topo.pbox_offsets[topo.MaxL-1]:] = 0
    return args, pbox_error

def refine(topo, pbox_error):
    refine_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarking(numpy.square(pbox_error), theta_refinement)

    return topo.refined_by_pbox(refine_pbox)

def adaptive_refine(ns, topo, args):
    # initial setup
    ns_ref, problem = setup(ns, topo)

    # set initial guess for uPrev as previous solution of u
    argsUprev0 = topo.project('uold_0' @ ns, onto = ns_ref.ubasisComp, arguments = args, geometry= geom, method='thb')[0]
    argsUprev1 = topo.project('uold_1' @ ns, onto = ns_ref.ubasisComp, arguments = args, geometry= geom, method='thb')[0]
    args = {'uPrev': np.hstack([argsUprev0,argsUprev1]), 'uold': args['uold']}

    args = solve(topo, args, problem)
    args, error = estimate(args, ns_ref, topo)
    iter = 1

    while max(error) >= error_target_ref:
        print(f'Refinement iter {iter:3d}, reduced error to {max(error):.1e} / {error_target_ref:.1e}')
        topo_new = refine(topo, error)

        # set initial guess for uPrev as previous solution of u
        # perform projection onto new topology before ns is updated
        argsUprev0 = topo_new.project('u_0' @ ns, onto=topo_new.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', coarse_topology=topo)[0]
        argsUprev1 = topo_new.project('u_1' @ ns, onto=topo_new.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', coarse_topology=topo)[0]

        ns_ref, problem = setup(ns_ref, topo_new)

        args = {'uPrev': np.hstack([argsUprev0, argsUprev1]), 'uold': args['uold']}

        args = solve(topo_new, args, problem)
        args, error = estimate(args, ns_ref, topo_new)

        iter += 1
        topo = topo_new


    print(f'Refinement iter {iter:3d}, reduced error to {max(error):.1e} / {error_target_ref:.1e}')
    return ns_ref, topo, args

def solve_time(ns, topo, N, uinitial = None):

    # setup initial condition
    if uinitial is None:
        ns.uoldbasis = function.vectorize([topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
        args = {'uold': np.zeros(len(ns.uoldbasis))}
    else:
        raise f"uinitial must be none, not implemented"

    ns, topo, args = adaptive_refine(ns, topo, args)

    plot(f'Images/TH_image{0}.png', ns, topo, args)

    for t in range(N):
        args, pbox_error = estimate(args, ns, topo)

        print(f'start coarsening!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        coarse_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarkingCoarsening(numpy.square(pbox_error), theta_coarsening)

        topo_coarse = topo.coarsen_by_pbox(coarse_pbox)

        argsuold0 = topo_coarse.project('u_0' @ ns, onto=topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
        argsuold1 = topo_coarse.project('u_1' @ ns, onto=topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]

        ns.uoldbasis = function.vectorize([topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
        args = {'uold': np.hstack([argsuold0, argsuold1])}

        ns, topo, args = adaptive_refine(ns, topo_coarse, args)
        plot(f'Images/TH_image{t+1}.png', ns, topo, args)

    return ns, topo, args

def solve_ss(ns, topo, N, uinitial = None):
    # setup initial condition
    if uinitial is None:
        ns.uoldbasis = function.vectorize([topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
        args = {'uold': np.zeros(len(ns.uoldbasis))}
    else:
        raise f"uinitial must be none, not implemented"

    ns, topo, args = adaptive_refine(ns, topo, args)

    plot(f'Images/TH_image{0}.png', ns, topo, args)


    return ns, topo, args


def plot(name, ns, topo, args):
    bezier = topo.sample('bezier', degree + 1)

    x, u, error = bezier.eval(['x_i', 'sqrt(u_i u_i)', 'η'] @ ns, **args)

    fig, ax = plt.subplots(1, 2)

    # export.triplot(ax[0,0], x, v[:,0], tri=bezier.tri, hull=bezier.hull)
    # export.triplot(ax[0,1], x, v[:,1], tri=bezier.tri, hull=bezier.hull)
    export.triplot(ax[0], x, u, tri=bezier.tri, hull=bezier.hull, cmap='hot_r', clim=(0, 1))
    export.triplot(ax[1], x, error, tri=bezier.tri, hull=bezier.hull)

    # plt.savefig(name,dpi=300)
    plt.show()

# ns, topo, args = solve_time(ns, topo, N = round(T/dt))
ns, topo, args = solve_ss(ns, topo, N = round(T/dt))

