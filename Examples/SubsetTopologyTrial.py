import numpy as np
from nutils_pbox import mesh_pbox
from nutils import mesh


domain, geom = mesh.unitsquare(2, 'square')
geom -= .5 # shift domain center to origin

x, y = geom
exact = (x**2 + y**2)**(1/3) * np.cos(np.arctan2(y+x, y-x) * (2/3))
selection = domain.select(exact, ischeme='gauss1')
print(selection)

domain = domain.subset(selection, newboundary='corner')


domain_pbox, geom_pbox = mesh_pbox.pbox((2,2),(2,2))
domain_pbox = domain_pbox.refined_by_pbox([2])
geom_pbox -= .5 # shift domain center to origin

x, y = geom_pbox
exact = (x**2 + y**2)**(1/3) * np.cos(np.arctan2(y+x, y-x) * (2/3))
selection = domain_pbox.select(exact, ischeme='gauss')
print(selection)
