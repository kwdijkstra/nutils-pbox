import time

import numpy

import nutils_pbox.topology_pbox
from nutils_pbox import mesh_pbox, tools_nutils
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export
from nutils_pbox.tools_evaluable import split_projection_refinement, split_projection_coarsening



r_elems = (2, 2) # number of pboxes in each directions
degree  = (4, 4) # spline degree

method = 'bern_numba'


pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=2)
pbox = pbox.refined_by([0])


ns = Namespace()
ns.x = geometry
ns.basis = pbox.basis('th-spline', degree = degree)

ns.fun = ' '.join([f"sin( {numpy.pi} x_{i} ) " for i in range(len(degree))])


# initial projection
args = {'phi' : pbox.project(ns.fun, onto = None, geometry = geometry, method = method, solve_method='QR', bern_degree=degree)[0]}

ns.add_field('phi', ns.basis)

# refine once
pbox_refined = pbox.refined_by([0])

ns.basisRefined = pbox_refined.basis()
ns.add_field('phi2', ns.basisRefined)

# projection over refined topology of previous projection
# this shows how we can refine a finite element function
# here we also make use of the relation between pbox and pbox_refined to speed up the projection
t0 = time.perf_counter()
args['phi2'] = pbox_refined.project(func = ns.phi, onto = ns.basisRefined, geometry = geometry, method = method, bern_degree = degree, solve_method='MoorePenrose', arguments=args, coarse_topology=pbox , coarse_basis=ns.basis)[0]
print(f'time : {time.perf_counter() - t0}')

# refine once more
pbox_more_refined = pbox_refined.refined_by([2,5])
ns.basisTwiceRefined = pbox_more_refined.basis()
ns.add_field('phi3', ns.basisTwiceRefined)

# another projection
t0 = time.perf_counter()
args['phi3'] = pbox_more_refined.project(func = ns.phi2, onto = ns.basisTwiceRefined, geometry = geometry, method = method, bern_degree = degree, solve_method='MoorePenrose', arguments=args, coarse_topology=pbox_refined, coarse_basis=ns.basisRefined)[0]
print(f'time : {time.perf_counter() - t0}')

# coarsen topology
pbox_coarse = pbox_more_refined.coarsen_by_pbox([8,9,12,13])
# change degree of spline to project onto


# print(len(pbox_coarse))
# print(len(pbox_more_refined))
#
# func = 'phi3' @ ns
#
# bern_basis = pbox_coarse.basis('discont',degree = degree)
# print(pbox_coarse.integrate(bern_basis, degree = max(degree), arguments = args))
# print(pbox_more_refined.integrate(bern_basis, degree = max(degree), arguments = args))
# print(pbox_more_refined.integrate(func, degree = max(degree), arguments = args))
# print(pbox_more_refined.integrate(bern_basis * func, degree = max(degree), arguments = args))
#
# quit()
coarse_degree = (4,3)
# coarse_degree = degree
ns.basiscoarse = pbox_coarse.basis(degree = coarse_degree)
ns.add_field('phi4', ns.basiscoarse)

# final projection
t0 = time.perf_counter()
args['phi4'] = pbox_coarse.project(func = ns.phi3, onto = ns.basiscoarse, geometry = geometry, method = method, bern_degree = coarse_degree, solve_method='MoorePenrose', arguments=args, refined_topology=pbox_more_refined, refined_basis=ns.basisTwiceRefined)[0]
# args['phi4'] = pbox_coarse.project(func = ns.fun, onto = ns.basiscoarse, geometry = geometry, method = method, bern_degree = coarse_degree, solve_method='MoorePenrose', arguments=args, refined_topology=pbox_more_refined, refined_basis=ns.basisTwiceRefined)[0]
print(f'time : {time.perf_counter() - t0}')

# plot final projection
bezier = pbox_coarse.sample('bezier', 4 * max(degree))
x, approx, error = bezier.eval(['x_i','phi4', 'phi4 - fun'] @ ns, **args)
print(f"max error over full domain : {max(abs(error))}")
fig, axs = plt.subplots(1,2)
export.triplot(axs[0], x, approx, tri=bezier.tri, hull=bezier.hull, cmap='hot_r')
export.triplot(axs[1], x, error, tri=bezier.tri, hull=bezier.hull)
axs[0].set_title('solution')
axs[1].set_title('error')
plt.show()


