import numpy
from nutils_pbox import mesh_pbox
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export



r_elems = (2, 2) # number of pboxes in each directions
degree  = (4, 4) # spline degree


pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=2)
pbox.refined_by_pbox([0])
# pbox.refined_by_pbox([3,4,5])

ns = Namespace()
ns.x = geometry
ns.basis = pbox.basis('th-spline', degree = degree)


coeffs = list(map(numpy.asarray, ns.basis._coeffs))
# coeffs = list(map(numpy.asarray, ns.basis._arg_dofs))
# coeffs = list(map(numpy.asarray, ns.basis._arg_coeffs))
dofs = list(map(numpy.asarray, ns.basis._dofs))
# args_ndofs = list(map(numpy.asarray, ns.basis._arg_ndofs))

# ns.basis._arg_dofs

print(len(ns.basis))
print(len(coeffs)) # per element, matrix of coeffs (row) for each dof (column)
print(len(dofs)) # per element, list of dofs
# print(len(coeffs_evaluable))
# print(len(args_ndofs))

print(coeffs[0].shape)

# print(-0.5+numpy)



print(pbox.topology)

quit()

ns.fun = ' '.join([f"sin( {numpy.pi} x_{i} ) " for i in range(len(degree))])

args = {'phi' : pbox.project(ns.fun, geometry, method = 'thb')[0]}

ns.add_field('phi', ns.basis)


bezier = pbox.topology.sample('bezier', 4 * max(degree))
x, approx, error = bezier.eval(['x_i','phi', 'phi - fun'] @ ns, **args)
print(f"max error over full domain : {max(error)}")
fig, axs = plt.subplots(1,2)
export.triplot(axs[0], x, approx, tri=bezier.tri, hull=bezier.hull)
export.triplot(axs[1], x, error, tri=bezier.tri, hull=bezier.hull)
axs[0].set_title('solution')
axs[1].set_title('error')
plt.show()


