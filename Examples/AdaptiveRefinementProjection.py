import os

import numpy
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export, mesh

import nutils_pbox.debug_logger
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from nutils_pbox import mesh_pbox


def Plot(axs, NAME:str, topology, geometry, basis, func, degree, args):
    ns = Namespace()
    ns.x = geometry
    ns.PI = numpy.pi
    ns.basis = basis
    ns.add_field('phi', ns.basis)
    ns.fun = func
    bezier = topology.sample('bezier', 4 * max(degree))
    x, error, zeroFunc = bezier.eval(['x_i', 'phi - fun', '0'] @ ns, **args)
    export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
    axs.set_title(f'{NAME}, DOFS : {len(ns.basis)}, error : {max(abs(error)):.2e}')


# def PrintIterativeTopologies(name : str, topology_list, geometry, error_list, dofs_list):
#
#     path = os.getcwd()[0:-8]+f"\\OutputFiles\\{name}"
#     try: os.mkdir(path)
#     except:
#         print("dir name already exists")
#     ns = Namespace()
#     ns.x = geometry
#     fig, axs = plt.subplots(1, 1)
#     for i in range(len(topology_list)):
#         topology = topology_list[i]
#         bezier = topology.sample('bezier', 4 * max(degree))
#         x, zeroFunc = bezier.eval(['x_i', '0'] @ ns, **args)
#         export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
#         axs.set_aspect('equal')
#         plt.savefig(path+f"\\image_{i}",dpi=600)
#         axs.clear()
#
#     N = len(error_list)
#     fig,axs  = plt.subplots(1, 1)
#     axs.loglog(dofs_list,error_list)
#     axs.set_title(f'error reduction over iterations')
#     axs.set_xlabel('DOFS')
#     axs.set_ylabel('log error')
#     axs.set_aspect('equal')
#     plt.savefig(path + f"\\error_convergence", dpi=600)


r_elems = (5, 5)
degree  = (3, 3)

theta = 0.5

admissibility_class = 2
eps_target = 1e-4
MaxL = 4

func = '1 - tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.3 ) / ( 0.05 sqrt(2) )  )'


fig, axs = plt.subplots(1,3)


refine_type = ['nutils_pbox-local','nutils_pbox-global','mesh']


## local nutils_pbox
if 'nutils_pbox-local' in refine_type:
    name = f'PboxLocal_degree_{max(degree)}_admis_{admissibility_class}_error_target_{eps_target:.1e}_dorfler_theta_{theta}'
    pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL = MaxL)
    args = {}
    pbox_refined, args['phi'] = AdaptiveRefinement(pbox).Project_iter_refine(func, geometry, eps_target, theta = theta, method='bern')

    Plot(axs[0],'Pbox : Loc', pbox_refined.nutils_topology, geometry, pbox_refined.basis(), func, degree, args)

## global nutils_pbox
if 'nutils_pbox-global' in refine_type:
    pbox, geometry = mesh_pbox.pbox(degree,r_elems,admissibility_class=admissibility_class, MaxL = MaxL)
    args = {}
    pbox, args['phi'] = AdaptiveRefinement(pbox).Project_iter_refine(func, geometry, eps_target, theta = theta, method = 'global')

    Plot(axs[1],'Pbox : Global', pbox.nutils_topology, geometry, pbox.basis(), func, degree, args)

## global mesh
if 'mesh' in refine_type:
    topology, geometry = mesh.rectilinear([numpy.linspace(0, 1, r * degree[i] + 1) for i,r in enumerate(r_elems)])
    args = {}
    topology, args['phi'] = AdaptiveRefinement(topology, degree = degree, MaxL=MaxL, admissibility_class=admissibility_class).Project_iter_refine(func, geometry, eps_target, theta=theta)

    Plot(axs[2],'Mesh : Global', topology, geometry, topology.basis('th-spline',degree = degree), func, degree, args)

plt.show()


