# import src.nutils_pbox.evaluable_pbox
import random
import time
from itertools import product
from nutils_pbox import mesh_pbox
import nutils.topology
from nutils import export, function
from nutils.expression_v2 import Namespace
import numpy as np
import scipy
import matplotlib.pyplot as plt
import math
from src.nutils_pbox.pol import  _nutils_coeffs_to_numpy_coeffs
from src.nutils_pbox.jitclasses import FastDictFloatMatrix
import src.nutils_pbox.pol

from src.nutils_pbox.jitclasses import from_list_of_arrays, FastDictFloatMatrix, FastDictFloatTensor
from numba import njit, prange
from src.nutils_pbox.numba_thb_project_qr import thb_bezier_project,_numpy_coeffs_to_nutils_coeffs, _nutils_basis_coeffs_to_array, _nutils_basis_structured_coeffs_to_nutils_array



degree = (2,2)

pbox, geom = mesh_pbox.pbox(degree,(80,80))

tb = pbox.basis('th-spline',degree = degree)

def geom_func(x,y):
    return [x + y, y]

x,y = geom


ns = Namespace()
ns.x = np.stack(geom_func(x,y))



ns.f = f'sin( {np.pi} x_0)  sin( {np.pi} x_1)'
ns.define_for('x', gradient='D', normal='n', jacobians=('dV', 'dS'))


ns.thbb = pbox.basis('th-spline',degree=degree)
ns.add_field('ρ',ns.thbb)


pbox.GenerateProjectionElement()

sol = thb_bezier_project('f' @ ns, ns.thbb, pbox.nutils_topology, pbox.basis('discont',degree = degree), geom, pbox.proj_elem, degree)
# #
# #
# # print(pbox.proj_elem)
# t0 = time.perf_counter()
# sol = thb_bezier_project('f' @ ns, ns.thbb, pbox.nutils_topology, pbox.basis('discont',degree = degree), geom, pbox.proj_elem, degree)
# t1  = time.perf_counter()
#
# print(f'numba first run took : {t1 - t0}')

t0 = time.perf_counter()
_ = pbox.project('f' @ ns, ns.thbb, geom, method='bern_numba')
t1  = time.perf_counter()
print(f'numba pbox initialization took : {t1 - t0}')

pbox.ElemSize = []

t0 = time.perf_counter()
_ = pbox.project('f' @ ns, ns.thbb, geom, method='bern_numba')
t1  = time.perf_counter()
print(f'numba pbox took : {t1 - t0}')

t0 = time.perf_counter()
_ = pbox.project('f' @ ns, ns.thbb, geom, method='bern_numba')
t1  = time.perf_counter()
print(f'numba pbox elem precomputed took : {t1 - t0}')

# t0 = time.perf_counter()
# _ = pbox.project('f' @ ns, ns.thbb, geom, method='bern')
# t1  = time.perf_counter()
# print(f'old pbox took : {t1 - t0}')

t0 = time.perf_counter()
_ = pbox.nutils_topology.project('f' @ ns,ns.thbb, geom, degree = 2 * max(degree))
t1  = time.perf_counter()
print(f'global took : {t1 - t0}')

# print(sol)


# ns.add_field('ϕ',ns.bb)


# print(res)

args = { 'ρ' : sol}

bezier = pbox.sample('bezier',15)
x, ρ, error_ρ = bezier.eval(['x_i','ρ','ρ - f'] @ ns, arguments = args)

fig,axs = plt.subplots(1,2)

# im1 = export.triplot(axs[0,0],x,ϕ,tri=bezier.tri)
# im2 = export.triplot(axs[0,1],x,error,tri=bezier.tri)
im3 = export.triplot(axs[0],x,ρ,tri=bezier.tri)
im4 = export.triplot(axs[1],x,error_ρ,tri=bezier.tri, hull=bezier.hull)

# plt.colorbar(im1)
# plt.colorbar(im2)
plt.colorbar(im3)
plt.colorbar(im4)

plt.show()