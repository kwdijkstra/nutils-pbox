import numpy
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export
from nutils_pbox.mesh_pbox import pbox
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement


r_elems = (5, 2)
degree  = (2, 2)

func = '1 - tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.3 ) / ( 0.05 sqrt(2) )  )'


pbox, geometry = pbox(degree, r_elems, admissibility_class=2, MaxL=6)

# geometric mapping
theta = (1-geometry[0]) * numpy.pi / 2
r_min = 0.5
r_max = 1

r = (r_max - r_min) * geometry[1] + r_min

geometry = numpy.stack([r*numpy.cos(theta), r*numpy.sin(theta)])

# projection
ns = Namespace()
ns.x = geometry

ns.fun = func

pbox_refined, proj_coeffs = AdaptiveRefinement(pbox).Project_iter_refine(ns.fun, geometry,3e-5, theta = 0.5, method='bern_numba')

args = {'phi' : proj_coeffs}

ns.add_field('phi', pbox_refined.basis())

# plotting
bezier = pbox_refined.sample('bezier', 4 * max(degree))
x, approx, error = bezier.eval(['x_i','phi', 'phi - fun'] @ ns, **args)
print(f"max error over full domain : {max(error)}")
fig, axs = plt.subplots(1,2)
export.triplot(axs[0], x, approx, tri=bezier.tri, hull=bezier.hull)
export.triplot(axs[1], x, 0*error, tri=bezier.tri, hull=bezier.hull, cmap='binary')
plt.show()


