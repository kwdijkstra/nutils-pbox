import time
import pickle

import matplotlib.colors
import numpy as np
from nutils import solver, function, export
from nutils.expression_v2 import Namespace
from nutils_pbox import mesh_pbox, AdaptiveRefinement
import numpy
import matplotlib.pyplot as plt

degree = 2
k = 2
regularity = 1
nelems = 8 * degree
dim = 2


error_target = 1e-12
reynolds = 6.5e2

assert dim == 2
assert degree > regularity

topo, geom = mesh_pbox.pboxNew( (degree,)*dim, (round(nelems/degree),) * dim , MaxL=4, admissibility_class=2 )

ns = Namespace()
ns.δ = function.eye(topo.ndims)
ns.ε = function.levicivita(2)
ns.sum = function.ones([topo.ndims])
ns.x = geom
ns.ν = 1/reynolds
ns.f = reynolds * np.stack(['x_1 - 0.5','x_0 - 0.5'] @ ns)
ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))

def setup(ns, topo):
    ns.ωbasis = topo.basis('th-spline', degree = (degree, degree))
    ns.ubasis = function.vectorize([
        topo.basis('th-spline', degree = (degree,degree-1)),
        topo.basis('th-spline', degree = (degree-1,degree))
    ])
    ns.pbasis = topo.basis('th-spline', degree=(degree-1,degree-1))

    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top'), 0])
    #
    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top') * (1-topo.topology.boundary.indicator('left'))  * (1-topo.topology.boundary.indicator('right')), 0])

    # ns.uwall = np.stack([0,0])
    # ns.f = 1e-0 * np.stack(['(x_1 - 0.5)^3 + abs( x_1 - 0.5 )', '(0.5 - x_0)^3'] @ ns)

    ns.add_field(['ω'],ns.ωbasis)
    ns.add_field(['u','v','U'], ns.ubasis)
    ns.add_field(['p','q','P'], ns.pbasis)

    ns.hbasis = topo.basis('discont', degree=(0,0))
    ns.add_field(['h','η'], ns.hbasis)
    args = {'h' : np.sqrt(topo.integrate_elementwise('dV' @ ns, degree = 0))}

    int_degree = degree + 1

    cons_res  = topo.boundary.integral(' ( n_i ( u_i - uwall_i )^2 ) dS ' @ ns, degree = 2*int_degree)
    cons  = solver.optimize('u,', cons_res, droptol=1e-15)

    res  = topo.integral(' ( ν (∇_i(u_j) + ∇_j(u_i) ) ∇_i(v_j) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( u_i ∇_i(u_j) v_j ) dV ' @ ns, degree = 3*int_degree)
    res -= topo.integral(' ( p ∇_i(v_i) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( q ∇_i(u_i) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' p dV' @ ns, degree=2 * int_degree)
    res -= topo.integral(' ( f_i v_i ) dV ' @ ns, degree = 2 * int_degree)

    ns.N = f'{5 * degree } / h'  # Nitsche constant based on element size = 1/nelems
    ns.nitsche_i = 'ν ( N v_i - ( ∇_j(v_i) + ∇_i(v_j) ) n_j ) '
    res += topo.topology.boundary.integral('(nitsche_i ( u_i - uwall_i ) - v_i ( ν ( ∇_i(u_j) + ∇_j(u_i) ) - p δ_ij ) n_j) dS' @ ns, degree=2 * degree)

    args = solver.newton('u:v,p:q', res, arguments=args, constrain=cons).solve(tol=error_target)

    sqr = topo.integral(' sum_i ( u_i - ε_ij ∇_j(ω) )^2 dV' @ ns, degree = 2 * degree)
    args = solver.optimize('ω,', sqr, arguments=args)

    return ns, args

def finescales(ns, topo, args):
    ns.ωPbasis = topo.basis(degree = (degree+k,degree+k))
    ns.uFbasis = function.vectorize([
        topo.basis('th-spline', degree=(degree+k, degree+k - 1)),
        topo.basis('th-spline', degree=(degree+k - 1, degree+k))
    ])
    ns.pFbasis = topo.basis('th-spline', degree=(degree+k - 1, degree+k - 1))

    ns.add_field(['ωP'],ns.ωPbasis)
    ns.add_field(['uP','vP'], ns.uFbasis)
    ns.add_field(['pP','qP'], ns.pFbasis)

    int_degree = degree + k + 1

    cons_res  = topo.boundary.integral(' ( n_i ( uP_i )^2 ) dS ' @ ns, degree = 2*int_degree)
    cons  = solver.optimize('uP,', cons_res, arguments = args, droptol=1e-15)

    # res =  topo.integral(' ( ν (∇_i( u_j  ) + ∇_j( u_i  ) ) ∇_i( vP_j ) ) dV ' @ ns, degree=2 * int_degree)
    res =  topo.integral(' ( ν (∇_i( uP_j ) + ∇_j( uP_i ) ) ∇_i( vP_j ) ) dV ' @ ns, degree=2 * int_degree)
    # res += topo.integral(' ( ( u_i ) ∇_i( u_j ) vP_j ) dV ' @ ns, degree=3 * int_degree)
    res += topo.integral(' ( (uP_i) ∇_i(u_j) vP_j ) dV ' @ ns, degree=3 * int_degree)
    res += topo.integral(' ( (u_i) ∇_i(uP_j) vP_j ) dV ' @ ns, degree=3 * int_degree)
    res += topo.integral(' ( (uP_i) ∇_i(uP_j) vP_j ) dV ' @ ns, degree=3 * int_degree)
    # res -= topo.integral(' ( ( p ) ∇_i(vP_i) ) dV ' @ ns, degree=2 * int_degree)
    res -= topo.integral(' ( ( pP ) ∇_i(vP_i) ) dV ' @ ns, degree=2 * int_degree)
    res -= topo.integral(' ( f_i vP_i ) dV ' @ ns, degree=2 * int_degree)

    # full residual
    res += topo.integral( ' ( u_j ∇_j(u_i) - ν ∇_j(∇_j(u_i) + ∇_i(u_j)) + ∇_i(p) ) vP_i dV ' @ ns, degree = 3 * int_degree )

    # orthogonality
    res += topo.integral( ' ( uP_i v_i ) dV' @ ns, degree = 2 * int_degree)
    res += topo.integral( ' ( U_i vP_i ) dV' @ ns ,degree = 2 * int_degree)
    res += topo.integral( ' ( pP q ) dV' @ ns, degree = 2 * int_degree)
    res += topo.integral( ' ( P qP ) dV' @ ns, degree = 2 * int_degree)


    res += topo.integral(' ( qP ∇_i( uP_i ) ) dV ' @ ns, degree=2 * int_degree)
    res += topo.integral(' ( pP ) dV' @ ns, degree = 2*int_degree)

    ns.NP = f'{5 * ( degree + k )} / h'  # Nitsche constant based on element size = 1/nelems
    ns.nitscheP_i = 'ν ( N vP_i - ( ∇_j(vP_i) + ∇_i(vP_j) ) n_j ) '
    # res +=  topo.topology.boundary.integral('(nitscheP_i ( u_i + uP_i - uwall_i ) - vP_i ( ν ( ∇_i(u_j + uP_j ) + ∇_j(u_i + uP_i) ) - ( pP) δ_ij ) n_j) dS' @ ns, degree=2 * degree)
    # res +=  topo.topology.boundary.integral('( - vP_i ( ν ( ∇_i( uP_j ) + ∇_j( uP_i) ) - (  pP) δ_ij ) n_j) dS' @ ns, degree=2 * degree)
    # res +=  topo.topology.boundary.integral('(nitscheP_i ( uP_i + uP_i - uwall_i  ) - vP_i ( ν ( ∇_i( u_j + uP_j) + ∇_j( u_i + uP_i) ) - ( p + pP) δ_ij ) n_j) dS' @ ns, degree=2 * int_degree)
    # res +=  topo.topology.boundary.integral('(nitscheP_i ( u_i + uP_i - uwall_i ) - vP_i ( ν ( ∇_i( uP_j) + ∇_j( uP_i) ) - ( pP) δ_ij ) n_j) dS' @ ns, degree=2 * int_degree)
    res +=  topo.topology.boundary.integral('( nitscheP_i ( u_i + uP_i - uwall_i ) - vP_i ( ν ( ∇_i( uP_j) + ∇_j( uP_i) ) - ( pP) δ_ij ) n_j) dS' @ ns, degree=2 * int_degree)

    args = solver.newton('uP:vP,pP:qP,U:v,P:q', res, constrain=cons, arguments=args).solve(tol=error_target)

    sqr = topo.integral(' sum_i ( uP_i - ε_ij ∇_j(ωP) )^2 dV' @ ns, degree = 2 * int_degree)
    args = solver.optimize('ωP,', sqr, arguments=args)

    return ns, args

def coarsescales(ns, topo, args):
    int_degree = degree + k + 1

    cons_res  = topo.boundary.integral(' ( n_i ( u_i )^2 ) dS ' @ ns, degree = 2*int_degree)
    cons  = solver.optimize('u,', cons_res, arguments=args, droptol=1e-15)

    res  = topo.integral(' ( ν ( ∇_i(u_j + uP_j) + ∇_j(u_i + uP_i) ) ∇_i(v_j) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( (u_i + uP_i) ∇_i(u_j + uP_j) v_j ) dV ' @ ns, degree = 3*int_degree)
    res -= topo.integral(' ( ( p ) ∇_i(v_i) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( q ∇_i(u_i) ) dV ' @ ns, degree = 2*int_degree)
    res += topo.integral(' ( p ) dV' @ ns, degree=2 * int_degree)
    res -= topo.integral(' ( f_i v_i ) dV ' @ ns, degree=2 * int_degree)

    ns.N = f'{5 * degree } / h'  # Nitsche constant based on element size = 1/nelems
    ns.nitsche_i = 'ν (N v_i - ( ∇_j(v_i) + ∇_i(v_j) ) n_j) '
    res += topo.topology.boundary.integral('(nitsche_i (u_i + uP_i - uwall_i) - v_i ( ν ( ∇_i(u_j + uP_j) + ∇_j(u_i + uP_i) ) - ( p ) δ_ij )  n_j) dS' @ ns, degree=2 * int_degree)
    # res += topo.topology.boundary.integral('(nitsche_i (u_i - uwall_i) - v_i ( ν ( ∇_i(u_j ) + ∇_j(u_i ) ) - (p ) δ_ij )  n_j) dS' @ ns, degree=2 * degree)


    args = solver.newton('u:v,p:q', res, arguments=args, constrain=cons).solve(tol=error_target)

    sqr = topo.integral(' sum_i ( u_i  - ε_ij ∇_j(ω) )^2 dV' @ ns, degree = 2 * int_degree)
    args = solver.optimize('ω,', sqr, arguments=args)

    return ns, args


ns, args = setup(ns, topo)

bezier = topo.sample('bezier', 9*degree)

x, uNoStab, ωNoStab = bezier.eval(['x_i','sqrt(u_i u_i)', 'ω'] @ ns, **args)
# ns.ν = 1e-3
for i in range(10):
    print(i+1)
    ns, args = finescales(ns, topo, args)
    args['uP'] = 1 * args['uP']
    ns, args = coarsescales(ns, topo, args)



x, uStab, uP, ωStab, ωP = bezier.eval(['x_i','sqrt( u_i u_i )','sqrt(uP_i uP_i)','ω', 'ωP'] @ ns, **args)

fig, ax = plt.subplots(1,3)


# export.triplot(ax[0,0], x, v[:,0], tri=bezier.tri, hull=bezier.hull)
# export.triplot(ax[0,1], x, v[:,1], tri=bezier.tri, hull=bezier.hull)
export.triplot(ax[0], x, uNoStab, tri=bezier.tri, hull=bezier.hull, cmap='hot_r', clim=(0,1))
ax[0].tricontour(x[:, 0], x[:, 1], bezier.tri, ωNoStab, levels=numpy.percentile(ωNoStab, numpy.arange(2, 100, 3)), colors='k', linestyles='solid', linewidths=.5, zorder=9)

export.triplot(ax[1], x, uP, tri=bezier.tri, hull=bezier.hull, cmap='hot_r', clim=(0,1))
ax[1].tricontour(x[:, 0], x[:, 1], bezier.tri, ωP, levels=numpy.percentile(ωP, numpy.arange(2, 100, 3)), colors='k', linestyles='solid', linewidths=.5, zorder=9)

export.triplot(ax[2], x, uStab, tri=bezier.tri, hull=bezier.hull, cmap='hot_r', clim=(0,1))
ax[2].tricontour(x[:, 0], x[:, 1], bezier.tri, ωStab, levels=numpy.percentile(ωStab , numpy.arange(2, 100, 3)), colors='k', linestyles='solid', linewidths=.5, zorder=9)

# export.triplot(ax[1], x, error, tri=bezier.tri, hull=bezier.hull)

plt.show()