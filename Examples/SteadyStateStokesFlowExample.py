import time
import pickle

import matplotlib.colors
import numpy as np
from nutils import solver, function, export
from nutils.expression_v2 import Namespace
from nutils_pbox import mesh_pbox, AdaptiveRefinement
import numpy
import matplotlib.pyplot as plt

degree = 2
nelems = 2 * degree
dim = 2

warmstart = 1
error_target = 1e-3

solverargs = {'linrtol':1e-10,'linatol':1e-10}

reynolds = 1e3

assert dim == 2

topo, geom = mesh_pbox.pboxNew( (degree,)*dim, (round(nelems/degree),) * dim , MaxL=6, admissibility_class=2 )

# topo.refined_by_pbox([1,2,3])

def Setup_ns(topo, old_sol = None, old_basis = None):
    ns = Namespace()
    ns.δ = function.eye(topo.ndims)
    ns.ε = function.levicivita(2)
    ns.Σ = function.ones([topo.ndims])
    ns.Re = reynolds
    # ns.uwall = numpy.stack([topo.boundary.indicator('top'), 0])
    ns.x = geom
    ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))
    ns.vbasisPrev = function.vectorize([
                    topo.basis('th-spline', degree = (degree,degree-1)),
                    topo.basis('th-spline', degree = (degree-1,degree))
        ])

    # if old_sol is not None and old_basis is not None:
    #     ns.add_field('uold', old_basis)
    #     args = {'uold': old_sol}
    # else:
    #     ns.add_field('uold', ns.vbasisPrev)
    #     args = {'uold':numpy.zeros((len(ns.vbasisPrev),))}

    args = {}


    ns.f = 0*numpy.stack([f'cos( {numpy.pi} x_1)', f'cos( {numpy.pi} x_1)'] @ ns)
    return ns, args

def setup_residual(topo, solve = True, old_sol = None, old_basis = None):

    # print('topo',topo.pbox_active_indices_per_level)

    ns, args = Setup_ns(topo, old_sol, old_basis)
    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top'), 0])
    # ns.uwall = numpy.stack([0,0])

    ns.vxbasis = topo.basis('th-spline', degree=(degree, degree - 1))
    ns.vybasis = topo.basis('th-spline', degree=(degree - 1, degree))
    ns.vbasis = function.vectorize([
        ns.vxbasis,
        ns.vybasis
    ])
    ns.pbasis = topo.basis('th-spline', degree=(degree - 1, degree - 1))
    ns.nelemsbasis = topo.basis('discont', degree = (0,0))


    ns.add_field(('u', 'v'), ns.vbasis)
    ns.add_field(('p', 'q'), ns.pbasis)
    ns.add_field('nelems', ns.nelemsbasis)
    ns.residual_j = ' u_i ∇_i(u_j) - ∇_i(∇_i(u_j)) / Re + ∇_j(p)'
    # ns.residual_j = 'u_k ∇_k(u_j) - ( ∇_i(∇_i(u_j)) + ∇_i(∇_j(u_i)) ) / Re + ∇_j(p)'
    # ns.Res = numpy.stack([ns.residual, '∇_i(u_i)' @ ns])
    ns.σ_ij = '(∇_j(u_i) + ∇_i(u_j)) / Re - p δ_ij'

    res  = reynolds * topo.integral(' (∇_j(v_i) σ_ij ) dV ' @ ns, degree = 2 * degree)
    res += (1 / reynolds) * topo.integral(' (∇_i(u_i) q ) dV ' @ ns, degree = 2 * degree)
    res -= reynolds * topo.integral(' ( f_i v_i ) dV ' @ ns, degree = 2 * degree)

    # res += reynolds * topo.integral(' ( ((u_i - uold_i) / dt ) v_i ) dV ' @ ns, degree = 2 * degree)

    # boundary
    # ns.N = 5 * degree * nelems  # Nitsche constant based on element size = 1/nelems
    ns.N = f'{5 * degree }  nelems'  # Nitsche constant based on element size = 1/nelems
    ns.nitsche_i = '(N v_i - (∇_j(v_i) + ∇_i(v_j)) n_j) / Re'
    res += reynolds * topo.topology.boundary.integral('(nitsche_i (u_i - uwall_i) - v_i σ_ij n_j) dS' @ ns, degree=2 * degree)

    sqr = topo.topology.boundary.integral(' ( u_i n_i )^2 dS ' @ ns, degree= 2 * degree)
    cons = solver.optimize('u,', sqr, droptol=1e-15)

    topo.elem_size(0, geom)
    args['nelems'] = 1/numpy.sqrt(topo.ElemSize)
    # print('nelems',len(args['nelems']), len(ns.nelemsbasis))
    if solve:
        args = solver.solve_linear('u:v,p:q', res, constrain=cons, arguments=args)

    res += reynolds * topo.integral('v_i ∇_j(u_i) u_j dV' @ ns, degree=degree*3)

    return res, cons, ns, args
def plotting(topo, args, ns, error_element,i, newton_iterations):

    bezier = topo.sample('bezier', 9*degree)

    args['nelems'] = numpy.log10(error_element)

    x, v, u, error = bezier.eval(['x_i','u_i','sqrt(u_i u_i)','nelems'] @ ns, **args)

    fig, ax = plt.subplots(1,1)


    # export.triplot(ax[0,0], x, v[:,0], tri=bezier.tri, hull=bezier.hull)
    # export.triplot(ax[0,1], x, v[:,1], tri=bezier.tri, hull=bezier.hull)
    export.triplot(ax, x, u, tri=bezier.tri, hull=bezier.hull, cmap='hot_r', clim=(0,1))
    ax.set_title(f'Flow velocity\n{newton_iterations} iterations')
    # ax.set_title(f'Starting Mesh')

    plt.savefig(f'Images/FlowVelocityStep_{i}.png',dpi=600)

    fig.clf()

    fig, ax = plt.subplots(1, 1)
    export.triplot(ax, x, error, tri=bezier.tri, hull=bezier.hull)

    colorbar_Min = -4
    colorbar_Max = 4
    target = -3


    formatter = matplotlib.ticker.FixedFormatter([f"{f'1e{tick}' if tick != target else f'1e{tick} : Target' }"  for tick in np.arange(colorbar_Min,colorbar_Max+1)])

    plt.colorbar(plt.cm.ScalarMappable(matplotlib.colors.Normalize(colorbar_Min,colorbar_Max)),ax= ax,ticks = np.arange(colorbar_Min,colorbar_Max+1),format= formatter)
    # export.triplot(ax[1,1], x, residual, tri=bezier.tri, hull=bezier.hull)

    # fig.colorbar(im, cax=ax[1,1], orientation='vertical')
    ax.set_title('a posteriori error estimation')
    plt.savefig(f'Images/ElementError_{i}.png',dpi=600)

    fig.clf()

    # plt.show()
def calculate_error(topo, ns, argsError):

    t0 = time.perf_counter()
    # error_pbox = numpy.sqrt(
    #     topo.integrate_pboxwise(' ( (  Re ( residual_i residual_i ) / nelems^2 )   ) dV ' @ ns, degree = 4 * degree, arguments = argsError) +
    #     topo.integrate_pboxwise(' ( ( ∇_i(u_i) )^2 / Re  ) dV ' @ ns, degree= 2 * degree, arguments=argsError)
    # )

    error_element = numpy.sqrt(
        topo.integrate_elementwise(' ( (  Re ( residual_i residual_i ) / nelems^2 )   ) dV ' @ ns, degree = 4 * degree, arguments = argsError) +
        topo.integrate_elementwise(' ( ( ∇_i(u_i) )^2 / Re  ) dV ' @ ns, degree= 2 * degree, arguments=argsError)
    )

    error_pbox = topo._elementwise_pboxwise(error_element)

    print(f"   quadrature took {time.perf_counter() - t0}")
    # error_pbox[error_pbox < error_target] = 0

    # maxpboxNum = topo.pbox_offsets
    if len(topo.pbox_offsets) > topo.MaxL:
        error_pbox = numpy.array(error_pbox)
        error_pbox[topo.pbox_offsets[topo.MaxL-1]:] = 0
    print(f"   quadrature took {time.perf_counter() - t0}")

    t0 = time.perf_counter()
    theta = 0.75
    if topo.len * 4 > 1800:
        theta = 0.75
    print(f"elements : {topo.len * 4}, settings theta {theta}")

    refine_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarking(numpy.square(error_pbox), theta)
    # print(numpy.sum(numpy.array(error_pbox)[refine_pbox]),numpy.sum(error_pbox))

    # print(error_pbox)
    # print("refine", refine_pbox)
    print(f"   marking took {time.perf_counter() - t0}")
    return refine_pbox, max(error_pbox), error_element

def initial_solve(topo):
    ns, args = Setup_ns(topo)
    res, cons, ns, args0 = setup_residual(topo)

    args1,info = solver.newton('u:v,p:q', res, arguments = args | args0, constrain=cons, **solverargs).solve_withinfo(tol=error_target)

    return args1, ns, (info.niter, len(ns.vbasis) + len(ns.pbasis))
def iterate_solve(topo, nsOld, argsOld, old_sol = None, old_basis = None, force_solve = False, refine_pbox = None):


    if not force_solve:
        t0 = time.perf_counter()
        topo_new = topo.refined_by_pbox(refine_pbox)
        print(f'refining pbox took {time.perf_counter()-t0}')
    else:
        topo_new = topo

    res, cons, nsNew, args0 = setup_residual(topo_new, solve=False, old_sol=old_sol, old_basis=old_basis)
    if not force_solve:
        argsProj = project(topo, nsOld, topo_new, nsNew, argsOld)
    else:
        argsProj = argsOld

    t0 = time.perf_counter()
    # solverargs = {'linsolver':'gmres','linrtol':error_target,'linatol':error_target}
    args1, info = solver.newton('u:v,p:q', res, arguments = args0 | argsProj, constrain=cons, **solverargs).solve_withinfo(tol=error_target)
    print(f'newton took {time.perf_counter()-t0}')
    t0 = time.perf_counter()
    refine_pbox, max_error, elem_error = calculate_error(topo_new, nsNew, args1)
    print(f'error calculation took {time.perf_counter()-t0}')
    return args1, topo_new, nsNew, max_error, refine_pbox, (info.niter, len(nsNew.vbasis) + len(nsNew.pbasis)), elem_error

def project(topo_old, nsOld, topo_new, nsNew, ArgsOld):
    print('start projection')
    t0 = time.perf_counter()
    argsProj = {}
    method = 'thb'
    solve_method = 'QR'

    if topo_old.len > topo_new.len:
        # coarsening
        topo_coarse = None
        topo_refined = topo_old

        basis_p_coarse = None
        basis_vx_coarse = None
        basis_vy_coarse = None
        basis_p_refined = nsOld.pbasis
        basis_vx_refined = nsOld.vxbasis
        basis_vy_refined = nsOld.vybasis
    else:
        # refinement
        topo_coarse = topo_old
        topo_refined = None

        basis_p_coarse = nsOld.pbasis
        basis_vx_coarse = nsOld.vxbasis
        basis_vy_coarse = nsOld.vybasis
        basis_p_refined = None
        basis_vx_refined = None
        basis_vy_refined = None

    argsProj['p'] = warmstart*topo_new.project(func='p' @ nsOld, onto=nsNew.pbasis, geometry=geom, method=method,bern_degree=(degree - 1, degree - 1), solve_method=solve_method, topo_old=topo_old, coarse_topology = topo_coarse, coarse_basis = basis_p_coarse, refined_basis = basis_p_refined, arguments=ArgsOld)[0]
    # projecting the u components seperately
    # requires us to retrieve the correct coeffs from ArgsOld
    lenU = len(nsOld.vxbasis)
    coeff_u_0 = ArgsOld['u'][:lenU]
    coeff_u_1 = ArgsOld['u'][lenU:]

    print(len(coeff_u_0))
    print(len(coeff_u_1))

    vx = warmstart*topo_new.project(func='u_0' @ nsOld, onto=nsNew.vxbasis, geometry=geom, method=method, bern_degree=(degree, degree - 1), solve_method=solve_method, topo_old=topo_old, coarse_topology = topo_coarse, coarse_basis = basis_vx_coarse, refined_basis = basis_vx_refined, arguments=ArgsOld, coeffs = coeff_u_0)[0]
    vy = warmstart*topo_new.project(func='u_1' @ nsOld, onto=nsNew.vybasis, geometry=geom, method=method, bern_degree=(degree - 1, degree), solve_method=solve_method, topo_old=topo_old, coarse_topology = topo_coarse, coarse_basis = basis_vy_coarse, refined_basis = basis_vy_refined, arguments=ArgsOld, coeffs = coeff_u_1)[0]
    argsProj['u'] = numpy.hstack([vx, vy])  # combine into one variable
    print(f'finished projection in {time.perf_counter() - t0 :.2f} seconds')
    # print(argsProj)


    if False: # plotting test
        fig, axs = plt.subplots(3,3)

        bezier_new = topo_new.topology.sample('bezier', 4 * degree)
        bezier_old = topo_old.topology.sample('bezier', 4 * degree)
        x, p, vx, vy = bezier_new.eval(['x_i', 'p', 'u_0', 'u_1'] @ nsNew, **argsProj)
        p_old, vx_old, vy_old = bezier_new.eval(['p', 'u_0', 'u_1'] @ nsOld, **ArgsOld)
        export.triplot(axs[0,0], x, p-p_old, tri=bezier_new.tri, hull=bezier_new.hull)
        # export.triplot('test_p.png', x, p-p_old, tri=bezier_new.tri, hull=bezier_new.hull)
        export.triplot(axs[0,1], x, vx-vx_old, tri=bezier_new.tri, hull=bezier_new.hull)
        # export.triplot('test_vx.png', x, vx-vx_old, tri=bezier_new.tri, hull=bezier_new.hull)
        export.triplot(axs[0,2], x, vy-vy_old, tri=bezier_new.tri, hull=bezier_new.hull)
        export.triplot(axs[1,0], x, p, tri=bezier_new.tri, hull=bezier_new.hull)
        # export.triplot('test_p.png', x, p-p_old, tri=bezier_new.tri, hull=bezier_new.hull)
        export.triplot(axs[1,1], x, vx, tri=bezier_new.tri, hull=bezier_new.hull)
        # export.triplot('test_vx.png', x, vx-vx_old, tri=bezier_new.tri, hull=bezier_new.hull)
        export.triplot(axs[1,2], x, vy, tri=bezier_new.tri, hull=bezier_new.hull)
        # export.triplot('test_vy.png', x, vy-vy_old, tri=bezier_new.tri, hull=bezier_new.hull)
        # fig.colorbar(im, cax=axs[0,2], orientation='vertical')

        x, p, vx, vy = bezier_old.eval(['x_i', 'p', 'u_0', 'u_1'] @ nsOld, **ArgsOld)
        export.triplot(axs[2,0], x, p, tri=bezier_old.tri, hull=bezier_old.hull)
        export.triplot(axs[2,1], x, vx, tri=bezier_old.tri, hull=bezier_old.hull)
        export.triplot(axs[2,2], x, vy, tri=bezier_old.tri, hull=bezier_old.hull)

        plt.show()



    return argsProj

def AdaptiveRefine(topo, ns, Args, old_sol = None, old_basis = None, force_solve = False, newton_iter = 0):
    _, _, ns, args0 = setup_residual(topo, solve=False, old_sol=old_sol, old_basis=old_basis)
    Args['nelems'] = args0['nelems']
    # Args = Args | args0
    if old_sol is not None:
        Args['uold'] = old_sol

    max_error = numpy.infty
    t0 = time.perf_counter()
    refine_pbox, max_error, elem_error = calculate_error(topo, ns, Args)
    print(f'error calculation took {time.perf_counter() - t0}')

    plotting(topo, Args, ns, elem_error,0, newton_iter)
    # plt.show()

    i = 1
    if force_solve: max_error = numpy.infty
    # print(len(ns.nelemsbasis), len(Args['nelems']))
    newton_iter_list = []
    while max_error > error_target:
        t0 = time.perf_counter()
        Args, topo, ns, max_error, refine_pbox, newton_iter, elem_error = iterate_solve(topo, ns, Args, old_sol, old_basis, force_solve = force_solve, refine_pbox=refine_pbox)
        print(f'reduced max error to {max_error} in {time.perf_counter() - t0} seconds')
        newton_iter_list.append(newton_iter)
        force_solve = False
        # print(f'max error {max_error}')


        plotting(topo, Args, ns, elem_error,i, newton_iter[0])
        # plt.show()

        i += 1
        # if i > 2:
        #     break
    return Args, topo, ns, max_error, newton_iter_list, elem_error




iterations = [[] for _ in range(1)]

print(f'########## {0}/{1} ##########')
t0 = time.perf_counter()
args1, ns, newton_iter = initial_solve(topo)
iterations[0].append(newton_iter)
args1, topo, ns, max_error, newton_iter_list, elem_error = AdaptiveRefine(topo, ns, args1, newton_iter = newton_iter[0])
print(f'solving time step {0} took {time.perf_counter()-t0} seconds')
for item in newton_iter_list:
    iterations[0].append(item)

print(iterations)
plotting(topo, args1, ns, elem_error,'final',iterations[0][-1][0])
# plt.show()


quit()

name = lambda x : f'Images/TODELETE_Image_SlowCoarse_NoWarmStart_{x}.png'

plotting(topo, args1, ns)
plt.savefig(name(0))
plt.close('all')



for i in range(N-1):
    print(f'########## {i+1}/{N} ##########')
    if i == 0:
        old_basis = ns.vbasis
        old_sol = args1['u']
    else:
        old_basis = function.vectorize([vxbasis,vybasis])
        old_sol = args1['u']

    args1['u'] = warmstart*args1['u']
    args1['p'] = warmstart*args1['p']

    t0 = time.perf_counter()
    args1, topo_refined, ns, max_error, newton_iterations = AdaptiveRefine(topo, ns, args1, old_sol=old_sol, old_basis=old_basis, force_solve = True)
    print(f'solving time step {i+1} took {time.perf_counter()-t0} seconds')
    iterations[i+1] = newton_iterations

    plotting(topo_refined, args1, ns)

    plt.savefig(name(i+1))
    plt.close('all')


    error_pbox = numpy.sqrt( topo_refined.integrate_pboxwise(' ( (  Re ( residual_i residual_i ) / nelems^2 ) + ( ∇_i(u_i) )^2 / Re  ) dV ' @ ns, degree = 4 * degree, arguments = args1))
    error_pbox[error_pbox > error_target] = error_target # these elements are at refinement level limit, and thus their error is uncapped. Messing with marking


    coarse_pbox = AdaptiveRefinement.AdaptiveRefinement(topo_refined).DorflerMarkingCoarsening(numpy.square(error_pbox), 1e-2)

    # print(topo_refined.pbox_active_indices_per_level)
    # print('coarsening',coarse_pbox)

    topo_coarse = topo_refined.coarsen_by_pbox(coarse_pbox)

    pbasis = topo_coarse.basis(degree=(degree-1,degree-1))
    vxbasis = topo_coarse.basis(degree=(degree,degree-1))
    vybasis = topo_coarse.basis(degree=(degree-1,degree))

    # argsProj = {}
    method = 'thb'
    solve_method = 'MoorePenrose'
    args1['p'] = topo_coarse.project(func='p' @ ns, onto=pbasis, geometry=geom,method=method, bern_degree=(degree - 1, degree - 1),  arguments=args1, refined_topology=topo_refined, refined_basis = ns.pbasis)[0]
    # projecting the u components seperately
    vx = topo_coarse.project(func='u_0' @ ns, onto=vxbasis, geometry=geom, method=method,bern_degree=(degree, degree - 1), arguments=args1, refined_topology=topo_refined, refined_basis = ns.vxbasis)[0]
    vy = topo_coarse.project(func='u_1' @ ns, onto=vybasis, geometry=geom, method=method,bern_degree=(degree - 1, degree), arguments=args1, refined_topology=topo_refined, refined_basis = ns.vybasis)[0]
    args1['u'] = numpy.hstack([vx, vy])  # combine into one variable
    # args1['uold'] = args1['u']  # combine into one variable

    topo = topo_coarse

    _, _, nsNew, _ = setup_residual(topo, solve=False, old_sol=None, old_basis=None)

    plotting(topo, args1, nsNew)

    plt.savefig(f'projected_{i+1}.png')
    plt.close('all')


print(iterations)