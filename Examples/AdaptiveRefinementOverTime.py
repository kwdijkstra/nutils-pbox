import os

import numpy
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export
from nutils_pbox import AdaptiveRefinement, mesh_pbox
from nutils_pbox.debug_logger import data_logger


def Plot(axs, NAME:str, topology, geometry, basis, func, degree, args):
    ns = Namespace()
    ns.x = geometry
    ns.PI = numpy.pi
    ns.basis = basis
    # ns.add_field('phi', ns.basis)
    # ns.fun = func
    bezier = topology.sample('bezier', 4 * max(degree))
    x, zeroFunc = bezier.eval(['x_i', '0'] @ ns, **args)
    export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
    axs.set_title(f'{NAME}')


def PrintIterativeTopologies(name : str, topology_list, geometry, error_list, dofs_list):

    path = os.getcwd()+f"\\OutputFiles\\{name}"
    try: os.mkdir(path)
    except:
        print("dir name already exists")
    ns = Namespace()
    ns.x = geometry
    fig, axs = plt.subplots(1, 1)
    for i in range(len(topology_list)):
        topology = topology_list[i]
        bezier = topology.sample('bezier', 4 * max(degree))
        x, zeroFunc = bezier.eval(['x_i', '0'] @ ns, **args)
        export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
        axs.set_aspect('equal')
        plt.savefig(path+f"\\image_{i}",dpi=600)
        axs.clear()

    N = len(error_list)
    fig,axs  = plt.subplots(1, 1)
    axs.loglog(dofs_list,error_list)
    axs.set_title(f'error reduction over iterations')
    axs.set_xlabel('iterations')
    axs.set_ylabel('log error')
    axs.set_aspect('equal')
    plt.savefig(path + f"\\error_convergence", dpi=600)


r_elems = (5, 5)
degree  = (2, 2)

theta = 0.25
theta_c = 0.1

ElemProj = "Bern"

admissibility_class = 2
eps_target = 5e-4
MaxL = 4

steps = 5
max_offset = 0.2
offset_list = numpy.linspace(-max_offset, max_offset, steps)
offset_list = [f" {str(offset).replace('-','- ')} " if offset < 0 else f" + {offset} " for offset in offset_list]

print(offset_list)
# quit()
# offset_list = [" - 0.2 "," - 0.1 ", " + 0 ", " + 0.1 ", " + 0.2 "]
# offset = 0.2
func = f'1 - tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.25 ) / ( 0.05 sqrt(2) )  )'

fig, axs = plt.subplots(1,len(offset_list))


# refine_type = ['nutils_pbox-local','nutils_pbox-global','mesh']
refine_type = ['nutils_pbox-local']
#

args = {"max-iterations":30,"preserve-iterations":True, "preserve-error":True}
# args = {}
pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL = MaxL)
extra_output = data_logger()

func_array = [f'1 - tanh( ( sqrt( ( 2 ( x_0 {offset} ) - 1)^2 + ( 2 ( x_1 {offset} ) - 1 )^2  ) - 0.25 ) / ( 0.05 sqrt(2) )  )' for offset in offset_list]
print(func_array)

output = data_logger()
coeffs = AdaptiveRefinement.AdaptiveRefinement(pbox, **args).Project(func_array, geometry, eps_target, theta, theta_c, extra_output=output)

print(output.data)

for i in range(steps):
    topology = output.data['topology'][i]
    Plot(axs[i],"", topology, geometry, topology.basis('th-spline', degree=degree), func, degree, {})

plt.show()

quit()


for i, offset in enumerate(offset_list):
    func = f'1 - tanh( ( sqrt( ( 2 ( x_0 {offset} ) - 1)^2 + ( 2 ( x_1 {offset} ) - 1 )^2  ) - 0.25 ) / ( 0.05 sqrt(2) )  )'

    name = f'PboxLocal_degree_{max(degree)}_admis_{admissibility_class}_error_target_{eps_target:.1e}_dorfler_theta_{theta}'
    print(f"Start adaptive refinement")
    coeffs = AdaptiveRefinement(pbox, **args).Project_iter_refine(func, eps_target, theta = theta,extra_output=extra_output)

    args_class = {'phi' : coeffs}
    print(f"plotting")
    Plot(axs[0][i],'Pbox : Loc', pbox.topology, pbox.geometry, pbox.basis(), func, degree, args_class)

    print(f"start coarsening step")
    _, _, _, error, _, _ = AdaptiveRefinement(pbox, **args).ErrorIteration(args_class, func)
    # elements_to_coarsen = numpy.where(error <= eps_target/1e2)[0].astype(int)

    elements_to_coarsen = AdaptiveRefinement(pbox, **args).DorflerMarkingCoarsening(error, theta_c)
    # print(elements_to_coarsen)

    pbox.coarsen_by_pbox(elements_to_coarsen)

    print(f"plotting")
    Plot(axs[1][i], 'Pbox : Loc', pbox.topology, pbox.geometry, pbox.basis(), func, degree, args_class)
    axs[0][i].set_title(f"Iteration : {i}\nAdaptive refinement")
    axs[1][i].set_title(f"Iteration : {i}\nCoarsening")



    # topology_list = extra_output['iterative_topologies']
    # print(len(topology_list))
    #
    # PrintIterativeTopologies(name = name, topology_list = topology_list, geometry=nutils_pbox.geometry, error_list=extra_output['iterative_error'], dofs_list = extra_output['iterative_dofs'])



plt.show()


