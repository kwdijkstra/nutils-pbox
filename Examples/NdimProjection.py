import numpy
from nutils.expression_v2 import Namespace
import time
from nutils_pbox import mesh_pbox

ndim = 4

print(f"solving ndim {ndim}")

r_elems = [2] * ndim
degree  = [2] * ndim


func = ' '.join([f"sin( {numpy.pi} ( 1 - x_{i} ) ) " for i in range(ndim)])

t0 = time.perf_counter()

pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=2)
pbox.refined_by_pbox([0])

print(f'setup time {time.perf_counter() - t0}')

ns = Namespace()
ns.x = geometry
t1 = time.perf_counter()
ns.basis = pbox.basis('th-spline', degree = degree)
print(f'setup basis {time.perf_counter() - t1}')


ns.fun = func



args = {'phi' : pbox.project(ns.fun,  onto = ns.basis, geometry = geometry, method = 'thb')[0]}


ns.add_field('phi', ns.basis)
bezier = pbox.sample('bezier', max(degree)+1)
error = bezier.eval(['phi - fun'] @ ns, **args)
print(f"max error over full domain : {max(abs(error[0]))}")
# fig, axs = plt.subplots()
# export.triplot(axs, x, approx, tri=bezier.tri, hull=bezier.hull)
# plt.show()

print(f"took {time.perf_counter() - t0}")
