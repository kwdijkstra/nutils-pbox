from numba import types
from numba.experimental import jitclass

import numpy as np

from typing import Sequence

float_array = types.float64[:]
float_matrix = types.float64[:, :]
float_tensor = types.float64[:, :, :]
int64_array = types.int64[:]
int32_array = types.int32[:]


@jitclass([('vals', int64_array),
           ('indices', int64_array)])
class FastDictInt64Array(object):

  def __init__(self, vals, indices):
    self.vals = vals
    self.indices = indices

  def __getitem__(self, key):
    return self.vals[self.indices[key]: self.indices[key+1]]

  def get_multiple(self, keys):
    # get length of every key entry in an array
    lengths = np.empty((len(keys),), dtype=np.int64)
    for i,key in enumerate(keys):
      lengths[i] = self.nentries(key)

    # pre allocate return array
    tot_length = lengths.sum()
    ret = np.zeros((tot_length,), dtype=np.int64)

    # loop over lenghts / keys and fill ret array
    offset = 0
    for i, mylength in enumerate(lengths):
      ret[offset: offset + mylength] = self[keys[i]]
      offset += mylength

    # return unique elements
    return np.unique(ret)

  def nentries(self, key):
    return self.indices[key + 1] - self.indices[key]

  @property
  def length(self):
    return len(self.indices) - 1

@jitclass([('vals', float_array),
           ('indices', int64_array)])
class FastDictFloatArray(object):

  def __init__(self, vals, indices):
    self.vals = vals
    self.indices = indices

  def __getitem__(self, key ):
    return self.vals[self.indices[key]: self.indices[key+1]]

  def get_multiple(self, key):
    myarrays = []
    lengths = np.empty((len(key),), dtype=np.int64)
    for i in key:
      myarray = self[i]
      myarrays.append(myarray)
      lengths[i] = len(myarray)

    tot_length = lengths.sum()

    ret = np.zeros((tot_length,), dtype=np.float64)

    offset = 0
    for i, mylength in enumerate(lengths):
      ret[offset: offset + mylength] = myarrays[i]
      offset += mylength

    return np.unique(ret)

  def nentries(self, index):
    return self.indices[index + 1] - self.indices[index]

  @property
  def length(self):
    return len(self.indices) - 1

@jitclass([('vals', float_matrix),
           ('indices', int64_array)])
class FastDictFloatMatrix(object):

  def __init__(self, vals, indices):
    self.vals = vals
    self.indices = indices

  def __getitem__(self, key ):
    return self.vals[self.indices[key]: self.indices[key+1],:]

  def get_multiple(self, key):
    myarrays = []
    lengths = np.empty((len(key),), dtype=np.int64)
    for i in key:
      myarray = self[i]
      myarrays.append(myarray)
      lengths[i] = len(myarray)

    tot_length = lengths.sum()

    ret = np.zeros((tot_length,), dtype=np.float64)

    offset = 0
    for i, mylength in enumerate(lengths):
      ret[offset: offset + mylength] = myarrays[i]
      offset += mylength

    return np.unique(ret)

  def nentries(self, index):
    return self.indices[index + 1] - self.indices[index]

  @property
  def length(self):
    return len(self.indices) - 1

@jitclass([('vals', float_tensor),
         ('indices', int64_array)])
class FastDictFloatTensor(object):

  def __init__(self, vals, indices):
      self.vals = vals
      self.indices = indices

  def __getitem__(self, key):
      return self.vals[self.indices[key]: self.indices[key + 1], :]

  def get_multiple(self, key):
      myarrays = []
      lengths = np.empty((len(key),), dtype=np.int64)
      for i in key:
          myarray = self[i]
          myarrays.append(myarray)
          lengths[i] = len(myarray)

      tot_length = lengths.sum()

      ret = np.zeros((tot_length,), dtype=np.float64)

      offset = 0
      for i, mylength in enumerate(lengths):
          ret[offset: offset + mylength] = myarrays[i]
          offset += mylength

      return np.unique(ret)

  def nentries(self, index):
      return self.indices[index + 1] - self.indices[index]

  @property
  def length(self):
      return len(self.indices) - 1

def from_list_of_arrays(list_of_arrays: Sequence[np.ndarray]):
    list_of_arrays = list(map(np.asarray, list_of_arrays))

    lengths = np.asarray(list(map(len, list_of_arrays)),dtype=np.int64)

    indices = np.array([0, *lengths]).cumsum()

    arr0 = list_of_arrays[0]
    if arr0.ndim == 1:
        if np.issubdtype(arr0.dtype, np.int64):
            return_type = FastDictInt64Array
        elif np.issubdtype(arr0.dtype, np.int32):
            # standard length if int for windows is 32 bit, cast these to 64 bit
            list_of_arrays = list(map(lambda x : np.asarray(x,dtype=np.int64), list_of_arrays))
            return_type = FastDictInt64Array
        else:
            raise NotImplementedError
            # return_type = FastDictFloatArray
    elif arr0.ndim == 2:
        # raise NotImplementedError
      return_type = FastDictFloatMatrix
    else:
        raise TypeError

    return return_type(np.concatenate(list_of_arrays, axis=0), indices)
