import numpy

import nutils_pbox.topology_pbox
from nutils_pbox import tools_nutils
from nutils_pbox.tools_nutils import refine_Vec
import itertools
import bisect

import time



def index_mapping(refined_indices, coarsened_indices, base_counts):
    '''Given two hierarchical index lists, that originate from the same base tensor mesh, return the mapping between the coarse and refined

    inputs :
    - refined_indices
    - coarse indices  (must be strictly coarser than the refined indices)
    - base_counts     (the number of elements in each direction for the lowest level)

    outputs :
    - elem_mapping      (list indexed by the refined elements, and contains a mapping to the same coarse element (None if no such element exists) )
    - elem_mapping_inv  (list indexed by the coarse elements, and contains a mapping to the same refined element (None if no such element exists) )
    '''


    coarse_offsets = numpy.cumsum([0, *map(len, coarsened_indices)], dtype=int)
    refined_offsets = numpy.cumsum([0, *map(len, refined_indices)], dtype=int)

    numCoarse = numpy.sum([len(indices) for indices in coarsened_indices])

    def recursive_mapper(ielem_level, level, tomap_indices, tomap_offsets):
        if ielem_level in tomap_indices[level]:
            return [numpy.where(ielem_level == tomap_indices[level])[0][0] + tomap_offsets[level]]
        else:
            # ielem is refined in the oiginal mapping, hence search for refined ielems
            ielem_level_vec = tools_nutils.map_index_vec(ielem_level, base_counts * (2 ** (level)))
            ielem_refined_vec = refine_Vec(ielem_level_vec)
            ielem_refined_index = tools_nutils.map_vec_index(ielem_refined_vec, base_counts * (2 ** (level + 1)))

            return numpy.concatenate([recursive_mapper(ielem_refined, level + 1, tomap_indices, tomap_offsets) for ielem_refined in ielem_refined_index])

    elem_mapping_inv = [[] for _ in range(numCoarse)]

    for ielem_coarse in range(numCoarse):
        level = numpy.sum(coarse_offsets <= ielem_coarse) - 1
        ielem_level = coarsened_indices[level][ielem_coarse - coarse_offsets[level]]



        elem_mapping_inv[ielem_coarse] = numpy.sort(recursive_mapper(ielem_level, level, refined_indices, refined_offsets))
        if len(elem_mapping_inv[ielem_coarse]) != 1:
            elem_mapping_inv[ielem_coarse] = None

    numFine = numpy.sum([len(indices) for indices in refined_indices])

    elem_mapping = [ None for _ in range(numFine)]

    for ielem_coarse,ielem_refined_list in enumerate(elem_mapping_inv):
        if ielem_refined_list is not None:
            elem_mapping[ielem_refined_list[0]] = ielem_coarse

    return elem_mapping, elem_mapping_inv


def split_projection_refinement(pbox_refined, pbox_coarse, basis_refined, basis_coarse, coeffs_coarse):
    if len(pbox_coarse.active_pboxes_per_level) == 1:
        # if pbox_coarse is a tensor produce mesh. Then, pbox_coarse._indiced_per_level does not exist.
        elem_mapping, _ = index_mapping(pbox_refined.nutils_topology._indices_per_level, [list(range(numpy.prod(pbox_coarse.pbox_count * pbox_coarse.degree)))], pbox_coarse.pbox_count * pbox_coarse.degree)
        pbox_mapping, _ = index_mapping(pbox_refined.active_pboxes_per_level, [list(range(numpy.prod(pbox_coarse.pbox_count)))], pbox_coarse.pbox_count)
    else:
        elem_mapping, _ = index_mapping(pbox_refined.nutils_topology._indices_per_level, pbox_coarse.nutils_topology._indices_per_level, pbox_coarse.pbox_count * pbox_coarse.degree)
        pbox_mapping, _ = index_mapping(pbox_refined.active_pboxes_per_level, pbox_coarse.active_pboxes_per_level, pbox_coarse.pbox_count)

    # print(elem_mapping) # elem mapping is index by refined elements and represents the associated coarse element that is equal

    # simple mapping


    # dofs_mapping = [basis_coarse.get_dofs(e) if e is not None else None for e in elem_mapping] # generate a list indexed by refined elements, and that contains the dofs of the coarse basis

    # generate macro elements
    t0 = time.perf_counter()

    pbox_refined.GenerateProjectionElement()

    # for refined pboxes that no mapping exists in pbox_mapping, collect their refined elements
    elem_non_trivial = numpy.concatenate([pbox_refined.proj_elem[ipbox] for ipbox, p in enumerate(pbox_mapping) if p is None])

    splits = numpy.searchsorted(elem_non_trivial, pbox_refined.nutils_topology._offsets, side='left')

    pbox_non_trivial = []
    def unique_insert(list, elem):
        i = bisect.bisect_left(list, elem)
        if len(list) == i:
            list.insert(i, elem)
        elif list[i] != elem:
            list.insert(i, elem)

    # loop over refined non-trivial elements
    for ilevel, (start, stop) in enumerate(zip(splits[:-1], splits[1:])):
        if start == stop:
            continue
        elem_non_trivial_ilevel = elem_non_trivial[start:stop]

        macro_cutoff = pbox_refined.pbox_offsets[ilevel]

        # get refined dofs in the elements
        dofs = []
        for ielem in elem_non_trivial_ilevel:
            for dof in basis_refined.get_dofs(ielem):
                unique_insert(dofs, dof)

        # get supported elements by dofs
        supp = []
        for dof in dofs:
            for sup in basis_refined.get_support(dof):
                unique_insert(supp, sup)

        # get supported pboxes by the dofs
        macro_supp = []
        for supp_elem in supp:
            unique_insert(macro_supp, pbox_refined.elem_proj[supp_elem])

        # add these pboxes the non-trivial pboxes
        for macro in macro_supp:
            if macro >= macro_cutoff:
                unique_insert(pbox_non_trivial, macro)


    # loop over the non-trivial pboxes
    pbox_non_trivial_elements = itertools.cycle(pbox_non_trivial)
    next_non_trivial = next(pbox_non_trivial_elements)

    # loop over the pboxes, and if it is not a non-trivial pbox, add it to the trivial pboxes
    pbox_trivial = []
    dofs_trivial_coarse = []
    for ipbox in range(pbox_refined.proj_count):
        if ipbox != next_non_trivial:
            # add this pbox to the trivial pboxes.
            # check if the number of dofs stay the same
            ipbox_coarse = pbox_mapping[ipbox]

            ielems_refined = pbox_refined.proj_elem[ipbox]
            dofs_refined = []
            for ielem in ielems_refined:
                for dof in basis_refined.get_dofs(ielem):
                    unique_insert(dofs_refined, dof)
            # dofs_refined = numpy.unique( numpy.concatenate([ basis_refined.get_dofs(ielem) for ielem in ielems_refined ]) )

            ielems_coarse = pbox_coarse.proj_elem[ipbox_coarse]
            dofs_coarse = []
            for ielem in ielems_coarse:
                for dof in basis_coarse.get_dofs(ielem):
                    unique_insert(dofs_coarse,dof)
            # dofs_coarse = numpy.unique(numpy.concatenate([basis_coarse.get_dofs(ielem) for ielem in ielems_coarse]))

            assert len(dofs_refined) == len(dofs_coarse) # check for consistency between the bases over the trivial pbox ipbox
            pbox_trivial.append(ipbox)
            dofs_trivial_coarse.append(dofs_coarse)
        else:
            next_non_trivial = next(pbox_non_trivial_elements)


    macro_coeffs_trivial = [coeffs_coarse[dofs] for dofs in dofs_trivial_coarse]
    # print(macro_coeffs_trivial)

    pbox_splitting = (pbox_trivial, pbox_non_trivial)

    return {"pbox_splitting": pbox_splitting, "macro_coeffs_trivial": macro_coeffs_trivial}


def split_projection_coarsening(pbox_refined, pbox_coarse, basis_refined, basis_coarse, coeffs_refined):
    _, elem_mapping = index_mapping(pbox_refined.topology._indices_per_level, pbox_coarse.topology._indices_per_level, pbox_coarse.PboxBaseTopology.pbox_count * pbox_coarse.PboxBaseTopology.degree)
    _, pbox_mapping = index_mapping(pbox_refined.active_pboxes_per_level, pbox_coarse.active_pboxes_per_level, pbox_coarse.PboxBaseTopology.pbox_count)

    dofs_mapping = [basis_refined.get_dofs(e) if e is not None else None for e in elem_mapping]

    pbox_coarse.GenerateProjectionElement()

    elem_non_trivial = numpy.concatenate([pbox_coarse.proj_elem[ipbox] for ipbox, p in enumerate(pbox_mapping) if p is None])
    splits = numpy.searchsorted(elem_non_trivial, pbox_coarse.topology._offsets, side='left')

    pbox_non_trivial = []

    def unique_insert(list, elem):
        i = bisect.bisect_left(list, elem)
        if len(list) == i:
            list.insert(i, elem)
        elif list[i] != elem:
            list.insert(i, elem)

    for ilevel, (start, stop) in enumerate(zip(splits[:-1], splits[1:])):
        if start == stop:
            continue
        elem_non_trivial_ilevel = elem_non_trivial[start:stop]

        macro_cutoff = pbox_coarse.pbox_offsets[ilevel]

        dofs = []
        for ielem in elem_non_trivial_ilevel:
            for dof in basis_coarse.get_dofs(ielem):
                unique_insert(dofs, dof)

        supp = []
        for dof in dofs:
            for sup in basis_coarse.get_support(dof):
                unique_insert(supp, sup)

        macro_supp = []
        for supp_elem in supp:
            unique_insert(macro_supp, pbox_coarse.elem_proj[supp_elem])

        for macro in macro_supp:
            if macro >= macro_cutoff:
                unique_insert(pbox_non_trivial, macro)

    pbox_non_trivial_elements = itertools.cycle(pbox_non_trivial)
    next_non_trivial = next(pbox_non_trivial_elements)

    pbox_trivial = []
    for ipbox in range(pbox_coarse.proj_count):
        if ipbox != next_non_trivial:
            pbox_trivial.append(ipbox)
        else:
            next_non_trivial = next(pbox_non_trivial_elements)

    macroelems_trivial = [sorted(pbox_coarse.proj_elem[i]) for i in pbox_trivial]
    macro_coeffs_trivial = []

    for ielems_trivial in macroelems_trivial:
        macro_dofs_old = numpy.unique(numpy.concatenate([dofs_mapping[ielem] for ielem in ielems_trivial]))
        macro_coeffs_trivial.append(coeffs_refined[macro_dofs_old])

    pbox_splitting = (pbox_trivial, pbox_non_trivial)

    return {"pbox_splitting": pbox_splitting, "macro_coeffs_trivial": macro_coeffs_trivial}