class data_logger():
    def __init__(self):
        self.data = {}

    def add_multiple(self, data:dict):
        for kwarg, data in data.items():
            self.add(kwarg, data)
    def add(self, kwarg:str, data):
        # add data to list
        if kwarg in self.data.keys():
            self.data[kwarg].append(data)
        else:
            self.data[kwarg] = [data]