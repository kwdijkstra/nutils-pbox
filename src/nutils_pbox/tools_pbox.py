import numpy as np
from THB_coarse_refine_functions import refinement_neigh_list, refine_by_elements


## contains code for pboxes

def adhere_admissibility(element_indices_level, offsets, nelems, degree, m):
    # add in extra level for refinement
    element_indices_level = [np.array(sub_array) for sub_array in element_indices_level] + [[]]

    # elem_index_level = [ np.array(element_indices_level[level][marked_elements[start:stop] - offsets[level]]) if start != stop else np.array([]) for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:]))]

    # for each level, perform algorithm, note that element_indices_level and offsets change for each loop, but, crucially,
    # the algorithm only changes the space for the levels: level and lower.
    L = len(element_indices_level)
    for level in np.arange(L-2,-1,-1):
        # get level - m + 1 parent elements
        if level-m+1 >= 0:
            # level l-m+2
            parent_elem = refinement_neigh_list(nelems, degree, elem_index_level[level], level, m)
            # print(elem_index_level[level-m+1])
            elem_index_level[level-m+1] = np.append(elem_index_level[level-m+1],parent_elem)
            # print(elem_index_level[level-m+1])


        # refine level l elements
        element_indices_level, offsets = refine_by_elements(element_indices_level, offsets, nelems, degree, elem_index_level[level], level)

    # if no extra level is necessary, remove this level
    if len(element_indices_level[-1]) == 0:
        element_indices_level.pop(-1)

    element_indices_level = [np.array(element_indices,dtype=int) for element_indices in element_indices_level]

    return element_indices_level