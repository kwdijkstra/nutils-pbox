import time

import nutils.topology
from nutils import function
import numpy as np
from nutils_pbox.jitclasses import from_list_of_arrays, FastDictFloatMatrix, FastDictFloatArray
from numba import njit, prange
from itertools import product

LOOKUP_TABLE = np.array([
    1, 1, 2, 6, 24, 120, 720, 5040, 40320,
    362880, 3628800, 39916800, 479001600,
    6227020800, 87178291200, 1307674368000,
    20922789888000, 355687428096000, 6402373705728000,
    121645100408832000, 2432902008176640000], dtype='int64')

def PaddedMatrixInverse(mat):
    shape = mat.shape
    # for a given m x n matrix mat, where n > m that is formed from a m x m invertible matrix padded by extra zero columns,
    # calculate the row padded inverse matrix
    active_col = [any(mat[:, i] != 0) for i in range(shape[1])]
    active_row = [i for i in range(shape[0])]

    mat_inv = np.zeros((shape[1],shape[0]))
    mat_inv[np.ix_(active_col, active_row)] = np.linalg.inv(mat[:, active_col])
    return mat_inv

def _nutils_basis_coeffs_to_array(basis):
    basis_elem_wise = list(map(np.asarray, basis._coeffs))
    splits = [len(basis_elem) for basis_elem in basis_elem_wise]
    return np.concatenate(basis_elem_wise,dtype=np.float64), np.concatenate([[0], np.cumsum(splits)])

def _nutils_basis_structured_coeffs_to_nutils_array(structured_basis):
    # structured_basis = [list(map(np.asarray, coeffs)) for coeffs in structured_basis._coeffs]
    #
    # basis_elem_wise = list([np.kron(b0,b1) for b0,b1 in product(structured_basis[0],structured_basis[1])])

    basis_elem_wise = [structured_basis.get_coefficients(e) for e in range(structured_basis.nelems)]

    splits = [len(basis_elem) for basis_elem in basis_elem_wise]
    return np.concatenate(basis_elem_wise,dtype=np.float64), np.concatenate([[0], np.cumsum(splits)])

# def _nutils_coeffs_to_numpy_coeffs(coeffs: np.ndarray):
#     n = int(-1/2 + np.sqrt(0.25 + 2 * coeffs.shape[1]))
#     p = n - 3
#
#     indices = np.asarray([elem for elem in product(range(n), range(n)) if sum(elem) < n], dtype=int)
#
#     array = np.zeros((coeffs.shape[0], n, n),dtype=np.float64)
#     array[:, indices[:, 0], indices[:, 1]] = coeffs[:, ::-1]
#
#     return array[:, :p+1, :p+1]
#
def _numpy_coeffs_to_nutils_coeffs(coeffs: np.ndarray):
    # print(coeffs.shape)
    p = np.int64(np.sqrt(coeffs.shape[-1]))
    n = 2*p -1
    m = int( (n**2 + n) / 2)
    print(p,n,m)

    print(coeffs.shape)

    indices = np.asarray([elem for elem in product(reversed(range(n)), reversed(range(n))) if sum(elem) < n], dtype=int)
    # print(indices)
    active_indices = np.asarray([max(elem)<p for elem in indices],dtype=bool)
    # print(active_indices)




    array = np.zeros((coeffs.shape[0], m),dtype=np.float64)
    array[:, active_indices] = coeffs[:, :]

    return array

# print(_numpy_coeffs_to_nutils_coeffs(np.asarray([[[1,2,3],[2,3,4],[4,5,6]],]*5)))



@njit(cache = True)
def choose(n,i):
    if i < 0 or i > n:
        return 0
    elif i > 20 or n > 20:
        raise ValueError
    else:
        return (LOOKUP_TABLE[n]/( LOOKUP_TABLE[i] * LOOKUP_TABLE[n-i]))
        # return math.comb(n,i)

# print(choose(3,2))

@njit(cache = True)
def BernsteinInvUnivariate(p:int):
    Binv = np.zeros((p+1,p+1))
    for i in range(p+1):
        for j in range(p+1):
            InterSum = 0
            for k in range(p+1):
                InterSum += ( 2 * k + 1 ) * choose( p + k + 1 , p - j)  * choose( p - k,p - j)  * choose( p + k + 1, p - i)  * choose(p - k,p-i)
            Binv[i,j] = (-1)**(i+j) * InterSum / (choose(p,i) * choose(p,j))

    # print(Binv)
    return Binv

@njit()
def BernsteinInv(degree):
    p_list = degree
    Binv = BernsteinInvUnivariate(p_list[0])
    for p in p_list[1:]:
        Binv = np.kron(Binv, BernsteinInvUnivariate(p))
    return Binv

# @njit(parallel=True)
def proj_elem(b, degree, elem_counts):

    Binv = BernsteinInv(degree)
    elem_dofs = np.prod(np.array(degree)+1)

    res = np.zeros((elem_dofs * elem_counts))

    for e in range(elem_counts):
        indices = np.arange(e * elem_dofs,(e+1)*elem_dofs)

        # coeff = b[indices]
        res[indices] = Binv @ b[indices]

    return res

@njit(cache = True)
def subset_mapping(subset, set):
    # given a set and a subset, returns an array the same size as subset, where the entries represent the index within set.
    # e.g., subset[i] = set[subset_mapping[i]]
    subset_mapping = subset.copy()
    k_start = 0
    for i, dof_global in enumerate(subset):
        for k in np.arange(k_start, len(set)):
            if dof_global == set[k]:
                subset_mapping[i] = k
                break
    return subset_mapping

@njit()
def macro_projection(ipbox, macro_arr, dof_arr, elem_dofs, coeff_arr, bernstein_arr, elem_size, pol_extraction_bezier, degree, bernstein_mass_matrix_inv):
    macro_elems = macro_arr[ipbox]
    dofs_macro = dof_arr.get_multiple(macro_elems)

    # setup macro matrices
    extraction_matrix_imacro = np.zeros((elem_dofs * len(macro_elems), len(dofs_macro)), dtype=np.float64)
    b_imacro = np.zeros((elem_dofs * len(macro_elems)), dtype=np.float64)
    w_imacro = np.zeros((len(dofs_macro)), dtype=np.float64)

    for ielem_local, ielem_global in enumerate(macro_elems):
        extraction_matrix_ielem = np.transpose(coeff_arr[ielem_global] @ pol_extraction_bezier)

        dofs_ielem_global = dof_arr[ielem_global]
        dofs_ielem_local = subset_mapping(dofs_ielem_global, dofs_macro)

        dofs_bern_local = np.arange(elem_dofs) + elem_dofs * ielem_local

        # perform elementwise bernstein projection
        # bern_coeff = bernstein_mass_matrix_inv @ ( bern_int[dofs_bern_global] / elem_size[ielem_global] )
        bern_coeff = bernstein_mass_matrix_inv @ (bernstein_arr[ielem_global] / elem_size[ielem_global] )

        # construct relevant macroelementwise data
        for i, db in enumerate(dofs_bern_local):
            b_imacro[db] = bern_coeff[i]

            for j, di in enumerate(dofs_ielem_local):
                extraction_matrix_imacro[db, di] = extraction_matrix_ielem[i, j]

        # calculate weights per macro element
        w_ielem = elem_size[ielem_global] * np.sum(extraction_matrix_ielem, axis=0)
        for j, di in enumerate(dofs_ielem_local):
            w_imacro[di] += w_ielem[j]

    # perform macroelementwise thb-spline projection
    Q, R = np.linalg.qr(extraction_matrix_imacro)
    sol_imacro = np.linalg.solve(R, Q.T @ b_imacro)

    return sol_imacro, w_imacro

@njit(parallel = True)
def global_thb_projection(degree, len_thbb, len_macro, macro_arr, dof_arr, coeff_arr, bernstein_arr, elem_size , pol_extraction_bezier, bernstein_mass_matrix_inv):
    elem_bern_ndofs = np.prod(np.asarray(degree) + 1)

    sol_global = np.zeros((len_thbb), dtype=np.float64)
    w_global = np.zeros((len_thbb), dtype=np.float64)

    for ipbox in prange(len_macro):
        macro_elems = macro_arr[ipbox]
        dofs_macro = dof_arr.get_multiple(macro_elems)

        sol_imacro, w_imacro = macro_projection(ipbox, macro_arr, dof_arr, elem_bern_ndofs, coeff_arr, bernstein_arr, elem_size, pol_extraction_bezier, degree, bernstein_mass_matrix_inv)

        for i, i_global in enumerate(dofs_macro):
            sol_global[i_global] += sol_imacro[i] * w_imacro[i]
            w_global[i_global] += w_imacro[i]

    return sol_global / w_global



def thb_bezier_project(func, onto, topo, bernstein_basis,  geom, proj_elem, integration_degree, thb_degree = None, arguments = {}, func_topo = None, element_sizes = None):
    if func_topo is None: func_topo = topo
    if thb_degree is None: thb_degree = integration_degree
    if element_sizes is not None: assert len(element_sizes) == len(topo)

    # func_topo = topo
    t0 = time.perf_counter()
    bernstein_innerproduct = func_topo.integrate(func * bernstein_basis * function.J(geom), degree = 2*max(integration_degree), arguments=arguments)
    splits = [ e * np.prod(np.asarray(thb_degree)+1) for e in range(len(topo)+1) ]
    bernstein_arr = FastDictFloatArray(bernstein_innerproduct, np.asarray(splits, dtype=np.int64))
    # print(f' > bernstein calculation took {time.perf_counter() - t0}')
    t0 = time.perf_counter()
    if element_sizes is None:
        element_sizes = topo.integrate_elementwise(function.J(geom), degree = 2)
    # print(f' > elem size took {time.perf_counter() - t0}')
    t0 = time.perf_counter()
    pol_extraction_bezier = np.asarray(PaddedMatrixInverse(np.array(bernstein_basis._coeffs[0])))

    # does not work, topo should be the most refined topology to integrate func on. It can thus be different then the topology that onto is defined over.
    if isinstance(topo, nutils.topology.StructuredTopology):
        coeffs, splits = _nutils_basis_structured_coeffs_to_nutils_array(onto)
    else:
        coeffs, splits = _nutils_basis_coeffs_to_array(onto)

    coeff_arr = FastDictFloatMatrix(coeffs, np.asarray(splits, dtype=np.int64))

    # same as above
    if isinstance(topo, nutils.topology.StructuredTopology):
        dofs = [onto.get_dofs(e) for e in range(onto.nelems)]
    else:
        dofs = onto._dofs
    dof_arr = from_list_of_arrays(dofs)
    macro_arr = from_list_of_arrays(proj_elem)

    bernstein_mass_matrix_inv = BernsteinInv(thb_degree).copy(order='C')

    output = global_thb_projection(thb_degree, len(onto), len(proj_elem), macro_arr, dof_arr, coeff_arr, bernstein_arr, element_sizes, pol_extraction_bezier, bernstein_mass_matrix_inv)
    # print(f' > numba calculations took {time.perf_counter() - t0}')
    return output