import numpy
import numpy as np
import nutils.topology
from nutils.expression_v2 import Namespace
from nutils import types
from nutils import function, evaluable, sparse, numeric
from nutils.topology import Topology
import time
import typing
from functools import cached_property
from nutils_pbox.numba_thb_project_qr import thb_bezier_project

def QRinv(A):
    '''Matrix inverse via QR decomposition of a n x m matrix (n <= m)

    This is used to solve least square problems that are poorly conditioned.
    The result is transposed to reform a n x m matrix. This seems to be a limitation of nutils where the result
    needs to be the same size as the original matrix. (Keep this in mind when using the qr inverse)
    '''

    Q, R = numpy.linalg.qr(A)
    return ( numpy.linalg.inv(R) @ Q.T ).T

def qrinverse(arg, axes=(-2, -1)):
    '''Calculates the QR inverse of arg. This function is an altered copy of inverse from nutils.evaluable.inverse'''
    arg = evaluable.asarray(arg)
    if arg.dtype == bool:
        raise ValueError('The boolean inverse is not supported.')
    if arg.dtype == int:
        arg = evaluable.IntToFloat(arg)
    return evaluable.Transpose.from_end(QRInverse(evaluable.Transpose.to_end(arg, *axes)), *axes)


class QRInverse(evaluable.Array):
    '''
    Matrix inverse of ``func`` over the last two axes.  All other axes are
    treated element-wise.
    '''

    def __init__(self, func: evaluable.Array):
        assert isinstance(func, evaluable.Array) and func.ndim >= 2, f'func={func!r}'
        self.func = func
        super().__init__(args=(func,), shape=func.shape, dtype=complex if func.dtype == complex else float)

    def _simplified(self):
        result = self.func._inverse(self.ndim-2, self.ndim-1)
        if result is not None:
            return result
        if evaluable._equals_scalar_constant(self.func.shape[-1], 1):
            return evaluable.reciprocal(self.func)

    evalf = staticmethod(QRinv)

    def _derivative(self, var, seen):
        return -evaluable.einsum('Aij,AjkB,Akl->AilB', self, evaluable.derivative(self.func, var, seen), self)

    def _eig(self, symmetric):
        eigval, eigvec = evaluable.Eig(self.func, symmetric)
        return evaluable.Tuple((evaluable.reciprocal(eigval), eigvec))

    def _determinant(self, axis1, axis2):
        if sorted([axis1, axis2]) == [self.ndim-2, self.ndim-1]:
            return evaluable.reciprocal(evaluable.Determinant(self.func))

    def _take(self, indices, axis):
        if axis < self.ndim - 2:
            return QRInverse(evaluable._take(self.func, indices, axis))

    def _takediag(self, axis1, axis2):
        assert axis1 < axis2
        if axis2 < self.ndim-2:
            return QRinverse(evaluable._takediag(self.func, axis1, axis2), (self.ndim-4, self.ndim-3))

    def _unravel(self, axis, shape):
        if axis < self.ndim-2:
            return QRInverse(evaluable.unravel(self.func, axis, shape))
#
# class QRInverse(evaluable.Array):
#     '''
#     Matrix QRinverse of ``func`` over the last two axes.  All other axes are
#     treated element-wise.
#     This is an altered copy of nutils.evaluable.Inverse.
#     Note that for a n x m matrix, the result is the transposed QR inverse matrix. This is due to a limitation of nutils,
#     where it expects the Inverse matrix to be the same size. (This function is a very simple alteration of the Inverse class)
#     '''
#
#     func: evaluable.Array
#
#     # turn of checks
#     # def __post_init__(self):
#     #     assert isinstance(self.func, evaluable.Array) and self.func.dtype in (float, complex) and self.func.ndim >= 2 and evaluable._equals_simplified(self.func.shape[-1], self.func.shape[-2]), f'func={self.func!r}'
#
#     @property
#     def dependencies(self):
#         return self.func,
#
#     @cached_property
#     def dtype(self):
#         return self.func.dtype
#
#     @cached_property
#     def shape(self):
#         return self.func.shape
#
#     def _simplified(self):
#         if evaluable._equals_scalar_constant(self.func.shape[-1], 1):
#             return evaluable.reciprocal(self.func)
#         if evaluable._equals_scalar_constant(self.func.shape[-1], 0):
#             return evaluable.singular_like(self)
#         result = self.func._inverse(self.ndim-2, self.ndim-1)
#         if result is not None:
#             return result
#
#     # return the QR inv result
#     evalf = staticmethod(QRinv)
#
#     def _derivative(self, var, seen):
#         return -evaluable.einsum('Aij,AjkB,Akl->AilB', self, evaluable.derivative(self.func, var, seen), self)
#
#     def _eig(self, symmetric):
#         eigval, eigvec = evaluable.Eig(self.func, symmetric)
#         return evaluable.Tuple((evaluable.reciprocal(eigval), eigvec))
#
#     def _determinant(self, axis1, axis2):
#         if sorted([axis1, axis2]) == [self.ndim-2, self.ndim-1]:
#             return evaluable.reciprocal(evaluable.Determinant(self.func))
#
#     def _take(self, indices, axis):
#         if axis < self.ndim - 2:
#             return evaluable.Inverse(evaluable._take(self.func, indices, axis))
#
#     def _takediag(self, axis1, axis2):
#         assert axis1 < axis2
#         if axis2 < self.ndim-2:
#             return evaluable.inverse(evaluable._takediag(self.func, axis1, axis2), (self.ndim-4, self.ndim-3))
#
#     def _unravel(self, axis, shape):
#         if axis < self.ndim-2:
#             return evaluable.Inverse(evaluable.unravel(self.func, axis, shape))



def PaddedMatrixInverse(mat):
    shape = mat.shape
    # for a given m x n matrix mat, where n > m that is formed from a m x m invertible matrix padded by extra zero columns,
    # calculate the row padded inverse matrix
    active_col = [any(mat[:, i] != 0) for i in range(shape[1])]
    active_row = [i for i in range(shape[0])]

    mat_inv = numpy.zeros((shape[1],shape[0]))
    mat_inv[numpy.ix_(active_col, active_row)] = numpy.linalg.inv(mat[:, active_col])
    return mat_inv

class ProjectEvaluable():
    # this is going to be the evaluable projection class that does all the projections. This allows us to define
    # sub methods as class functions, preventing repeating code.

    def __init__(self, topo: Topology,
        geom: function.Array,
        thbbasis: typing.Union[function.Basis],
        bernbasis: typing.Union[function.Basis],
        weights: str,
        macroelems: typing.Sequence[np.ndarray],
        fun: function.Array,
        integration_degree: int,
        degree_list: list[int],
        proj_method = 'bern',
        solve_method = 'QR',
        mapping = None,
        refined_topo = None,
        element_sizes = None,
        **arguments: typing.Mapping[str, numpy.ndarray]):

        # checks on macro elements:
        concat_macroelems = numpy.concatenate(macroelems)
        unique_macroelems = numpy.unique(concat_macroelems)
        if len(unique_macroelems) != len(concat_macroelems):
            raise ValueError('duplicate element indices in parameter `macroelems`')
        # if len(unique_macroelems) != len(topo) or not (unique_macroelems == np.arange(len(topo))).all():
        #     raise ValueError('`macroelems` is not a partition of the element indices of `topo`')

        self.topo = topo
        self.geom = geom
        self.thbbasis = thbbasis
        self.bernbasis = bernbasis
        self.weights = weights
        self.macroelems = macroelems
        self.fun = fun
        self.degree = integration_degree
        self.degree_list = degree_list
        self.proj_method = proj_method
        self.solve_method = solve_method
        self.refined_topo = refined_topo
        self.element_sizes = element_sizes

        if arguments['arguments'] is not None:
            self.arguments = arguments['arguments']
        else:
            self.arguments = arguments


        if mapping is not None:
            trivial_macro, non_trivial_macro = mapping['pbox_splitting']
            self.macro_coeffs_trivial = mapping['macro_coeffs_trivial']

        else:
            trivial_macro = []
            non_trivial_macro = numpy.arange(len(self.macroelems))
            self.macro_coeffs_trivial = None


        self.split = not len(trivial_macro) == 0

        # print(non_trivial_macro)

        self.macroelems_nontrivial = [ self.macroelems[i] for i in non_trivial_macro ]
        self.macroelems_trivial    = [ self.macroelems[i] for i in trivial_macro ]


    def project(self, **kwargs):
        '''returns the projected coefficients'''

        # TODO: check if self.method is proper method


        f = getattr(self, 'proj_' + self.proj_method)

        # perform projection and time its execution
        t0 = time.perf_counter()
        output = f(**kwargs)
        t1 = time.perf_counter()

        return output, t1 - t0


    def _setup_macro_loop_index(self):
        '''Generates both the imacro and ielem loop indices.'''
        # note that the ielem loop indices are dependent on the imacro loop indices. Meaning that one should loop over
        # imacro loop indices to be able to reference the ielem loop indices.

        self.macroelems = [sorted(macro) for macro in self.macroelems]

        self.macroelems_nontrivial = [sorted(macro) for macro in self.macroelems_nontrivial]

        self.imacro_nontrivial = evaluable.loop_index('macro_nontrivial', len(self.macroelems_nontrivial))

        self.ielems_nontrivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macroelems_nontrivial)), self.imacro_nontrivial, int)
        self.imacroelem_nontrivial = evaluable.loop_index('elem_nontrivial', self.ielems_nontrivial.shape[0])
        self.ielem_nontrivial = evaluable.Take(self.ielems_nontrivial, self.imacroelem_nontrivial)

        if len(self.macroelems_trivial) > 0:
            self.split = True

            self.macroelems_trivial = [sorted(macro) for macro in self.macroelems_trivial]

            self.imacro_trivial = evaluable.loop_index('macro_trivial', len(self.macroelems_trivial))

            self.ielems_trivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macroelems_trivial)), self.imacro_trivial, int)
            self.imacroelem_trivial = evaluable.loop_index('elem_trivial', self.ielems_trivial.shape[0])
            self.ielem_trivial = evaluable.Take(self.ielems_trivial, self.imacroelem_trivial)
        else:
            self.split = False

    def _setup_elem_loop_index(self):
        '''Generates both the ielem loop index.'''
        self.ielem = evaluable.loop_index('elems', len(self.topo))

    def _setup(self):
        '''Performs general setup of required nutils/evaluable functions'''
        self.smpl = self.topo.sample('gauss', self.degree)
        if self.proj_method == 'discont':
            self._setup_elem_loop_index()
        else:
            self._setup_macro_loop_index()
        # self.lower_args = self.smpl.get_lower_args(self.ielem)
        self.lower_args_nontrivial = self.smpl.get_lower_args(self.ielem_nontrivial)
        if self.split:
            self.lower_args_trivial = self.smpl.get_lower_args(self.ielem_trivial)
        if self.proj_method == 'discont':
            self._setup_elem_integral()
        else:
            self._setup_macro_integral()

    def _setup_macro_integral(self):
        '''Create both the elemwise and macroelemwise integral functions'''
        self.elem_int_nontrivial = lambda integrand: evaluable.einsum('A,AB->B', self.smpl.get_evaluable_weights(self.ielem_nontrivial) * function.jacobian(self.geom, self.topo.ndims).lower(self.lower_args_nontrivial), integrand)
        self.macro_int_nontrivial = lambda integrand: evaluable.loop_sum(self.elem_int_nontrivial(integrand), self.imacroelem_nontrivial)

        if self.split:
            self.elem_int_trivial = lambda integrand: evaluable.einsum('A,AB->B', self.smpl.get_evaluable_weights(self.ielem_trivial) * function.jacobian(self.geom, self.topo.ndims).lower(self.lower_args_trivial), integrand)
            self.macro_int_trivial = lambda integrand: evaluable.loop_sum(self.elem_int_trivial(integrand), self.imacroelem_trivial)

    def _setup_elem_integral(self):
        '''Create the elemwise integral function'''
        self.weights = self.smpl.get_evaluable_weights(self.ielem) * function.jacobian(self.geom, self.topo.ndims).lower(self.lower_args)
        self.elem_int = lambda integrand: evaluable.einsum('A,AB->B', self.weights, integrand)



    def _process_macro_elems(self, basis):
        '''processes macro elements to generate the required dofs per macro element based on 'basis'
        '''
        # Collect all dofs that have support on a macroelement for all
        # macroelements in `macro_dofs` and a mapping of element index to
        # macroelement index in `elem_to_macro`.
        #
        # self.macro_dofs = []
        # self.elem_to_macro2 = [None] * len(self.topo)
        # for ielems in self.macroelems:
        #     dofs = [basis.get_dofs(ielem) for ielem in ielems]
        #     unique = np.unique(np.concatenate(dofs))
        #     self.macro_dofs.append(unique)
        #     for ielem, dofs in zip(ielems, dofs):
        #         self.elem_to_macro2[ielem] = np.searchsorted(unique, dofs)
        #         assert (unique[self.elem_to_macro2[ielem]] == dofs).all()

        self.macro_dofs_nontrivial = []
        self.elem_to_macro = [None] * len(self.topo)
        for ielems_nontrivial in self.macroelems_nontrivial:
            dofs_nontrivial = [basis.get_dofs(ielem_nontrivial) for ielem_nontrivial in ielems_nontrivial]
            unique_nontrivial = np.unique(np.concatenate(dofs_nontrivial))
            self.macro_dofs_nontrivial.append(unique_nontrivial)
            for ielem, dofs_nontrivial in zip(ielems_nontrivial, dofs_nontrivial):
                self.elem_to_macro[ielem] = np.searchsorted(unique_nontrivial, dofs_nontrivial)
                assert (unique_nontrivial[self.elem_to_macro[ielem]] == dofs_nontrivial).all()

        if self.split:
            self.macro_dofs_trivial = []
            self.macro_coeffs = []
            # self.elem_to_macro = [[-1]] * len(self.topo)
            for ielems_trivial in self.macroelems_trivial:
                dofs_trivial = [basis.get_dofs(ielem_trivial) for ielem_trivial in ielems_trivial]
                unique_trivial = np.unique(np.concatenate(dofs_trivial))
                self.macro_dofs_trivial.append(unique_trivial)
                for ielem, dofs_trivial in zip(ielems_trivial, dofs_trivial):
                    self.elem_to_macro[ielem] = np.searchsorted(unique_trivial, dofs_trivial)
                    assert (unique_trivial[self.elem_to_macro[ielem]] == dofs_trivial).all()

            # print(self.macro_dofs_trivial)




    def _macro_elem_weight(self, macro_proj_coeffs_nontrivial, macro_proj_coeffs_trivial):
        '''Performs the global smooting step by performing a weighted averaging.
        Depending on 'weight', the average is calculated weighted by the 'thbbasis' mass over a macro element
        (weight = integral), or an uniform weighting (weight = uniform)
        '''
        # _, thb_basis_coeffs = self.thbbasis.f_dofs_coeffs(self.ielem)
        _, thb_basis_coeffs_nontrivial = self.thbbasis.f_dofs_coeffs(self.ielem_nontrivial)
        if self.split:
            _, thb_basis_coeffs_trivial = self.thbbasis.f_dofs_coeffs(self.ielem_trivial)

        # Multiply the local projection coefficients with the weights for this element.
        # thb_shapes = evaluable.Polyval(thb_basis_coeffs, self.topo.f_coords.lower(self.lower_args))
        # thb_shapes = evaluable.Inflate(thb_shapes, self.elem_to_macro, self.macro_thb_dofs.shape[0])

        thb_shapes_nontrivial = evaluable.Polyval(thb_basis_coeffs_nontrivial, self.topo.f_coords.lower(self.lower_args_nontrivial))
        thb_shapes_nontrivial = evaluable.Inflate(thb_shapes_nontrivial, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])

        if self.split:
            thb_shapes_trivial = evaluable.Polyval(thb_basis_coeffs_trivial, self.topo.f_coords.lower(self.lower_args_trivial))
            thb_shapes_trivial = evaluable.Inflate(thb_shapes_trivial, self.elem_to_macro_trivial, self.macro_thb_dofs_trivial.shape[0])

        if self.weights == 'uniform':
            weights_nontrivial = evaluable.ones(thb_shapes_nontrivial.shape[1:])
        elif self.weights == 'integral':
            weights_nontrivial = self.macro_int_nontrivial(thb_shapes_nontrivial)
            if self.split:
                weights_trivial = self.macro_int_trivial(thb_shapes_trivial)
        else:
            raise ValueError(f'unknown weights method: {self.weights}, supported: uniform, integral')
        # weight calculations
        summed_weights = evaluable.loop_sum(evaluable.Inflate(weights_nontrivial, self.macro_thb_dofs_nontrivial, evaluable.asarray(self.thbbasis.ndofs)), self.imacro_nontrivial)
        if self.split:
            summed_weights += evaluable.loop_sum(evaluable.Inflate(weights_trivial, self.macro_thb_dofs_trivial, evaluable.asarray(self.thbbasis.ndofs)), self.imacro_trivial)

        t0 = time.perf_counter()
        summed_weights_eval = sparse.toarray(evaluable.eval_sparse((summed_weights,))[0])  # eval the actual full weights for each dof
        # print(f'      weights eval took {time.perf_counter() - t0}')

        # scale based on nontrivial weights
        macro_proj_coeffs_nontrivial *= weights_nontrivial
        # Scatter the local projection coefficients to the global coefficients ...
        proj_coeffs_nontrivial = evaluable.Inflate(macro_proj_coeffs_nontrivial, self.macro_thb_dofs_nontrivial, evaluable.asarray(self.thbbasis.ndofs))
        # ... sum over all non trivial macro elements ...
        proj_coeffs = evaluable.loop_sum(proj_coeffs_nontrivial, self.imacro_nontrivial)
        proj_coeffs_nontrivial = proj_coeffs



        if self.split:
            # scale based on trivial weights
            macro_proj_coeffs_trivial = evaluable.multiply(macro_proj_coeffs_trivial, weights_trivial)
            # scatter
            proj_coeffs_trivial = evaluable.Inflate(macro_proj_coeffs_trivial, self.macro_thb_dofs_trivial, evaluable.asarray(self.thbbasis.ndofs))
            # sum over all trivial macro elements
            proj_coeffs_trivial = evaluable.loop_sum(proj_coeffs_trivial, self.imacro_trivial)

            proj_coeffs += proj_coeffs_trivial

            output = [proj_coeffs_nontrivial / summed_weights_eval, proj_coeffs_trivial / summed_weights_eval]
        else:
            output = None

        # normalize based on total weights
        proj_coeffs /= summed_weights_eval



        return proj_coeffs, output

    def _elemwise_bern_projection(self):
        '''Returns the evaluable that calculates the ielem-wise bernstein projection of 'fun' onto 'bernbasis'.'''
        # Obtain the local dofs and coefficients from `basis` at element `ielem`.
        # dofs, basis_coeffs = self.bernbasis.f_dofs_coeffs(self.ielem)

        # Sample the local basis in the local coordinates. The first axes of
        # `shapes` correspond to `weights`, the last axis has length `basis.ndofs`
        shapes = evaluable.Polyval(self.bern_basis_coeffs, self.topo.f_coords.lower(self.lower_args))

        # Compute the local mass matrix and right hand side.
        mass = self.elem_int(evaluable.einsum('Ai,Aj->Aij', shapes, shapes))

        if isinstance(self.fun, function.Array):
            rhs = self.elem_int(evaluable.einsum('Ai,AB->AiB', shapes, self.fun.lower(self.lower_args)))
        else:
            # self.fun contains elementwise rhs integration constants. Need to be mapped to macro elements.
            rhs = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.fun)), self.ielem, float)
            # rhs_macro = evaluable.Inflate(rhs_elem, self.elem_to_macro, self.macro_thb_dofs.shape[0])
            # rhs = evaluable.loop_sum(rhs_macro, self.imacroelem)

        # rhs = self.elem_int(evaluable.einsum('Ai,AB->AiB', shapes, self.fun.lower(self.lower_args)))

        # Solve the local least squares problem.
        local_proj_coeffs = evaluable.einsum('ij,jB->Bi', evaluable.inverse(mass), rhs)

        return local_proj_coeffs

    def _elemwise_bern_projection_nontrivial(self):
        '''Returns the evaluable that calculates the ielem-wise bernstein projection of 'fun' onto 'bernbasis'.'''
        # Obtain the local dofs and coefficients from `basis` at element `ielem`.
        # dofs, basis_coeffs = self.bernbasis.f_dofs_coeffs(self.ielem)

        # Sample the local basis in the local coordinates. The first axes of
        # `shapes` correspond to `weights`, the last axis has length `basis.ndofs`
        shapes = evaluable.Polyval(self.bern_basis_coeffs, self.topo.f_coords.lower(self.lower_args_nontrivial))

        # Compute the local mass matrix and right hand side.
        mass = self.elem_int_nontrivial(evaluable.einsum('Ai,Aj->Aij', shapes, shapes))

        if isinstance(self.fun, function.Array):
            rhs = self.elem_int_nontrivial(evaluable.einsum('Ai,AB->AiB', shapes, self.fun.lower(self.lower_args_nontrivial)))
        else:
            # self.fun contains elementwise rhs integration constants. Need to be mapped to macro elements.
            rhs = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.fun)), self.ielem_nontrivial, float)

        # Solve the local least squares problem.
        local_proj_coeffs = evaluable.einsum('ij,jB->Bi', evaluable.inverse(mass), rhs)

        return local_proj_coeffs

    def _macrowise_thb_projection_trivial(self):
        '''Returns the evaluable that calculates the imacro-wise THB-spline projection of 'fun' onto 'thbbasis'.'''
        if self.split:
            return evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_coeffs_trivial)), self.imacro_trivial, float)
        else:
            return None
    def _macrowise_thb_projection_nontrivial(self):
        '''Returns the evaluable that calculates the imacro-wise THB-spline projection of 'fun' onto 'thbbasis'.'''
        shapes_nontrivial = evaluable.Polyval(self.thb_basis_coeffs_nontrivial, self.topo.f_coords.lower(self.lower_args_nontrivial))
        # ... and insert zeros for the dofs of the macroelement for which that
        # basis has no support on the real element. This `inflate` triggers an
        # explicit inflation warning, which should be ignored.
        shapes_nontrivial = evaluable.Inflate(shapes_nontrivial, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])

        # Compute the local mass matrix and right hand side.
        mass = self.macro_int_nontrivial(evaluable.einsum('Ai,Aj->Aij', shapes_nontrivial, shapes_nontrivial))

        if isinstance(self.fun, function.Array):
            rhs = self.macro_int_nontrivial(evaluable.einsum('Ai,AB->AiB', shapes_nontrivial, self.fun.lower(self.lower_args_nontrivial)))
        else:
            # self.fun contains elementwise rhs integration constants. Need to be mapped to macro elements.
            rhs_elem = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.fun)), self.ielem_nontrivial, float)
            rhs_macro = evaluable.Inflate(rhs_elem, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])
            rhs = evaluable.loop_sum(rhs_macro, self.imacroelem_nontrivial)

        # Solve the local least squares problem.
        return evaluable.einsum('ij,jB->Bi', evaluable.inverse(mass), rhs)

    def _macrowise_thb_projection(self):
        '''Returns the evaluable that calculates the imacro-wise THB-spline projection of 'fun' onto 'thbbasis'.'''
        shapes = evaluable.Polyval(self.thb_basis_coeffs, self.topo.f_coords.lower(self.lower_args))
        # ... and insert zeros for the dofs of the macroelement for which that
        # basis has no support on the real element. This `inflate` triggers an
        # explicit inflation warning, which should be ignored.
        shapes = evaluable.Inflate(shapes, self.elem_to_macro, self.macro_thb_dofs.shape[0])

        # Compute the local mass matrix and right hand side.
        mass = self.macro_int(evaluable.einsum('Ai,Aj->Aij', shapes, shapes))

        if isinstance(self.fun, function.Array):
            rhs = self.macro_int(evaluable.einsum('Ai,AB->AiB', shapes, self.fun.lower(self.lower_args)))
        else:
            # self.fun contains elementwise rhs integration constants. Need to be mapped to macro elements.
            rhs_elem = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.fun)), self.ielem_nontrivial, float)
            rhs_macro = evaluable.Inflate(rhs_elem, self.elem_to_macro, self.macro_thb_dofs.shape[0])
            rhs = evaluable.loop_sum(rhs_macro, self.imacroelem_nontrivial)

        # Solve the local least squares problem.
        return evaluable.einsum('ij,jB->Bi', evaluable.inverse(mass), rhs)

    def _macrowise_bezier_extraction_bern_projection(self, elem_bern_proj_coeffs):
        if self.solve_method == 'QR':
            return self._macrowise_bezier_extraction_bern_projection_QR(elem_bern_proj_coeffs)
        elif self.solve_method == 'MoorePenrose':
            return self._macrowise_bezier_extraction_bern_projection_MoorePenrose(elem_bern_proj_coeffs)
        else:
            raise "No valid solve method given. Please choose between 'QR' or 'MoorePenrose'. Defaulting to 'QR'."
        return self._macrowise_bezier_extraction_bern_projection_QR(elem_bern_proj_coeffs)

    def _macrowise_bezier_extraction_bern_projection_nontrivial(self, elem_bern_proj_coeffs):
        if self.solve_method == 'QR':
            return self._macrowise_bezier_extraction_bern_projection_QR_nontrivial(elem_bern_proj_coeffs)
        elif self.solve_method == 'MoorePenrose':
            return self._macrowise_bezier_extraction_bern_projection_MoorePenrose_nontrivial(elem_bern_proj_coeffs)
        else:
            raise "No valid solve method given. Please choose between 'QR' or 'MoorePenrose'. Defaulting to 'QR'."
        return self._macrowise_bezier_extraction_bern_projection_QR_nontrivial(elem_bern_proj_coeffs)

    def _macrowise_bezier_extraction_bern_projection_MoorePenrose_nontrivial(self, elem_bern_proj_coeffs):
        '''Returns the evaluable that calculates the iamcro-wise THB-spline projection of the ielem-wise bernstein
         coefficients evaluable 'elem_bern_proj_coeffs' onto 'thbbasis'.

         This projection is performed via the Moore Penrose pseudo invere of the Bezier extraction matrix.'''
        # obtain elem Bezier extraction matrix for THB-splines
        # inverse of bernbasis monomial extraction matrix (this matrix is constant over each element and is precomputed)
        C = evaluable.constant(PaddedMatrixInverse(numpy.array(self.bernbasis.get_coefficients(0))))
        # get thb - bezier extraction matrix that describes THB splines as bernstein polynomials
        elem_bezier_extraction = evaluable.einsum('ji,ik->kj', self.thb_basis_coeffs_nontrivial, C)

        # for (C^T C)^-1 C^T b, perform C^T b, casting the elemwise bern dofs to macro THB dofs:
        elem_thb_pre_projection = evaluable.einsum('kj,kB->Bj', elem_bezier_extraction, elem_bern_proj_coeffs)
        elem_thb_pre_projection = evaluable.Inflate(elem_thb_pre_projection, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])
        thb_pre_projection = evaluable.loop_sum(elem_thb_pre_projection, self.imacroelem_nontrivial)

        # find (C^T C) by summing over elements
        elem_CTC = evaluable.einsum('kj,ki->ji', elem_bezier_extraction, elem_bezier_extraction)
        elem_CTC = evaluable.Inflate(elem_CTC, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])
        # elem_CTC = evaluable.einsum('ij->ji', elem_CTC)
        elem_CTC = evaluable.Inflate(evaluable.transpose(elem_CTC,[1,0]), self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])
        macro_CTC = evaluable.loop_sum(elem_CTC, self.imacroelem_nontrivial)

        # get macro elements by inverting CTC and multiplying with macro pre coefficients
        # return evaluable.einsum('ij,jB->Bi', evaluable.inverse(macro_CTC), thb_pre_projection)
        return evaluable.einsum('ij,j->i', evaluable.inverse(macro_CTC), thb_pre_projection)

    def _macrowise_bezier_extraction_bern_projection_QR_nontrivial(self, elem_bern_proj_coeffs):
        '''Returns the evaluable that calculates the iamcro-wise THB-spline projection of the ielem-wise bernstein
         coefficients evaluable 'elem_bern_proj_coeffs' onto 'thbbasis'.

         This projection is performed via the Moore Penrose pseudo invere of the Bezier extraction matrix.'''
        # obtain elem Bezier extraction matrix for THB-splines
        # inverse of bernbasis monomial extraction matrix (this matrix is constant over each element and is precomputed)
        C = evaluable.constant(PaddedMatrixInverse(numpy.array(self.bernbasis.get_coefficients(0))))
        # get thb - bezier extraction matrix that describes THB splines as bernstein polynomials
        elem_bezier_extraction = evaluable.einsum('ji,ik->kj', self.thb_basis_coeffs_nontrivial, C)

        elem_bezier_extraction = evaluable.Inflate(elem_bezier_extraction, self.elem_to_macro_nontrivial, self.macro_thb_dofs_nontrivial.shape[0])
        elem_bezier_extraction = evaluable.einsum('kj->jk', elem_bezier_extraction)
        elem_bezier_extraction = evaluable.loop_concatenate(elem_bezier_extraction, self.imacroelem_nontrivial)
        elem_bezier_extraction = evaluable.einsum('jk->kj', elem_bezier_extraction)

        # get qr pseudo inverse (note that this result is transposed, a limitation of the qrinverse implementation)
        pseudo_inverse = qrinverse(elem_bezier_extraction)

        thb_pre_projection = evaluable.loop_concatenate(elem_bern_proj_coeffs, self.imacroelem_nontrivial)

        # solve macro elementwise projection (recall that the pseudo_inverse is transposed)
        return evaluable.einsum('ki,k->i', pseudo_inverse, thb_pre_projection)

    def proj_discont(self, **kwargs):
        '''Calculates the elemwise projection of the function 'fun' onto the discontinuous space 'bernbasis'.

        Returns an array containing the bernstein coefficients associated to 'bernbasis'
        '''
        self.proj_method = 'discont'
        # setup samples, loop indices, etc.
        self._setup()

        # Obtain the elem dofs and coefficients from `basis` at element `ielem`.
        self.bern_dofs, self.bern_basis_coeffs = self.bernbasis.f_dofs_coeffs(self.ielem)

        elem_bern_proj_coeffs = self._elemwise_bern_projection()

        # Scatter the elem projection coefficients to the global coefficients and
        # do this for every element in the topology.
        bern_proj_coeffs = evaluable.Inflate(elem_bern_proj_coeffs, self.bern_dofs, evaluable.asarray(self.bernbasis.ndofs))
        bern_proj_coeffs = evaluable.loop_sum(bern_proj_coeffs, self.ielem)
        bern_proj_coeffs = evaluable.Transpose.from_end(bern_proj_coeffs, 0)

        # Evaluate.
        return sparse.toarray(evaluable.eval_sparse((bern_proj_coeffs,), **self.arguments)[0])

    def proj_thb(self, **kwargs):
        '''Calculates the macroelemwise projection of the function 'fun' onto the THB-spline space 'thbbasis' and
        performs a global smooting step to obtain a final global THB-spline projection.

        Returns an array containing the THB-spline coefficients associated to 'thbbasis'
        '''
        self.proj_method = 'thb'
        self._process_macro_elems(self.thbbasis)
        self._setup()

        # Obtain the dofs at macroelement `imacro`, which we have prepared at the
        # beginning of this function.
        # self.macro_thb_dofs = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_dofs)), self.imacro, int)
        # self.elem_to_macro = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.elem_to_macro)), self.ielem, int)

        self.macro_thb_dofs_nontrivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_dofs_nontrivial)), self.imacro_nontrivial, int)
        self.elem_to_macro_nontrivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.elem_to_macro)), self.ielem_nontrivial, int)

        if self.split:
            self.macro_thb_dofs_trivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_dofs_trivial)), self.imacro_trivial, int)
            self.elem_to_macro_trivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.elem_to_macro)), self.ielem_trivial, int)

        # Obtain the coefficients of `basis` at real element `ielem` ...
        # _, self.thb_basis_coeffs = self.thbbasis.f_dofs_coeffs(self.ielem)
        _, self.thb_basis_coeffs_nontrivial = self.thbbasis.f_dofs_coeffs(self.ielem_nontrivial)
        if self.split:
            _, self.thb_basis_coeffs_trivial = self.thbbasis.f_dofs_coeffs(self.ielem_trivial)

        ################# step one  : macro thb projections
        macro_proj_coeffs_nontrivial    = self._macrowise_thb_projection_nontrivial()
        macro_proj_coeffs_trivial       = self._macrowise_thb_projection_trivial()

        ################# step two  : global smooting
        proj_coeffs, output = self._macro_elem_weight(macro_proj_coeffs_nontrivial, macro_proj_coeffs_trivial)

        # print(self.split)

        # print('output',output)

        if output is not None:
            t0 = time.perf_counter()
            result =  sparse.toarray(evaluable.eval_sparse((output[0],), **self.arguments)[0])
            print(f'      nontrivial time {time.perf_counter() - t0}')
            t0 = time.perf_counter()
            result += sparse.toarray(evaluable.eval_sparse((output[1],), **self.arguments)[0])
            print(f'      trivial time {time.perf_counter() - t0}')
        else:
            result = sparse.toarray(evaluable.eval_sparse((proj_coeffs,), **self.arguments)[0])

        # Evaluate.
        return result


    def proj_bern(self, **kwargs):
        '''Calculates the two-step projection of the function 'fun' via an intial elemwise projection onto the
        dicsontinous space 'bernbasis' and a macroelemwise reprojection onto the thb-spline space 'thbbasis'. This
        macroelemwise projection is smoothed to obtain a final global THB-spline projection of 'fun'.

        Returns an array containing the THB-spline coefficients associated to 'thbbasis'
        '''
        t0 = time.perf_counter()
        self.proj_method = 'bern'
        self._process_macro_elems(self.thbbasis)
        self._setup()
        # print(f'1 {time.perf_counter() - t0}')

        # get dofs for each basis type
        t0 = time.perf_counter()
        self.bern_dofs, self.bern_basis_coeffs = self.bernbasis.f_dofs_coeffs(self.ielem_nontrivial)
        self.thb_dofs_nontrivial, self.thb_basis_coeffs_nontrivial = self.thbbasis.f_dofs_coeffs(self.ielem_nontrivial)
        if self.split:
            self.thb_dofs_trivial, self.thb_basis_coeffs_trivial = self.thbbasis.f_dofs_coeffs(self.ielem_trivial)
        # print(f'2 {time.perf_counter() - t0}')

        # get macro dofs for THB-splines.
        t0 = time.perf_counter()
        self.macro_thb_dofs_nontrivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_dofs_nontrivial)), self.imacro_nontrivial, int)
        self.elem_to_macro_nontrivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.elem_to_macro)), self.ielem_nontrivial, int)

        if self.split:
            self.macro_thb_dofs_trivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.macro_dofs_trivial)), self.imacro_trivial, int)
            self.elem_to_macro_trivial = evaluable.Elemwise(tuple(map(nutils.types.arraydata, self.elem_to_macro)), self.ielem_trivial, int)
        # print(f'3 {time.perf_counter() - t0}')

        ################# step one  : elem bezier projections
        t0 = time.perf_counter()
        elem_bern_proj_coeffs_nontrivial = self._elemwise_bern_projection_nontrivial()
        # print(f'4 {time.perf_counter() - t0}')

        ################# step two  : macro thb projections (via moore penrose pseudo inverse)
        t0 = time.perf_counter()
        macro_proj_coeffs_nontrivial = self._macrowise_bezier_extraction_bern_projection_nontrivial(elem_bern_proj_coeffs_nontrivial)
        macro_proj_coeffs_trivial = self._macrowise_thb_projection_trivial() # we use thb projection as we simply map the appropriate coeffs
        # print(f'5 {time.perf_counter() - t0}')

        ################# step three : global smooting
        t0 = time.perf_counter()
        proj_coeffs, output = self._macro_elem_weight(macro_proj_coeffs_nontrivial, macro_proj_coeffs_trivial)

        # print(self.split)

        # print('output', output)

        if output is not None:
            t0 = time.perf_counter()
            result = sparse.toarray(evaluable.eval_sparse((output[0],), **self.arguments)[0])
            # print(f'      nontrivial time {time.perf_counter() - t0}')
            t0 = time.perf_counter()
            result += sparse.toarray(evaluable.eval_sparse((output[1],), **self.arguments)[0])
            # print(f'      trivial time {time.perf_counter() - t0}')
        else:
            result = sparse.toarray(evaluable.eval_sparse((proj_coeffs,), **self.arguments)[0])
        # print(f'6 {time.perf_counter() - t0}')

        # Evaluate and return
        # t0 = time.perf_counter()
        # output = sparse.toarray(evaluable.eval_sparse((proj_coeffs,), **self.arguments)[0])
        # print(f'7 {time.perf_counter() - t0}')
        return result


    def proj_global(self, **kwargs):
        '''Calculate the global L2 projection. This function is intended as a comparison/back up
        and not meant to be used. The other options are supposed to be faster.

        Returns an array containing the THB-spline coefficients associated to 'thbbasis'
        '''
        self.proj_method = 'global'

        ns = Namespace()
        ns.x = self.geom

        A = function.outer(self.thbbasis, self.thbbasis)
        b = self.thbbasis * self.fun
        J = function.J(self.geom)

        Amat = self.topo.integrate(A * J, degree=self.degree, arguments=self.arguments)
        bvec = self.topo.integrate(b * J, degree=self.degree, arguments=self.arguments)

        return Amat.solve(bvec, lhs0 = None, constrain = None)

    def proj_bern_numba(self, **kwargs):
        'use the numba method to perform the projection.'
        return thb_bezier_project(self.fun, self.thbbasis, self.topo, self.bernbasis, self.geom, self.macroelems, self.degree_list, self.degree_list, arguments = self.arguments, func_topo=self.refined_topo, element_sizes=self.element_sizes)
