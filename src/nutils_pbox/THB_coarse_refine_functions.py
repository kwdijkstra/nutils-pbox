# this code is an implementation of the coarsening and refinement funtions for THB-splines from
# Carraturo_Giannelli_Reali_Vázquez_2019
import nutils.expression_v2
from nutils import mesh
import numpy
from nutils import export
import nutils_pbox.tools_nutils as tools_nutils
import matplotlib.pyplot as plt

def coarsen_THB(topo, nelems, degree, marked_elements, m, periodic = ()):

    StartBasis = topo.basetopo

    indices_per_level = coarsen(topo._indices_per_level, topo._offsets, nelems, degree, marked_elements, m, periodic=periodic)

    return nutils.topology.HierarchicalTopology(StartBasis,([numpy.unique(numpy.array(i, dtype=int)) for i in indices_per_level]))

def refine_THB(topo, nelems, degree, marked_elements, m, periodic = ()):

    StartBasis = topo.basetopo

    indices_per_level = refine(topo._indices_per_level, topo._offsets, nelems, degree, marked_elements, m, periodic=periodic)

    return nutils.topology.HierarchicalTopology(StartBasis,([numpy.unique(numpy.array(i, dtype=int)) for i in indices_per_level]))

def coarsen(element_indices_level, offsets, nelems, degree, marked_elements, m, periodic = ()):
    element_indices_level = [numpy.array(sub_array).astype(int) for sub_array in element_indices_level]
    splits = numpy.searchsorted(marked_elements, offsets, side='left')

    elem_index_level = [ element_indices_level[level][marked_elements[start:stop] - offsets[level]] if start != stop else [] for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:]))]

    parent_elements = [[]] * len(element_indices_level)
    for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:])):
        if level == 0: continue
        parent_elements[level-1] = derefine_elem_to_level(elem_index_level[level],level, level-1,nelems)

    # print('parent elements', parent_elements)
    # print('marked elements', elem_index_level)

    for rev_level, parent_elements_level in enumerate(reversed(parent_elements)):
        level = len(parent_elements) - rev_level - 1
        parent_elements_level = numpy.flip(numpy.sort(parent_elements_level))
        for parent_element in parent_elements_level:
            parent_element_children = refine_elem_to_level(parent_element, level, level+1,nelems)

            # print('parent_element_children',level,parent_element_children)
            #
            # quit()

            # print((active_elements(element_indices_level, parent_element_children, level+1)),len(coarsing_neigh(element_indices_level, offsets, nelems, degree, parent_element, level, m)),all(active_elements(element_indices_level, parent_element_children, level+1)) and len(coarsing_neigh(element_indices_level, offsets, nelems, degree, parent_element, level, m)) == 0)
            # if all(active_elements(element_indices_level, parent_element_children, level+1)) and len(coarsing_neigh(element_indices_level, offsets, nelems, degree, parent_element, level, m)) == 0:
            if all(active_elements(elem_index_level, parent_element_children, level+1)) and len(coarsing_neigh(element_indices_level, offsets, nelems, degree, parent_element, level, m, periodic = periodic)) == 0:
                # reactivate parent element, and deactivate children
                # print(f'coarsen element')
                ii = numpy.searchsorted(element_indices_level[level],parent_element)
                element_indices_level[level] = numpy.insert(element_indices_level[level], ii, parent_element)
                ii = numpy.searchsorted(element_indices_level[level+1],parent_element_children)
                element_indices_level[level+1] = numpy.delete(element_indices_level[level+1], ii)


    return element_indices_level

# def refine_old(element_indices_level, offsets, nelems, degree, marked_elements, m):
#
#     # add in extra level for refinement
#     element_indices_level = [numpy.array(sub_array) for sub_array in element_indices_level] + [[]]
#     splits = numpy.searchsorted(marked_elements, offsets, side='left')
#
#     elem_index_level = [ element_indices_level[level][marked_elements[start:stop] - offsets[level]] if start != stop else [] for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:]))]
#
#     # for each level, perform algorithm, note that element_indices_level and offsets change for each loop, but, crucially,
#     # the algorithm only changes the space for the levels: level and lower.
#     for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:])):
#         for elem in elem_index_level[level]:
#             element_indices_level, offsets = refine_recursive(element_indices_level, offsets, nelems, degree, elem, level, m)
#
#     # if no extra level is necessary, remove this level
#     if len(element_indices_level[-1]) == 0:
#         element_indices_level.pop(-1)
#
#     element_indices_level = [numpy.array(element_indices,dtype=int) for element_indices in element_indices_level]
#
#     return element_indices_level

def refine(element_indices_level, offsets, nelems, degree, marked_elements, m, periodic = ()):
    # add in extra level for refinement
    element_indices_level = [numpy.array(sub_array) for sub_array in element_indices_level] + [[]]
    splits = numpy.searchsorted(marked_elements, offsets, side='left')

    elem_index_level = [ numpy.array(element_indices_level[level][marked_elements[start:stop] - offsets[level]]) if start != stop else numpy.array([]) for level, (start, stop) in enumerate(zip(splits[:-1], splits[1:]))]

    # for each level, perform algorithm, note that element_indices_level and offsets change for each loop, but, crucially,
    # the algorithm only changes the space for the levels: level and lower.
    L = len(element_indices_level)
    for level in numpy.arange(L-2,-1,-1):
        # get level - m + 1 parent elements
        if level-m+1 >= 0:
            # level l-m+2
            parent_elem = refinement_neigh_list(nelems, degree, elem_index_level[level], level, m, periodic=periodic)
            # print(elem_index_level[level-m+1])
            elem_index_level[level-m+1] = numpy.append(elem_index_level[level-m+1],parent_elem)
            # print(elem_index_level[level-m+1])


        # refine level l elements
        element_indices_level, offsets = refine_by_elements(element_indices_level, offsets, nelems, degree, elem_index_level[level], level)

    # if no extra level is necessary, remove this level
    if len(element_indices_level[-1]) == 0:
        element_indices_level.pop(-1)

    element_indices_level = [numpy.array(element_indices,dtype=int) for element_indices in element_indices_level]

    return element_indices_level

def refine_by_elements(element_indices_level, offsets, nelems, degree, refine_elem_list, level):
    for elem in refine_elem_list:
        element_indices_level, offsets = refined_by_element(element_indices_level, offsets, nelems, degree, elem, level)

    return element_indices_level, offsets

def multi_level_support_list(nelems, degree, elem_list, level, support_level, periodic = ()):
    if len(elem_list) == 0:
        return []
    if level < 0:
        return []
    count_level = [nelem * (2**level) for nelem in nelems]
    count_support_level = [nelem * (2**support_level) for nelem in nelems]

    # find support_elem of level support_level that contains elem of level level
    vec_elem_list = tools_nutils.map_index_vec(elem_list, count_level)
    vec_support_elem_list = (numpy.floor(vec_elem_list / 2 ** (level - support_level) ).astype(int))

    # generate the support extension mask
    mask = tools_nutils.combvec([range(-d, d + 1) for d in degree])

    output_vec = numpy.vstack([vec_elem + mask for vec_elem in vec_support_elem_list])

    # check for bounds
    for i, _ in enumerate(degree):
        if i in periodic:
            output_vec[:, i] = numpy.mod(output_vec[:, i], count_support_level[i])
        else:
            # lower_bound = sum(output_vec[:, i] < 0)
            # upper_bound = sum(output_vec[:, i] >= count_support_level[i] )
            # to_remove = lower_bound + upper_bound

            output_vec = numpy.delete(output_vec, output_vec[:, i] < 0, axis=0)
            output_vec = numpy.delete(output_vec, output_vec[:, i] >= count_support_level[i], axis=0)

    # return index map
    return output_vec

def refinement_neigh_list(nelems, degree, elem_list, level, m, periodic = ()):
    if len(elem_list) == 0:
        return []
    support_extension_vec  = multi_level_support_list(nelems, degree, elem_list, level, level-m+2, periodic=periodic)

    # find elements of level: level-m+1 that cover the support extension
    vec_cover = (numpy.floor(support_extension_vec / 2 ).astype(int))
    counts_cover = [nelem * 2 ** (level - m + 1) for nelem in nelems]
    ind_cover = tools_nutils.map_vec_index(vec_cover, counts_cover)

    # return unique indices
    return numpy.unique(ind_cover)




#
# def refine_recursive(element_indices_level, offsets, nelems, degree, elem, level, m):
#     refine_neigh_level = level - m + 1
#     if refine_neigh_level >= 0:
#         for sup_elem in refinement_neigh(element_indices_level, offsets, nelems, degree, elem, level, m):
#             # print(sup_elem)
#             element_indices_level, offsets = refine_recursive(element_indices_level, offsets, nelems, degree, sup_elem, refine_neigh_level,  m)
#
#     # refine elem
#     element_indices_level, offsets = refined_by_element(element_indices_level, offsets, nelems, degree, elem, level)
#
#     return element_indices_level, offsets

def refined_by_element(element_indices_level, offsets, nelems, degree, elem, level):
    # find elem in appropiate list
    i = numpy.where(element_indices_level[level] == elem)[0]
    if len(i) == 0:
        # if not found, not possible to refine and returns original list
        return element_indices_level, offsets

    # delete elem from appropriate level list
    element_indices_level[level] = numpy.delete( element_indices_level[level], i )

    # refine element
    counts = [nelem * 2 ** (level) for nelem in nelems]
    elem_vec = tools_nutils.map_index_vec(elem, counts)
    elem_vec *= 2
    # elem_vec = elem_vec + [[0, 0], [0, 1], [1, 0], [1, 1]]
    elem_vec = elem_vec + tools_nutils.combvec([[0, 1]] * len(degree))
    refined_indices = tools_nutils.map_vec_index(elem_vec, [count * 2 for count in counts])

    # add refined elements to level + 1 list
    element_indices_level[level + 1] = numpy.append(element_indices_level[level + 1],refined_indices)
    element_indices_level[level + 1] = numpy.sort(element_indices_level[level + 1])

    # recalculate offsets
    offsets = update_offsets(element_indices_level)

    return element_indices_level, offsets


def update_offsets(element_indices_level):
    offsets = [len(list_elem) for list_elem in element_indices_level]
    return numpy.insert(numpy.cumsum((offsets)), 0, 0)

# def refinement_neigh(element_indices_level, offsets, nelems, degree, elem, level, m):
#     support_extension, support_extension_vec  = multilevel_support(element_indices_level, offsets, nelems, degree, elem, level, level-m+2)
#
#     # find elements of level: level-m+1 that cover the support extension
#     vec_cover = (numpy.floor(support_extension_vec / 2 ).astype(int))
#     counts_cover = [nelem * 2 ** (level - m + 1) for nelem in nelems]
#     ind_cover = tools_nutils.map_vec_index(vec_cover, counts_cover)
#
#     # return unique indices
#     return numpy.unique(ind_cover)



def derefine_elem_vec(elem_vec, nelems):
    return numpy.floor(elem_vec / 2).astype(int)

def refine_elem_vec(elem_vec, nelems):
    ndim = len(nelems)
    nvec = elem_vec.shape[0]
    ref_elem_vec = numpy.zeros((nvec*2**ndim,ndim))
    for i,vec in enumerate(elem_vec):
        ref_elem_vec[i*(2**ndim):(i+1)*(2**ndim),:] = vec * 2 + tools_nutils.combvec([[0, 1]] * ndim)

    return ref_elem_vec

def derefine_elem_to_level(elem_indices, level, target_level, nelems):
    assert level > target_level, f'The target level should be lower than starting level'
    if level == target_level: return elem_indices
    elem_vec = tools_nutils.map_index_vec(elem_indices, [nelem * 2 ** (level) for nelem in nelems])
    for leveli in range(target_level, level): # the actual leveli is not important, we only care about the number or times we repeat this step
        elem_vec = derefine_elem_vec(elem_vec, nelems)

    return numpy.unique(
        tools_nutils.map_vec_index(elem_vec, [nelem * 2 ** (target_level) for nelem in nelems])).astype(int)
def refine_elem_to_level(elem_indices, level, target_level, nelems):
    assert level < target_level, f'The target level should be higher than starting level'
    if level == target_level:
        return elem_indices
    elem_vec = tools_nutils.map_index_vec(elem_indices, [nelem * 2 ** (level) for nelem in nelems])
    for leveli in range(level, target_level):
        elem_vec = refine_elem_vec(elem_vec, nelems)
    return tools_nutils.map_vec_index(elem_vec, [nelem * 2 ** (target_level) for nelem in nelems])
def coarsing_neigh(element_indices_level, offsets, nelems, degree, elem, level, m, periodic = ()):
    child_elems = refine_elem_to_level(elem, level, level+1, nelems)
    # check if these are active
    active_child_elems = [child_elem for child_elem in child_elems if child_elem in element_indices_level[level+1]]

    coarse_elems = numpy.empty(0,dtype=int)
    for active_child_elem in active_child_elems:
        support_extension, _ = multilevel_support(element_indices_level, offsets, nelems, degree, active_child_elem, level+1, level + 1, periodic = periodic)
        support_extension_refined = refine_elem_to_level(support_extension, level+1, level+m, nelems)
        coarse_elems = numpy.append(coarse_elems, support_extension_refined)

    coarse_elems_active = coarse_elems[active_elements(element_indices_level, coarse_elems, level+m)]

    return numpy.unique(coarse_elems_active).astype(int)

def active_elements(element_indices_level, elem, level):
    # returns bool of active elements
    if level >= len(element_indices_level):
        return numpy.empty(0,dtype=int)
    if len(element_indices_level[level]) == 0:
        return numpy.empty(0,dtype=int)
    # find indices so that inserting elem into element_indices_level[level] stays sorted
    ii = numpy.searchsorted(element_indices_level[level], elem)
    ii[ii >= len(element_indices_level[level])] = -1

    # return bool of those elements that are active
    return element_indices_level[level][ii] == elem



def multilevel_support(element_indices_level, offsets, nelems, degree, elem, level, support_level, periodic = ()):
    if level < 0:
        return []
    count_level = [nelem * (2**level) for nelem in nelems]
    count_support_level = [nelem * (2**support_level) for nelem in nelems]

    # find support_elem of level support_level that contains elem of level level
    vec_elem = tools_nutils.map_index_vec(elem, count_level)
    vec_support_elem = (numpy.floor(vec_elem / 2 ** (level - support_level) ).astype(int))

    # generate the support extension
    possible_ind = [[] for d in degree]
    for i, _ in enumerate(degree):
        if i in periodic:
            possible_ind[i] = numpy.mod(range(vec_support_elem[0][i] - degree[i],vec_support_elem[0][i] + degree[i]+1), count_support_level[i])
        else:
            possible_ind[i] = range(max(0,vec_support_elem[0][i] - degree[i]),min(count_support_level[i],vec_support_elem[0][i] + degree[i]+1))


    # possible_ind = [ range(max(0,vec_support_elem[0][i] - degree[i]),min(count_support_level[i],vec_support_elem[0][i] + degree[i]+1)) for i in range(len(vec_support_elem)+1)]
    vec_mask = tools_nutils.combvec(possible_ind)

    # return index map
    return tools_nutils.map_vec_index(vec_mask, count_support_level), vec_mask




if __name__ == "__main__":
    nelems = (5,5)
    degree = (3,4)
    m = 2

    periodic = ()

    topo, geom = mesh.rectilinear([numpy.linspace(0, 1, nelem + 1) for nelem in nelems])
    topo = topo.refined_by([24])
    # topo = topo.refined_by([0,1,2])

    topo = refine_THB(topo, nelems, degree, [topo._offsets[-1]-1], m, periodic = periodic)
    topo = refine_THB(topo, nelems, degree, [topo._offsets[-1]-1], m, periodic = periodic)
    topo = refine_THB(topo, nelems, degree, [topo._offsets[-1]-1], m, periodic = periodic)
    topo = refine_THB(topo, nelems, degree, [topo._offsets[-1]-1], m, periodic = periodic)



    # print(len(topo))
    # print(topo._offsets)
    # indices_per_level = refine(topo._indices_per_level, topo._offsets, nelems, degree, marked_elements, m)
    # print(coarsing_neigh(topo._indices_per_level, topo._offsets, nelems, degree, [19], 0, m))

    print(topo._indices_per_level)
    print(topo._offsets)
    print(numpy.arange(topo._offsets[-2],topo._offsets[-1]))

    # coarsen_THB(topo, nelems, degree, [19], m)
    # topo = coarsen_THB(topo, nelems, degree, [150,151,152,153], m)
    topo = coarsen_THB(topo, nelems, degree, numpy.arange(topo._offsets[-2],topo._offsets[-1]), m)
    topo = coarsen_THB(topo, nelems, degree, numpy.arange(topo._offsets[-3],topo._offsets[-2]), m)
    topo = coarsen_THB(topo, nelems, degree, numpy.arange(topo._offsets[-4],topo._offsets[-3]), m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [topo._offsets[-1]-1], m)
    # topo = coarsen_THB(topo, nelems, degree, [13], m)




    ns = nutils.expression_v2.Namespace()
    ns.x = geom
    ns.THBBasis = topo.basis('th-spline',degree = degree)


    ns.add_field('phi', ns.THBBasis)
    args = {'phi': numpy.zeros(len(ns.THBBasis))}
    bezier = topo.sample('bezier', 4 * max(degree))
    x, approx = bezier.eval(['x_i', 'phi'] @ ns, **args)
    fig, axs = plt.subplots()
    export.triplot(axs, x, approx, tri=bezier.tri, hull=bezier.hull,cmap='binary')
    plt.show()


