# this is an extension for nutils that extends the hierarchicaltopology to use p-box
import time

import numpy as np
import nutils.topology
import numpy

import nutils_pbox.tools_evaluable
from nutils_pbox.evaluable_pbox import ProjectEvaluable
import nutils_pbox.THB_coarse_refine_functions as THB_coarse_refine_functions
import nutils_pbox.tools_nutils as tools_nutils
from nutils_pbox.tools_nutils import refine_Vec
from typing import Any, FrozenSet, Iterable, Iterator, List, Mapping, Optional, Sequence, Tuple, Union, Sequence

_ArgDict = Mapping[str, numpy.ndarray]

from nutils import sparse

from nutils import function
from typing import Optional
import numpy

from functools import cached_property

_ = numpy.newaxis


def map_pboxInd_elemInd(pbox_indices, pbox_count, level, degreeList):
    pbox_vec_list = tools_nutils.map_index_vec(pbox_indices, pbox_count * (2 ** (level)))
    elem_vec_list = map_pboxVec_elemVec(pbox_vec_list, degreeList)
    elem_ind_list = (tools_nutils.map_vec_index(elem_vec_list, pbox_count * degreeList * (2 ** (level))))
    return numpy.unique(elem_ind_list)


def map_pboxVec_elemVec(pboxVec, degreeList):
    dim = pboxVec.shape[1]
    combinations = tools_nutils.combvec([range(degree) for degree in degreeList])
    offset = combinations.shape[0]

    output = numpy.zeros( (pboxVec.shape[0] * combinations.shape[0],dim))

    for i in range(pboxVec.shape[0]):
        index = i * offset
        output[index:index+offset,:] = degreeList * pboxVec[i,:] + combinations

    return output

def map_elemVec_pboxVec(elemVec, degreeList):
    if elemVec.shape[0] == 0:
        return elemVec
    pboxVec = numpy.zeros(elemVec.shape, dtype=int)
    for i, degree in enumerate(degreeList):
        pboxVec[:,i] = numpy.floor_divide(elemVec[:,i], degree)

    # quick and dirty check for uniqueness.
    I = numpy.zeros(pboxVec.shape[0],dtype=bool)
    I[0] = True
    for j in range(pboxVec.shape[0]-1):
        if any(pboxVec[j+1,:]-pboxVec[j,:]!=0):
            I[j+1] = True

    return pboxVec[I,:]


def whereList(elem_list, list):
    elem_list_count = len(elem_list)
    output = numpy.zeros(elem_list_count, dtype=int)
    elem_list = (numpy.sort(elem_list))

    # print(elem_list)
    # print(list)


    output_index = 0
    elem2check = elem_list[output_index]
    for i, elem in enumerate((list)):
        # print(elem, elem2check)
        if elem == elem2check:
            output[output_index] = i
            output_index += 1
            # print(output_index, elem_list_count)
            if output_index == elem_list_count:
                return output
            elem2check = elem_list[output_index]

    return output

class PboxTopology():
    def __init__(self, NutilsBaseTopology, degree : tuple, MaxL : int,  admissibility_class : int, active_pboxes_per_level):
        self.basetopology = NutilsBaseTopology
        self.degree = numpy.array(degree)
        self.ndims = len(self.degree)
        self.pbox_count = numpy.array(self.basetopology.shape / self.degree, dtype=int)
        self.MaxL = MaxL
        self.m = admissibility_class

        self.active_pboxes_per_level = active_pboxes_per_level
        # additional processing
        self.pbox_offsets = numpy.cumsum([0, *map(len, self.active_pboxes_per_level)], dtype=int)
        self.L = len(self.active_pboxes_per_level)

        self.GenerateHierarchcialTopology()
        self.pbox_refined_indices_per_level = self.FindRefinedIndicesPerLevel()

        self.ElemSize = []
        self.proj_elem = None

        self.len = numpy.sum([len(pboxes) for pboxes in self.active_pboxes_per_level])

        # self.set_methods()


    # def set_methods(self):
    #     self.space = self.basetopology.space
    #     self._bnames = self.basetopology._bnames
    #     self.references = self.basetopology.references
    #     self.transforms = self.basetopology.transforms
    #     self.opposites = self.basetopology.opposites

    def _basis_spline(self, degree = 1, knotvalues=None, knotmultiplicities=None, continuity=-1, periodic=None):
        return self.nutils_topology._basis_spline(degree = degree, knotvalues=knotvalues, knotmultiplicities=knotmultiplicities, continuity=continuity, periodic=periodic)

    # @staticmethod
    # def space(self):
    #     return self.nutils_topology.space
    #
    # @staticmethod
    # def _bnames(self):
    #     return self.nutils_topology._bnames
    #
    # @staticmethod
    # def references(self):
    #     return self.nutils_topology.references
    #
    # @staticmethod
    # def transforms(self):
    #     return self.nutils_topology.transforms
    #
    # @staticmethod
    # def opposites(self):
    #     return self.nutils_topology.opposites

    def __len__(self):
        return self.len
    def __add__(self, other):
        # combine two PboxTopologies and returns the joined refined PboxTopology
        assert isinstance(other, PboxTopology)
        assert (self.degree == other.degree).all
        assert (self.pbox_count == other.pbox_count).all

        active_pboxes_per_level = self.active_pboxes_per_level
        other_active_pboxes_per_level = other.active_pboxes_per_level

        # combine both to one giant list
        L = max(len(active_pboxes_per_level), len(other_active_pboxes_per_level))
        new_active_pboxes_per_level = [numpy.empty(shape = 0, dtype=int),] * L
        for l in range(len(active_pboxes_per_level)):
            new_active_pboxes_per_level[l] = active_pboxes_per_level[l]
        for l in range(len(other_active_pboxes_per_level)):
            new_active_pboxes_per_level[l] = numpy.unique(numpy.concatenate([new_active_pboxes_per_level[l],other_active_pboxes_per_level[l]]))

        for l in reversed(range(L)):
            for pbox in new_active_pboxes_per_level[l]:
                # find all parent elements
                pbox_vec = tools_nutils.map_index_vec(pbox, self.pbox_count * 2**l)
                for parent_level in reversed(range(l)): # loop over all parent levels
                    pbox_vec = numpy.floor(pbox_vec / 2)
                    parent_index = tools_nutils.map_vec_index(pbox_vec, self.pbox_count * 2**parent_level)
                    new_active_pboxes_per_level[parent_level] = numpy.setdiff1d(new_active_pboxes_per_level[parent_level],[parent_index])

        # return new PboxTopology.
        # combine MaxL and admissibility class.
        return PboxTopology(self.basetopology, self.degree, max(self.MaxL,other.MaxL), max(self.m,other.m), new_active_pboxes_per_level)

    def select(self, indicator: function.Array, ischeme: str = 'bezier2', **kwargs: numpy.ndarray) -> 'Topology':
        # Select elements where `indicator` is strict positive at any of the
        # integration points defined by `ischeme`. We sample `indicator > 0`
        # together with the element index (`self.f_index`) and keep all indices
        # with at least one positive result.
        sample = self.sample(ischeme, degree=max(self.degree))
        isactive, ielem = sample.eval([indicator > 0, self.nutils_topology.f_index], **kwargs)
        selected = nutils.types.frozenarray(numpy.unique(ielem[isactive]))
        return selected
        # return self[selected]

    def GenerateHierarchcialTopology(self):
        # updates the topology by transforming the p-box structure to element structure.

        # generate data structure
        element_indices_per_level = [[] for _ in self.active_pboxes_per_level]


        # loop over levels
        for ilevel, pbox_indices in enumerate(self.active_pboxes_per_level):
            # generate elements for each p-box
            pbox_vec_list = tools_nutils.map_index_vec(pbox_indices, self.pbox_count * (2 ** (ilevel)))
            elem_vec_list = map_pboxVec_elemVec(pbox_vec_list, self.degree)
            # print(elem_vec_list)
            elem_ind_list = (tools_nutils.map_vec_index(elem_vec_list, self.pbox_count * self.degree * (2 ** (ilevel))))
            element_indices_per_level[ilevel] = numpy.unique(elem_ind_list)


        # create topology (code snippit taken from nutils)
        # need refined boolean for initial refinement
        if self.L > 1:
            self.nutils_topology = nutils.topology.HierarchicalTopology(self.basetopology, ([numpy.unique(numpy.array(i, dtype=int)) for i in element_indices_per_level]) )
        else:
            self.nutils_topology = self.basetopology

    def GenerateProjectionPbox(self, admissibility_class):
        # generates a list [macro elements] of lists [the active pboxes making up the macro element]
        if admissibility_class == 2:
            return [ [i] for i in range(self.pbox_offsets[-1]) ]

        active_Pbox_proj_elem = [] # active Pboxes making up a projection element
        parent_Pbox_proj_elem = [] # the parent well-behaved border nutils_pbox

        def CoveredPbox(pbox, level):
            # check wether the nutils_pbox is contained in one of the active nutils_pbox proj elem
            for parent in parent_Pbox_proj_elem:
                parent_level = self.L - len(parent)
                if pbox in parent[level-parent_level]:
                    return True
            return False

        def CoveredPboxCount(pbox, level):
            # check wether the nutils_pbox is conained in one of the active nutils_pbox proj elem
            for i,parent in enumerate(parent_Pbox_proj_elem):
                parent_level = self.L - len(parent)
                if level < parent_level:
                    continue
                if pbox in parent[level-parent_level]:
                    return True, i
            return False, -1

        for level in range(self.L):
            # get well-behaved-border-pboxes
            well_behaved_border_pbox_level, well_behaved_border_level_parent = self.find_well_behaved_border_p_box_level(level)

            # add all well-behaved border pboxes that are not already contained in a different active_pbox
            for i,wbb_pbox in enumerate(well_behaved_border_pbox_level):
                if not CoveredPbox(wbb_pbox,level):
                    if wbb_pbox in self.active_pboxes_per_level[level]:
                        active_Pbox_proj_elem.append([self.PboxGlobalIndex(wbb_pbox,level)])
                    else:
                        active_Pbox_proj_elem.append([])
                    parent_Pbox_proj_elem.append(well_behaved_border_level_parent[i])

        for i in range(self.pbox_offsets[-1]):
            pbox, level = self.PboxLevelIndex(i)
            Covered, loc =  CoveredPboxCount(pbox, level)
            if Covered and i not in active_Pbox_proj_elem[loc]:
                active_Pbox_proj_elem[loc].append(i)
            elif not any([i in pbox_proj_elem for pbox_proj_elem in active_Pbox_proj_elem]):
                active_Pbox_proj_elem.append([i])

        # print(active_Pbox_proj_elem)

        return active_Pbox_proj_elem

    def FindRefinedIndicesPerLevel(self):
        # finds a list of refined Pboxes

        prev_level_refined = numpy.array([], dtype=int)
        refined_pboxes = [[]] * len(self.active_pboxes_per_level)

        pbox_tot_count = numpy.prod(self.pbox_count)
        increase_factor = 2 ** len(self.pbox_count)

        for level, active_levels in enumerate(self.active_pboxes_per_level):
            level_pbox_tot_count = pbox_tot_count * increase_factor ** level
            all_pbox_indices = numpy.arange(0, level_pbox_tot_count, dtype=int)
            # remove the active elements
            # remove the previous level active elements
            to_remove_indices = numpy.append(prev_level_refined, active_levels)

            all_pbox_indices = numpy.delete(all_pbox_indices, to_remove_indices)

            # all_pbox_indices[to_remove_indices] = []

            refined_pboxes[level] = all_pbox_indices

            # refine to_remove_indices for next level
            vec_to_refine = tools_nutils.map_index_vec(to_remove_indices, self.pbox_count * 2 ** (level))
            vec_refined = refine_Vec(vec_to_refine)
            prev_level_refined = numpy.array(
                tools_nutils.map_vec_index(vec_refined, self.pbox_count * 2 ** (level + 1)), dtype=int)

        return refined_pboxes

    def find_well_behaved_border_p_box_level(self, level):
        refinement_domain = self.active_pboxes_per_level[level]
        if level < len(self.pbox_refined_indices_per_level):
            refinement_domain = numpy.append(refinement_domain,self.pbox_refined_indices_per_level[level])

        pbox_vec_list = tools_nutils.map_index_vec(refinement_domain, self.pbox_count * 2 ** level)

        well_behaved_border_level = []

        for i,pbox in enumerate(refinement_domain):  # loop over all pboxes
            # find nutils_pbox corner, which overlaps with previous level mesh. Use this corner to check the relevant edges
            corner_vec = pbox_vec_list[i, :] % 2
            corner_ind = corner_vec[0] + 2 * corner_vec[1]

            corner_normals = (-1 + 2 * corner_vec)
            indices = tools_nutils.combvec([numpy.array([0, offset]) for offset in corner_normals])[1:, :]
            # check relevant edges
            vec2check = pbox_vec_list[i, :] + indices
            ind2check = tools_nutils.map_vec_index(vec2check, self.pbox_count * (2 ** (level)))
            Neighbours = [element not in refinement_domain and element >= 0 for element in ind2check]

            if any(Neighbours):
                well_behaved_border_level.append(pbox)


        well_behaved_border_level_nested = [[[element],] for element in well_behaved_border_level]

        # generate the resulting higher level p-boxes contained in these well-behaved border elements
        for i, pbox in enumerate(well_behaved_border_level_nested):
            pbox_vec_list = tools_nutils.map_index_vec(pbox[0], self.pbox_count * 2 ** level)
            for lvl in range(level+1,self.L):
                pbox_vec_list = refine_Vec(pbox_vec_list)
                pbox_ind_list = tools_nutils.map_vec_index(pbox_vec_list, self.pbox_count * 2 ** (lvl))
                well_behaved_border_level_nested[i].append(pbox_ind_list)


        return well_behaved_border_level, well_behaved_border_level_nested


    def PboxGlobalIndex(self, pbox, level):
        return numpy.sum(self.active_pboxes_per_level[level]< pbox) + self.pbox_offsets[level]

    def PboxLevelIndex(self, pbox):
        level = numpy.sum(self.pbox_offsets <= pbox)-1
        return self.active_pboxes_per_level[level][pbox - self.pbox_offsets[level]], level

    def ElemGlobalIndex(self, elem, level):
        if isinstance(self.nutils_topology, nutils.topology.StructuredTopology):
            return elem
        else:
            return numpy.sum(self.nutils_topology._indices_per_level[level] < elem) + self.nutils_topology._offsets[level]

    def refined(self):
        # print("here")
        # print(self.nutils_topology)
        # print(self.nutils_topology.refined)

        return self.nutils_topology.refined()

    def refined_by(self, refine_pbox):
        # refines the topology given the refine_pbox list and returns a new PboxHierarchicalTopology object
        if max(refine_pbox) >= self.len: raise Exception(f'Trying to refine pboxes with index higher than total pboxes')
        refine_pbox = np.array(refine_pbox)

        # setup array
        if self.m != numpy.infty:
            admissible_class = self.m
        else:
            # set admissibility to be more than the maximum number of levels
            admissible_class = self.L + 1

        # limit refinement to MaxL
        if len(self.pbox_offsets) >= self.MaxL:
            max_pbox_id = self.pbox_offsets[self.MaxL-1]
            refine_pbox = refine_pbox[refine_pbox < max_pbox_id]

        active_pboxes_per_level = THB_coarse_refine_functions.refine(self.active_pboxes_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree),refine_pbox, m=admissible_class, periodic= self.basetopology.periodic)

        # print(active_pboxes_per_level)
        # quit()

        return PboxTopology(self.basetopology, self.degree, self.MaxL, self.m , active_pboxes_per_level)


    def coarsen_by_pbox(self, coarsen):
        if self.m != numpy.infty:
            admissible_class = self.m
        else:
            admissible_class = self.L + 1

        coarsen = numpy.array(coarsen)

        # refine the pboxes
        coarsened_active_indices_per_level = THB_coarse_refine_functions.coarsen(self.active_pboxes_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree), coarsen, admissible_class, periodic=self.basetopology.periodic)

        for _ in range(len(coarsened_active_indices_per_level)):
            if len(coarsened_active_indices_per_level[-1]) == 0:
                coarsened_active_indices_per_level.pop(-1)
            else:
                break

        return PboxTopology(self.basetopology, self.degree, self.MaxL, self.m , coarsened_active_indices_per_level)

    def basis(self, type='th-spline', degree=None, continuity=None):
        '''Generate a basis over the nutils_pbox.
        The standard option is the thb-spline basis with the same degree as the nutils_pbox.'''
        if degree is None:
            degree = self.degree
        if continuity is None:
            continuity = numpy.array([max(p - 1, -1) for p in degree])
        if type == "discont" or all(numpy.array(continuity) == -1):
            return tools_nutils.arb_basis_discontinuous(self.nutils_topology, degree)
        else:
            return self.nutils_topology.basis(name=type, degree=degree, continuity=continuity)

    def project(self,
                func : function.Array,
                onto : function.Array,
                geometry,
                method = "bern",
                degree = None,
                bern_degree = None,
                solve_method = 'QR',
                arguments: Optional[_ArgDict] = None,
                refined_topology = None,
                refined_basis = None,
                coarse_topology = None,
                coarse_basis = None,
                mapping = None,
                coeffs = None,
                **kwargs):
        '''Projects the nutils function 'func' onto the nutils_pbox thb-spline basis with the method given in 'method'.
        If func is defined over a more refined mesh, additionally supply refined_topology for integration.'''

        assert  not (refined_topology is not None and coarse_topology is not None)

        nutils_topology = self.nutils_topology

        t1 = time.perf_counter()

        # generate projection elements
        t0 = time.perf_counter()
        self.GenerateProjectionElement()
        time_pbox = time.perf_counter() - t0

        # project with the ProjectEvaluable class, taking advantage of the nutils evaluable methods.
        if onto is None:
            onto = self.basis()

        if bern_degree is None:
            bern_degree = self.degree

        if method == 'bern' or method == 'bern_numba':
            bern_basis = self.basis('discont', degree = bern_degree)
            for elem in range(len(self.nutils_topology)):
                if len(onto.get_dofs(elem)) < len(bern_basis.get_dofs(elem)):
                    raise Exception(f'Bernstein basis and onto basis are mismatched. Please provide onto degree vector to project function via bern_degree.')
        else:
            bern_basis = None

        numba_refined_topo = None

        if refined_topology is not None:
            #
            if method == 'bern':
                func = refined_topology.project_coarsen_preprocess(self, geometry, fun = func, coarse_basis = self.basis('discont',degree = bern_degree), arguments= arguments)
            elif method == 'bern_numba':
                func = func
                # nutils_topology = refined_topology.nutils_topology
                numba_refined_topo = refined_topology.nutils_topology
            else:
                if refined_basis is not None:
                    for key, item in func.arguments.items():
                        assert arguments[key].shape == item[0]

                        # mapping = nutils_pbox.tools_evaluable.split_projection_coarsening(pbox_refined=refined_topology, pbox_coarse=self, basis_refined = refined_basis, basis_coarse= onto ,coeffs_refined= arguments[key])

                func = refined_topology.project_coarsen_preprocess(self, geometry, fun=func, coarse_basis=onto, arguments=arguments)

        if coarse_topology is not None and coarse_basis is not None:
            # print('splitting projection')
            if method == 'bern_numba':
                nutils_topology = nutils_topology
            elif coeffs is None:
                for key, item in func.arguments.items():
                    assert arguments[key].shape == item[0]

                    mapping = nutils_pbox.tools_evaluable.split_projection_refinement(pbox_refined=self, pbox_coarse=coarse_topology, basis_refined=onto, basis_coarse=coarse_topology.basis(degree=bern_degree), coeffs_coarse=arguments[key])
            else:
                mapping = nutils_pbox.tools_evaluable.split_projection_refinement(pbox_refined=self, pbox_coarse=coarse_topology, basis_refined=onto, basis_coarse=coarse_topology.basis(degree=bern_degree), coeffs_coarse=coeffs)

        # print(f'   project setup took {time.perf_counter() - t1} of which MacroElem generation is {time_pbox}')

        # print('coarse',type(coarse_topology))
        # print('refined',type(refined_topology))
        #
        # if coarse_topology is not None:
        #     print(len(self))
        #     print(len(coarse_topology))
        #     print('integration')
        #     print(coarse_topology.integrate(func, degree = 2*max(degree), arguments=arguments))
        #     print(coarse_topology.integrate(bern_basis, degree = 2*max(degree), arguments=arguments))

        if degree is None:
            integrate_degree = 2 * max(self.degree)
        else:
            integrate_degree = degree

        self.elem_size(0, geometry)

        t1 = time.perf_counter()
        output = ProjectEvaluable(topo = nutils_topology, geom = geometry, thbbasis= onto, bernbasis= bern_basis, weights = 'integral', macroelems=self.proj_elem, fun= func, integration_degree = integrate_degree, degree_list=bern_degree , proj_method = method, solve_method = solve_method, arguments=arguments, mapping = mapping, refined_topo=numba_refined_topo, element_sizes=self.ElemSize).project(**kwargs)
        # output = ProjectEvaluable(topo = self.topology, geom = geometry, thbbasis= onto, bernbasis= self.basis('discont', degree = bern_degree), weights = 'integral', macroelems=self.proj_elem, fun= func, integration_degree = 2*max(self.degree), proj_method = method, solve_method = solve_method, arguments=arguments).project(**kwargs)
        # print(f'   project  took {time.perf_counter() - t1}')
        return output[0], [output[1], time_pbox]

    def project_coarsen_preprocess(self, coarse_topology, geometry, fun : function.Array, coarse_basis: function.Array = None, arguments: Optional[_ArgDict] = None ):
        assert isinstance(coarse_topology, PboxTopology)

        if coarse_basis is None:
            coarse_basis = coarse_topology.basis()
        lhs_integral = coarse_basis * fun
        J = nutils.function.J(geometry)
        b_fine = self.nutils_topology.integrate_elementwise(lhs_integral * J, degree = 2 * max(self.degree), arguments = arguments)

        # b_fine is defined over the refined elements, has to be remapped to the coarse elements
        def PboxMapping(original_indices, coarsened_indices):

            coarse_offsets = numpy.cumsum([0, *map(len, coarsened_indices)], dtype=int)
            original_offsets = numpy.cumsum([0, *map(len, original_indices)], dtype=int)

            numCoarse = numpy.sum([len(indices) for indices in coarsened_indices])

            def recursive_mapper(ielem_level, level, tomap_indices, tomap_offsets):
                if ielem_level in tomap_indices[level]:
                    return [numpy.where(ielem_level == tomap_indices[level])[0][0] + tomap_offsets[level]]
                else:
                    # ielem is refined in the oiginal mapping, hence search for refined ielems
                    ielem_level_vec = tools_nutils.map_index_vec(ielem_level, self.pbox_count * self.degree * (2 ** (level)))
                    ielem_refined_vec = refine_Vec(ielem_level_vec)
                    ielem_refined_index = tools_nutils.map_vec_index(ielem_refined_vec, self.pbox_count * self.degree * (2 ** (level + 1)))

                    return numpy.concatenate([recursive_mapper(ielem_refined, level + 1, tomap_indices, tomap_offsets) for ielem_refined in ielem_refined_index])

            elem_mapping_inv = [[] for _ in range(numCoarse)]

            for ielem_coarse in range(numCoarse):
                level = numpy.sum(coarse_offsets <= ielem_coarse) - 1
                ielem_level = coarsened_indices[level][ielem_coarse - coarse_offsets[level]]

                elem_mapping_inv[ielem_coarse] = numpy.sort(recursive_mapper(ielem_level, level, original_indices, original_offsets))

            return elem_mapping_inv

        if len(coarse_topology.active_pboxes_per_level) > 1:
            mapping_coarse_fine = PboxMapping(self.nutils_topology._indices_per_level, coarse_topology.nutils_topology._indices_per_level)
        else:
            mapping_coarse_fine = PboxMapping(self.nutils_topology._indices_per_level, [ numpy.arange(len(coarse_topology.nutils_topology)) ])

        N_e_coarse = len(coarse_topology.nutils_topology)

        coarse_elem_dofs = [coarse_basis.get_dofs(e) for e in range(N_e_coarse)]

        b_coarse = [ numpy.zeros(len(dofs)) for dofs in coarse_elem_dofs ]

        for e_coarse in range(N_e_coarse):
            for e_fine in mapping_coarse_fine[e_coarse]:
                b_coarse[e_coarse] += b_fine[e_fine,coarse_elem_dofs[e_coarse]]

        return b_coarse

    # def GenerateProjectionPbox(self):
    #     # pboxBaseTopology has a tensor product structure.
    #     return [[i] for i in numpy.arange(numpy.prod(self.pbox_count))]

    def GenerateProjectionElement(self):
        if self.proj_elem is not None:
            return

        self.proj_elem, self.elem_proj = self._GenerateProjectionElement(self.m)

        self.proj_count = len(self.proj_elem)

    def _GenerateProjectionElement(self, admissibility_class):
        proj_pbox_arr = self.GenerateProjectionPbox(admissibility_class)

        offset = numpy.prod(self.degree)
        proj_elem_arr = [numpy.zeros(offset * len(box), dtype=int) for box in proj_pbox_arr]

        for i, proj_pbox in enumerate(proj_pbox_arr):
            for j, pbox in enumerate(proj_pbox):
                pbox_index, level = self.PboxLevelIndex(pbox)
                pbox_vec = tools_nutils.map_index_vec(pbox_index, self.pbox_count * 2 ** level)
                elem_vec = map_pboxVec_elemVec(pbox_vec, self.degree)
                elem_index = tools_nutils.map_vec_index(elem_vec, self.pbox_count * self.degree * 2 ** level)
                elem = numpy.array([self.ElemGlobalIndex(elem_index_individual, level) for elem_index_individual in elem_index], dtype=int)

                proj_elem_arr[i][j * offset: j * offset + offset] = elem

        elem_proj_arr = numpy.zeros(len(self.nutils_topology), dtype=int)
        for ipbox, ielems in enumerate(proj_elem_arr):
            for ielem in ielems:
                elem_proj_arr[ielem] = ipbox

        return proj_elem_arr, elem_proj_arr

    def integral(self, func: function.IntoArray, ischeme: str = 'gauss', degree: Optional[int] = None, edit=None) -> nutils.function.Array:
        'integral'
        return self.nutils_topology.integral(func = func, ischeme = ischeme, degree = degree, edit = edit)

    def integrate(self, funcs: Iterable[function.IntoArray], ischeme: str = 'gauss', degree: Optional[int] = None, edit=None, *, arguments: Optional[_ArgDict] = None) -> Tuple[numpy.ndarray, ...]:
        'integrate'
        return self.nutils_topology.integrate(funcs, ischeme = ischeme, degree = degree, edit = edit, arguments = arguments)


    def integrate_elementwise(self, funcs: Iterable[function.Array], *, degree: int, asfunction: bool = False, ischeme: str = 'gauss', arguments: Optional[_ArgDict] = None) -> Union[List[numpy.ndarray], List[function.Array]]:
        'element-wise integration'
        return self.nutils_topology.integrate_elementwise(funcs, degree=degree, asfunction = asfunction, ischeme=ischeme, arguments = arguments)

    def _elementwise_pboxwise(self, data_elementwise, asfunction : bool = False, axis = 0):

        assert axis == 0

        # self.GenerateProjectionElement()

        proj_elem_admis2 = self._GenerateProjectionElement(2)[0]


        if len(data_elementwise.shape) == 1:
            pbox_retvals = numpy.asarray([numpy.sum(data_elementwise[elems]) for elems in proj_elem_admis2])
        else:
            pbox_retvals = numpy.asarray([numpy.sum(data_elementwise[elems,:],axis=axis) for elems in proj_elem_admis2])

        if asfunction:
            return [function.get(pbox_retvals, 0, self.f_index) for retval in data_elementwise]
        else:
            return pbox_retvals

    def integrate_pboxwise(self, funcs: Iterable[function.Array], *, degree: int, asfunction: bool = False, ischeme: str = 'gauss', arguments: Optional[_ArgDict] = None) -> Union[List[numpy.ndarray], List[function.Array]]:
        'pbox-wise integration'

        # nutils code for elementwise integrations
        retvals = self.integrate_elementwise(funcs, degree=degree, asfunction = asfunction, ischeme=ischeme, arguments = arguments)
        # contract over pboxes
        return self._elementwise_pboxwise(retvals, asfunction)

    @cached_property
    def boundary(self) -> 'Topology':
        return self.nutils_topology.boundary

    def sample(self, ischeme: str, degree: int) -> nutils.sample.Sample:
        return self.nutils_topology.sample(ischeme= ischeme, degree= degree)


    def find_elem_size(self, geometry):
        J = function.J(geometry)
        elemSizeSparse = tools_nutils.integrate_elementwise_sparse(self.nutils_topology, [J], degree=0)
        indices, elemSize, shape = sparse.extract(elemSizeSparse[0])
        self.ElemSize = elemSize

    def elem_size(self, elem, geometry):
        if len(self.ElemSize) == 0:
            self.find_elem_size(geometry)
        return self.ElemSize[elem]


    def connectivity(self):
        # returns an array with len(pbox) elements, where each entry is a list of neighbouring pboxes.
        # this code is pretty bad...

        # do initial loop, from lowest level, to highest level, and for each pbox in this level, find the next neighbouring $p$-boxes of the same level or higher

        connectivity_array = [numpy.array([],dtype=int)] * len(self)

        def find_neighbours_level(pbox, level):
            # find the neighbours of pbox at level = level
            # returns a list of active neighbours at level = level
            # and a list of non-active neighbours at level = level
            vec_pbox = tools_nutils.map_index_vec(pbox, self.pbox_count * 2**level)
            vec_neigh = vec_pbox + numpy.vstack([numpy.identity(self.ndims),-numpy.identity(self.ndims)])
            neigh = numpy.unique(tools_nutils.map_vec_index(vec_neigh, self.pbox_count * 2**level, remove_non_existent=True))

            return split_active_non_active(neigh, level)

        def split_active_non_active(pboxes, level):
            pboxes_active = numpy.intersect1d(pboxes, self.active_pboxes_per_level[level], assume_unique=True)
            pboxes_non_active = numpy.setdiff1d(pboxes, pboxes_active, assume_unique=True)
            return pboxes_active, pboxes_non_active

        def add_neigh(i_global: int = 0, neighours: numpy.ndarray = None, level: int = 0):
            assert isinstance(neighours, numpy.ndarray)
            level_index = numpy.asarray([numpy.where(self.active_pboxes_per_level[level]==neigh)[0] for neigh in neighours ], dtype=int) + self.pbox_offsets[level]
            connectivity_array[i_global] = numpy.append(connectivity_array[i_global], level_index)

        def coarsen_neigh(pboxes, starting_level):
            vec_pbox = tools_nutils.map_index_vec(pboxes, self.pbox_count * 2**starting_level)
            vec_pbox_coarsened = numpy.floor(vec_pbox / 2)
            pbox_coarsened =  tools_nutils.map_vec_index(vec_pbox_coarsened, self.pbox_count * 2**(starting_level-1),remove_non_existent=True)
            return split_active_non_active( numpy.unique(pbox_coarsened), starting_level - 1)


        for l, pboxes_level_l in reversed(list(enumerate(self.active_pboxes_per_level))):
            for i,pbox in enumerate(pboxes_level_l):
                i_global = i + self.pbox_offsets[l] # correct pbox index in global array
                neigh_active, neigh_non_active = find_neighbours_level(pbox, l)
                add_neigh(i_global = i_global, neighours = neigh_active, level = l)
                level_coarsening = l
                while len(neigh_non_active) > 0 and level_coarsening >= 0: #there are non-mapped neighbours
                    neigh_active, neigh_non_active = coarsen_neigh(neigh_non_active, level_coarsening)
                    level_coarsening -= 1

                    add_neigh(i_global=i_global, neighours=neigh_active, level=level_coarsening)

        for pbox, neighbours in enumerate(connectivity_array):
            for neigh in neighbours:
                connectivity_array[neigh] = numpy.append(connectivity_array[neigh], pbox)

        connectivity_array = [numpy.unique(neigh) for neigh in connectivity_array]

        return connectivity_array
