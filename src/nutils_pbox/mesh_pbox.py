from nutils_pbox.topology_pbox import PboxTopology
import nutils
import numpy

_ = numpy.newaxis

# def pbox(degree : tuple, pbox_count : tuple, MaxL = numpy.infty, admissibility_class = 2):
#     topology, geometry = nutils.mesh.rectilinear([numpy.linspace(0, 1, pbox_count[i] * degree[i] + 1) for i in range(len(degree))])
#
#     return PboxTopology(topology, degree, pbox_count, MaxL, admissibility_class), geometry

def pbox(degree : tuple, pbox_count : tuple, periodic = (), MaxL = numpy.infty, admissibility_class = 2, root = None):
    topology, geometry = nutils.mesh.rectilinear([numpy.linspace(0, 1, pbox_count[i] * degree[i] + 1) for i in range(len(degree))], root = root, periodic=periodic)

    return PboxTopology(topology,degree, MaxL, admissibility_class,[[i for i in range(numpy.prod(pbox_count))]]), geometry