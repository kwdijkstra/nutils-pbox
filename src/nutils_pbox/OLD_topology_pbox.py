# this is an extension for nutils that extends the hierarchicaltopology to use p-box
import time

import nutils.topology
import numpy

import nutils_pbox.tools_evaluable
from nutils_pbox.evaluable_pbox import ProjectEvaluable
import nutils_pbox.THB_coarse_refine_functions as THB_coarse_refine_functions
import nutils_pbox.tools_nutils as tools_nutils
from nutils_pbox.tools_nutils import refine_Vec
from typing import Any, FrozenSet, Iterable, Iterator, List, Mapping, Optional, Sequence, Tuple, Union, Sequence

_ArgDict = Mapping[str, numpy.ndarray]

from nutils import sparse

from nutils import function
from typing import Optional
import numpy

from functools import cached_property

_ = numpy.newaxis


def map_pboxInd_elemInd(pbox_indices, pbox_count, level, degreeList):
    pbox_vec_list = tools_nutils.map_index_vec(pbox_indices, pbox_count * (2 ** (level)))
    elem_vec_list = map_pboxVec_elemVec(pbox_vec_list, degreeList)
    elem_ind_list = (tools_nutils.map_vec_index(elem_vec_list, pbox_count * degreeList * (2 ** (level))))
    return numpy.unique(elem_ind_list)


def map_pboxVec_elemVec(pboxVec, degreeList):
    dim = pboxVec.shape[1]
    combinations = tools_nutils.combvec([range(degree) for degree in degreeList])
    offset = combinations.shape[0]

    output = numpy.zeros( (pboxVec.shape[0] * combinations.shape[0],dim))

    for i in range(pboxVec.shape[0]):
        index = i * offset
        output[index:index+offset,:] = degreeList * pboxVec[i,:] + combinations

    return output

def map_elemVec_pboxVec(elemVec, degreeList):
    if elemVec.shape[0] == 0:
        return elemVec
    pboxVec = numpy.zeros(elemVec.shape, dtype=int)
    for i, degree in enumerate(degreeList):
        pboxVec[:,i] = numpy.floor_divide(elemVec[:,i], degree)

    # quick and dirty check for uniqueness.
    I = numpy.zeros(pboxVec.shape[0],dtype=bool)
    I[0] = True
    for j in range(pboxVec.shape[0]-1):
        if any(pboxVec[j+1,:]-pboxVec[j,:]!=0):
            I[j+1] = True

    return pboxVec[I,:]


def whereList(elem_list, list):
    elem_list_count = len(elem_list)
    output = numpy.zeros(elem_list_count, dtype=int)
    elem_list = (numpy.sort(elem_list))

    # print(elem_list)
    # print(list)


    output_index = 0
    elem2check = elem_list[output_index]
    for i, elem in enumerate((list)):
        # print(elem, elem2check)
        if elem == elem2check:
            output[output_index] = i
            output_index += 1
            # print(output_index, elem_list_count)
            if output_index == elem_list_count:
                return output
            elem2check = elem_list[output_index]

    return output

class PboxBaseTopology():

    def __init__(self, NutilsTopology, degree : tuple, pbox_count : tuple, MaxL : int,  admissibility_class : int):
        self.topology = NutilsTopology
        self.degree = numpy.array(degree)
        self.ndims = len(self.degree)
        self.pbox_count = numpy.array(pbox_count)
        self.MaxL = MaxL
        self.m = admissibility_class

        self.active_pboxes_per_level = [ numpy.arange(numpy.prod(pbox_count)) ]

        # additional processing
        self.pbox_offsets = numpy.cumsum([0, *map(len, self.active_pboxes_per_level)], dtype=int)
        self.L = len(self.active_pboxes_per_level)

        self.ElemSize = []
        self.proj_elem = None

        self.len = numpy.sum([len(pboxes) for pboxes in self.active_pboxes_per_level])

    def refined_by_pbox(self, refine_pbox):
        # refines the topology given the refine_pbox list and returns a new PboxHierarchicalTopology object

        # setup array
        if self.m != numpy.infty:
            admissible_class = self.m
        else:
            # set admissibility to be more than the maximum number of levels
            admissible_class = self.L + 1


        pbox_active_indices_per_level = THB_coarse_refine_functions.refine(self.active_pboxes_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree),refine_pbox, admissible_class)

        return PboxHierarchicalTopology(self, pbox_active_indices_per_level)


    def basis(self, type='th-spline', degree=None, continuity=None):
        '''Generate a basis over the nutils_pbox.
        The standard option is the thb-spline basis with the same degree as the nutils_pbox.'''
        if degree is None:
            degree = self.degree
        if continuity is None:
            continuity = numpy.array([max(p - 1, -1) for p in degree])
        if type == "discont" or all(numpy.array(continuity) == -1):
            return tools_nutils.arb_basis_discontinuous(self.topology, degree)
        else:
            return self.topology.basis(name=type, degree=degree, continuity=continuity)

    def project(self,
                func : function.Array,
                onto : function.Array,
                geometry,
                method = "bern",
                bern_degree = None,
                solve_method = 'QR',
                arguments: Optional[_ArgDict] = None,
                refined_topology = None,
                refined_basis = None,
                coarse_topology = None,
                coarse_basis = None,
                mapping = None,
                coeffs = None,
                **kwargs):
        '''Projects the nutils function 'func' onto the nutils_pbox thb-spline basis with the method given in 'method'.
        If func is defined over a more refined mesh, additionally supply refined_topology for integration.'''

        assert  not (refined_topology is not None and coarse_topology is not None)

        t1 = time.perf_counter()

        # generate projection elements
        t0 = time.perf_counter()
        self.GenerateProjectionElement()
        time_pbox = time.perf_counter() - t0

        # project with the ProjectEvaluable class, taking advantage of the nutils evaluable methods.
        if onto is None:
            onto = self.basis()

        if bern_degree is None:
            bern_degree = self.degree

        if refined_topology is not None:
            #
            if method == 'bern':
                func = refined_topology.project_coarsen_preprocess(self, geometry, fun = func, coarse_basis = coarse_basis, arguments= arguments)
            else:
                if refined_basis is not None:
                    for key, item in func.arguments.items():
                        assert arguments[key].shape == item[0]

                        # mapping = nutils_pbox.tools_evaluable.split_projection_coarsening(pbox_refined=refined_topology, pbox_coarse=self, basis_refined = refined_basis, basis_coarse= onto ,coeffs_refined= arguments[key])

                func = refined_topology.project_coarsen_preprocess(self, geometry, fun=func, coarse_basis=onto, arguments=arguments)

        if coarse_topology is not None and coarse_basis is not None:
            # print('here1')
            if coeffs is None:
                for key, item in func.arguments.items():
                    # print(key,item)
                    # print('here2')
                    assert arguments[key].shape == item[0]

                    mapping = nutils_pbox.tools_evaluable.split_projection_refinement(pbox_refined=self, pbox_coarse=coarse_topology, basis_refined=onto, basis_coarse=coarse_topology.basis(degree=bern_degree), coeffs_coarse=arguments[key])
            else:
                mapping = nutils_pbox.tools_evaluable.split_projection_refinement(pbox_refined=self, pbox_coarse=coarse_topology, basis_refined=onto, basis_coarse=coarse_topology.basis(degree=bern_degree), coeffs_coarse=coeffs)

        # print(f'   project setup took {time.perf_counter() - t1} of which MacroElem generation is {time_pbox}')

        t1 = time.perf_counter()
        output = ProjectEvaluable(topo = self.topology, geom = geometry, thbbasis= onto, bernbasis= self.basis('discont', degree = bern_degree), weights = 'integral', macroelems=self.proj_elem, fun= func, integration_degree = 2*max(self.degree), proj_method = method, solve_method = solve_method, arguments=arguments, mapping = mapping).project(**kwargs)
        # output = ProjectEvaluable(topo = self.topology, geom = geometry, thbbasis= onto, bernbasis= self.basis('discont', degree = bern_degree), weights = 'integral', macroelems=self.proj_elem, fun= func, integration_degree = 2*max(self.degree), proj_method = method, solve_method = solve_method, arguments=arguments).project(**kwargs)
        # print(f'   project  took {time.perf_counter() - t1}')
        return output[0], [output[1], time_pbox]

    def project_coarsen_preprocess(self, coarse_topology, geometry, fun : function.Array, coarse_basis: function.Array = None, arguments: Optional[_ArgDict] = None ):
        assert isinstance(coarse_topology, PboxBaseTopology) or isinstance(coarse_topology, PboxHierarchicalTopology)

        if coarse_basis is None:
            coarse_basis = coarse_topology.basis()
        lhs_integral = coarse_basis * fun
        J = nutils.function.J(geometry)
        b_fine = self.topology.integrate_elementwise(lhs_integral * J, degree = 2 * max(self.PboxBaseTopology.degree), arguments = arguments)

        # b_fine is defined over the refined elements, has to be remapped to the coarse elements
        def PboxMapping(original_indices, coarsened_indices, PboxBaseTopology):

            coarse_offsets = numpy.cumsum([0, *map(len, coarsened_indices)], dtype=int)
            original_offsets = numpy.cumsum([0, *map(len, original_indices)], dtype=int)

            numCoarse = numpy.sum([len(indices) for indices in coarsened_indices])

            def recursive_mapper(ielem_level, level, tomap_indices, tomap_offsets):
                if ielem_level in tomap_indices[level]:
                    return [numpy.where(ielem_level == tomap_indices[level])[0][0] + tomap_offsets[level]]
                else:
                    # ielem is refined in the oiginal mapping, hence search for refined ielems
                    ielem_level_vec = tools_nutils.map_index_vec(ielem_level, PboxBaseTopology.pbox_count * PboxBaseTopology.degree * (2 ** (level)))
                    ielem_refined_vec = refine_Vec(ielem_level_vec)
                    ielem_refined_index = tools_nutils.map_vec_index(ielem_refined_vec, PboxBaseTopology.pbox_count * PboxBaseTopology.degree * (2 ** (level + 1)))

                    return numpy.concatenate([recursive_mapper(ielem_refined, level + 1, tomap_indices, tomap_offsets) for ielem_refined in ielem_refined_index])

            elem_mapping_inv = [[] for _ in range(numCoarse)]

            for ielem_coarse in range(numCoarse):
                level = numpy.sum(coarse_offsets <= ielem_coarse) - 1
                ielem_level = coarsened_indices[level][ielem_coarse - coarse_offsets[level]]

                elem_mapping_inv[ielem_coarse] = numpy.sort(recursive_mapper(ielem_level, level, original_indices, original_offsets))

            return elem_mapping_inv

        if isinstance(coarse_topology, PboxHierarchicalTopology):
            mapping_coarse_fine = PboxMapping(self.topology._indices_per_level, coarse_topology.topology._indices_per_level, self.PboxBaseTopology)
        else:
            mapping_coarse_fine = PboxMapping(self.topology._indices_per_level, [ numpy.arange(len(coarse_topology.topology)) ], self.PboxBaseTopology)

        N_e_coarse = len(coarse_topology.topology)

        coarse_elem_dofs = [coarse_basis.get_dofs(e) for e in range(N_e_coarse)]

        b_coarse = [ numpy.zeros(len(dofs)) for dofs in coarse_elem_dofs ]

        for e_coarse in range(N_e_coarse):
            for e_fine in mapping_coarse_fine[e_coarse]:
                b_coarse[e_coarse] += b_fine[e_fine,coarse_elem_dofs[e_coarse]]

        return b_coarse

    def GenerateProjectionPbox(self):
        # pboxBaseTopology has a tensor product structure.
        return [[i] for i in numpy.arange(numpy.prod(self.pbox_count))]

    def GenerateProjectionElement(self):
        if self.proj_elem is not None:
            return

        self.proj_pbox = self.GenerateProjectionPbox()

        offset = numpy.prod(self.degree)
        self.proj_elem = [numpy.zeros(offset * len(box), dtype = int) for box in self.proj_pbox]

        for i, proj_pbox in enumerate(self.proj_pbox):
            for j, pbox in enumerate(proj_pbox):
                pbox_index, level   = self.PboxLevelIndex(pbox)
                pbox_vec            = tools_nutils.map_index_vec(pbox_index, self.pbox_count * 2 ** level)
                elem_vec            = map_pboxVec_elemVec(pbox_vec, self.degree)
                elem_index          = tools_nutils.map_vec_index(elem_vec, self.pbox_count * self.degree * 2 ** level)
                elem                = numpy.array([self.ElemGlobalIndex(elem_index_individual, level) for elem_index_individual in elem_index],dtype=int)

                self.proj_elem[i][j * offset : j * offset + offset] = elem

        self.proj_count = len(self.proj_elem)

        self.elem_proj = numpy.zeros(len(self.topology),dtype=int)
        for ipbox, ielems in enumerate(self.proj_elem):
            for ielem in ielems:
                self.elem_proj[ielem] = ipbox

    def PboxGlobalIndex(self, pbox, level):
        return pbox

    def PboxLevelIndex(self, pbox):
        level = numpy.sum(self.pbox_offsets <= pbox)-1
        return pbox, 0

    def ElemGlobalIndex(self, elem, level):
        return elem


    def integral(self, func: function.IntoArray, ischeme: str = 'gauss', degree: Optional[int] = None, edit=None) -> nutils.function.Array:
        'integral'
        return self.topology.integral(func = func, ischeme = ischeme, degree = degree, edit = edit)

    def integrate_elementwise(self, funcs: Iterable[function.Array], *, degree: int, asfunction: bool = False, ischeme: str = 'gauss', arguments: Optional[_ArgDict] = None) -> Union[List[numpy.ndarray], List[function.Array]]:
        'element-wise integration'
        return self.topology.integrate_elementwise(funcs, degree=degree, asfunction = asfunction, ischeme=ischeme, arguments = arguments)

    def _elementwise_pboxwise(self, data_elementwise, asfunction : bool = False):
        self.GenerateProjectionElement()
        pbox_retvals = numpy.asarray([numpy.sum(data_elementwise[elems]) for elems in self.proj_elem])

        if asfunction:
            return [function.get(pbox_retvals, 0, self.f_index) for retval in data_elementwise]
        else:
            return pbox_retvals

    def integrate_pboxwise(self, funcs: Iterable[function.Array], *, degree: int, asfunction: bool = False, ischeme: str = 'gauss', arguments: Optional[_ArgDict] = None) -> Union[List[numpy.ndarray], List[function.Array]]:
        'pbox-wise integration'

        # nutils code for elementwise integrations
        retvals = self.integrate_elementwise(funcs, degree=degree, asfunction = asfunction, ischeme=ischeme, arguments = arguments)
        # contract over pboxes
        return self._elementwise_pboxwise(retvals, asfunction)

    @cached_property
    def boundary(self) -> 'Topology':
        return self.topology.boundary

    def sample(self, ischeme: str, degree: int) -> nutils.sample.Sample:
        return self.topology.sample(ischeme= ischeme, degree= degree)


    def find_elem_size(self, geometry):
        J = function.J(geometry)
        elemSizeSparse = tools_nutils.integrate_elementwise_sparse(self.topology, [J], degree=0)
        indices, elemSize, shape = sparse.extract(elemSizeSparse[0])
        self.ElemSize = elemSize

    def elem_size(self, elem, geometry):
        if len(self.ElemSize) == 0:
            self.find_elem_size(geometry)
        return self.ElemSize[elem]

class PboxHierarchicalTopology(PboxBaseTopology):
    def __init__(self, PboxBaseTopology : PboxBaseTopology, active_pboxes_per_level : list):
        self.PboxBaseTopology = PboxBaseTopology
        self.active_pboxes_per_level = active_pboxes_per_level

        # additional processing
        self.pbox_offsets = numpy.cumsum([0, *map(len, active_pboxes_per_level)], dtype=int)
        self.L = len(active_pboxes_per_level)

        # functional processing
        self.topology = self.GenerateHierarchcialTopology()
        self.pbox_refined_indices_per_level = self.FindRefinedIndicesPerLevel()

        # set additional variables
        self.degree = self.PboxBaseTopology.degree
        self.ndims = len(self.degree)
        self.pbox_count = self.PboxBaseTopology.pbox_count
        self.m = self.PboxBaseTopology.m
        self.MaxL = self.PboxBaseTopology.MaxL

        self.ElemSize = []
        self.proj_elem = None

        self.len = numpy.sum([len(pboxes) for pboxes in self.active_pboxes_per_level])

    def refined_by_pbox(self, refine_pbox):
        # refines the topology given the refine_pbox list and returns a new PboxHierarchicalTopology object

        # setup array
        if self.PboxBaseTopology.m != numpy.infty:
            admissible_class = self.PboxBaseTopology.m
        else:
            # set admissibility to be more than the maximum number of levels
            admissible_class = self.L + 1

        pbox_active_indices_per_level = THB_coarse_refine_functions.refine(self.active_pboxes_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree),refine_pbox, admissible_class)

        return PboxHierarchicalTopology(self.PboxBaseTopology, pbox_active_indices_per_level)

    def coarsen_by_pbox(self, coarsen):
        if self.PboxBaseTopology.m != numpy.infty:
            admissible_class = self.PboxBaseTopology.m
        else:
            admissible_class = self.L + 1

        coarsen = numpy.array(coarsen)

        # refine the pboxes
        coarsened_active_indices_per_level = THB_coarse_refine_functions.coarsen(self.active_pboxes_per_level, self.pbox_offsets, self.PboxBaseTopology.pbox_count, [1] * len(self.PboxBaseTopology.degree), coarsen, admissible_class)

        for _ in range(len(coarsened_active_indices_per_level)):
            if len(coarsened_active_indices_per_level[-1]) == 0:
                coarsened_active_indices_per_level.pop(-1)
            else:
                break

        if len(coarsened_active_indices_per_level) == 1:
            return self.PboxBaseTopology # coarsened to the lowest level tensor product mesh
        else:
            return PboxHierarchicalTopology(self.PboxBaseTopology, coarsened_active_indices_per_level)

    def GenerateHierarchcialTopology(self):
        # updates the topology by transforming the p-box structure to element structure.

        # generate data structure
        element_indices_per_level = [[] for _ in self.active_pboxes_per_level]


        # loop over levels
        for ilevel, pbox_indices in enumerate(self.active_pboxes_per_level):
            # generate elements for each p-box
            pbox_vec_list = tools_nutils.map_index_vec(pbox_indices, self.PboxBaseTopology.pbox_count * (2 ** (ilevel)))
            elem_vec_list = map_pboxVec_elemVec(pbox_vec_list, self.PboxBaseTopology.degree)
            # print(elem_vec_list)
            elem_ind_list = (tools_nutils.map_vec_index(elem_vec_list, self.PboxBaseTopology.pbox_count * self.PboxBaseTopology.degree * (2 ** (ilevel))))
            element_indices_per_level[ilevel] = numpy.unique(elem_ind_list)


        # create topology (code snippit taken from nutils)
        # need refined boolean for initial refinement
        if self.L > 1:
            return nutils.topology.HierarchicalTopology(self.PboxBaseTopology.topology, ([numpy.unique(numpy.array(i, dtype=int)) for i in element_indices_per_level]) )
        else:
            # coarsened to initial basis topology (should not occur here)
            raise 'error, only one level in a hierarchical topology'
            return self.topology.basetopo



    def GenerateProjectionPbox(self):
        # generates a list [macro elements] of lists [the active pboxes making up the macro element]
        if self.PboxBaseTopology.m == 2:
            return [ [i] for i in range(self.pbox_offsets[-1]) ]

        active_Pbox_proj_elem = [] # active Pboxes making up a projection element
        parent_Pbox_proj_elem = [] # the parent well-behaved border nutils_pbox

        def CoveredPbox(pbox, level):
            # check wether the nutils_pbox is contained in one of the active nutils_pbox proj elem
            for parent in parent_Pbox_proj_elem:
                parent_level = self.L - len(parent)
                if pbox in parent[level-parent_level]:
                    return True
            return False

        def CoveredPboxCount(pbox, level):
            # check wether the nutils_pbox is conained in one of the active nutils_pbox proj elem
            for i,parent in enumerate(parent_Pbox_proj_elem):
                parent_level = self.L - len(parent)
                if level < parent_level:
                    continue
                if pbox in parent[level-parent_level]:
                    return True, i
            return False, -1

        for level in range(self.L):
            # get well-behaved-border-pboxes
            well_behaved_border_pbox_level, well_behaved_border_level_parent = self.find_well_behaved_border_p_box_level(level)

            # add all well-behaved border pboxes that are not already contained in a different active_pbox
            for i,wbb_pbox in enumerate(well_behaved_border_pbox_level):
                if not CoveredPbox(wbb_pbox,level):
                    if wbb_pbox in self.active_pboxes_per_level[level]:
                        active_Pbox_proj_elem.append([self.PboxGlobalIndex(wbb_pbox,level)])
                    else:
                        active_Pbox_proj_elem.append([])
                    parent_Pbox_proj_elem.append(well_behaved_border_level_parent[i])

        for i in range(self.pbox_offsets[-1]):
            pbox, level = self.PboxLevelIndex(i)
            Covered, loc =  CoveredPboxCount(pbox, level)
            if Covered and i not in active_Pbox_proj_elem[loc]:
                active_Pbox_proj_elem[loc].append(i)
            elif not any([i in pbox_proj_elem for pbox_proj_elem in active_Pbox_proj_elem]):
                active_Pbox_proj_elem.append([i])

        # print(active_Pbox_proj_elem)

        return active_Pbox_proj_elem

    def FindRefinedIndicesPerLevel(self):
        # finds a list of refined Pboxes

        prev_level_refined = numpy.array([],dtype=int)
        refined_pboxes = [[]] * len(self.active_pboxes_per_level)

        pbox_tot_count = numpy.prod(self.PboxBaseTopology.pbox_count)
        increase_factor = 2 ** len(self.PboxBaseTopology.pbox_count)

        for level, active_levels in enumerate(self.active_pboxes_per_level):
            level_pbox_tot_count = pbox_tot_count*increase_factor**level
            all_pbox_indices = numpy.arange(0, level_pbox_tot_count, dtype=int)
            # remove the active elements
            # remove the previous level active elements
            to_remove_indices = numpy.append(prev_level_refined,active_levels)

            all_pbox_indices = numpy.delete(all_pbox_indices, to_remove_indices)

            # all_pbox_indices[to_remove_indices] = []

            refined_pboxes[level] = all_pbox_indices

            # refine to_remove_indices for next level
            vec_to_refine = tools_nutils.map_index_vec(to_remove_indices, self.PboxBaseTopology.pbox_count * 2 ** (level))
            vec_refined = refine_Vec(vec_to_refine)
            prev_level_refined = numpy.array(
                tools_nutils.map_vec_index(vec_refined, self.PboxBaseTopology.pbox_count * 2 ** (level + 1)), dtype=int)

        return refined_pboxes

    def find_well_behaved_border_p_box_level(self, level):
        refinement_domain = self.active_pboxes_per_level[level]
        if level < len(self.pbox_refined_indices_per_level):
            refinement_domain = numpy.append(refinement_domain,self.pbox_refined_indices_per_level[level])

        pbox_vec_list = tools_nutils.map_index_vec(refinement_domain, self.PboxBaseTopology.pbox_count * 2 ** level)

        well_behaved_border_level = []

        for i,pbox in enumerate(refinement_domain):  # loop over all pboxes
            # find nutils_pbox corner, which overlaps with previous level mesh. Use this corner to check the relevant edges
            corner_vec = pbox_vec_list[i, :] % 2
            corner_ind = corner_vec[0] + 2 * corner_vec[1]

            corner_normals = (-1 + 2 * corner_vec)
            indices = tools_nutils.combvec([numpy.array([0, offset]) for offset in corner_normals])[1:, :]
            # check relevant edges
            vec2check = pbox_vec_list[i, :] + indices
            ind2check = tools_nutils.map_vec_index(vec2check, self.PboxBaseTopology.pbox_count * (2 ** (level)))
            Neighbours = [element not in refinement_domain and element >= 0 for element in ind2check]

            if any(Neighbours):
                well_behaved_border_level.append(pbox)


        well_behaved_border_level_nested = [[[element],] for element in well_behaved_border_level]

        # generate the resulting higher level p-boxes contained in these well-behaved border elements
        for i, pbox in enumerate(well_behaved_border_level_nested):
            pbox_vec_list = tools_nutils.map_index_vec(pbox[0], self.PboxBaseTopology.pbox_count * 2 ** level)
            for lvl in range(level+1,self.L):
                pbox_vec_list = refine_Vec(pbox_vec_list)
                pbox_ind_list = tools_nutils.map_vec_index(pbox_vec_list, self.PboxBaseTopology.pbox_count * 2 ** (lvl))
                well_behaved_border_level_nested[i].append(pbox_ind_list)


        return well_behaved_border_level, well_behaved_border_level_nested


    def PboxGlobalIndex(self, pbox, level):
        return numpy.sum(self.active_pboxes_per_level[level]< pbox) + self.pbox_offsets[level]

    def PboxLevelIndex(self, pbox):
        level = numpy.sum(self.pbox_offsets <= pbox)-1
        return self.active_pboxes_per_level[level][pbox - self.pbox_offsets[level]], level

    def ElemGlobalIndex(self, elem, level):
        return numpy.sum(self.topology._indices_per_level[level] < elem) + self.topology._offsets[level]



class PboxTopology():
    '''The Pbox class.

    This is the main class from which the following can be done:
    - generate a nutils topology with a nutils_pbox structure.
    - generate basis functions over this topology.
    - project any nutils function onto the thb-spline basis over this topology.
    '''

    def __init__(self, init_topology, degree : tuple, pbox_count : tuple, MaxL = numpy.infty, admissibility_class = 2):
        '''
        The main class that aims to replace the standard nutils.topology object for a nutils_pbox-topology object.

        - degree                : tuple of ints : spline degree of THB-splines
        - pbox_count            : tuple of ints : number of nutils_pbox in every dimension
        - MaxL                  : int           : maximum refinement level
        - admissibility_class   : int           : admissibility class


        Projection example:

        import numpy
        from nutils_pbox import PboxTopology
        from nutils.expression_v2 import Namespace

        nutils_pbox = PboxTopology.Pbox((2,2), (3,3), admissibility_class=2)
        nutils_pbox.refined_by_pbox([0])
        nutils_pbox.refined_by_pbox([3,4,5])

        ns = Namespace()
        ns.x = nutils_pbox.geometry
        ns.fun = ' '.join([f"sin( {numpy.pi} x_{i} ) " for i in range(len(degree))])

        args = {'phi' : nutils_pbox.project(ns.fun)[0]}
        '''
        self.refined = False
        self.degree = numpy.array(degree,dtype=int)
        self.ndims = len(self.degree)
        self.pbox_count = numpy.array(pbox_count,dtype=int)
        self.elem_count = numpy.array([pbox_count[i] * degree[i] for i in range(len(degree))],dtype=int)

        self.topology = init_topology

        self.pbox_active_indices_per_level = tuple([numpy.arange( numpy.prod(pbox_count)  )])
        self.pbox_refined_indices_per_level = tuple([])
        self.L = 1
        self.MaxL = MaxL
        self.m = admissibility_class
        self.pbox_offsets = numpy.array([0, numpy.prod(pbox_count)])

        self.basis_obj = None

        self.ElemSize = []

    def project(self, func : function.Array, onto : function.Array, geometry, method = "bern", bern_degree = None, solve_method = 'QR', arguments: Optional[_ArgDict] = None, **kwargs):
        '''Projects the nutils function 'func' onto the nutils_pbox thb-spline basis with the method given in 'method'.'''

        # generate projection elements
        t0 = time.perf_counter()
        self.GenerateProjectionElement()
        time_pbox = time.perf_counter() - t0


        # project with the ProjectEvaluable class, taking advantage of the nutils evaluable methods.
        if onto is None:
            onto = self.basis()

        if bern_degree is None:
            bern_degree = self.degree

        output = ProjectEvaluable(topo = self.topology, geom = geometry, thbbasis= onto, bernbasis= self.basis('discont', degree = bern_degree), weights = 'integral', macroelems=self.proj_elem, fun= func, integration_degree = 2*max(self.degree), proj_method = method, solve_method = solve_method, arguments=arguments).project(**kwargs)

        return output[0], [output[1], time_pbox]
    def refined_by(self, refine):
        self.topology = self.topology.refined_by(refine)
        return self.topology

    def basis(self, type = 'th-spline', degree = None, continuity = None):
        '''Generate a basis over the nutils_pbox.
        The standard option is the thb-spline basis with the same degree as the nutils_pbox.'''
        if self.basis_obj != None:
            return self.basis_obj
        if degree is None:
            degree = self.degree
        if continuity is None:
            continuity = numpy.array( [ max(p - 1, -1) for p in degree ])
        if type == "discont" or all(numpy.array(continuity) == -1):
            return tools_nutils.arb_basis_discontinuous(self.topology, degree)
        else:
            return self.topology.basis(name= type, degree = degree, continuity=continuity)

    def integral(self, func: function.IntoArray, ischeme: str = 'gauss', degree: Optional[int] = None, edit=None) -> nutils.function.Array:
        'integral'
        return self.topology.integral(func = func, ischeme = ischeme, degree = degree, edit = edit)

    def integrate_pboxwise(self, funcs: Iterable[function.Array], *, degree: int, asfunction: bool = False, ischeme: str = 'gauss', arguments: Optional[_ArgDict] = None) -> Union[List[numpy.ndarray], List[function.Array]]:
        'element-wise integration'

        # nutils code for elementwise integrations
        retvals = self.topology.integrate_elementwise(funcs, degree=degree, asfunction = asfunction, ischeme=ischeme, arguments = arguments)
        # contract over pboxes
        self.GenerateProjectionElement()
        pbox_retvals = [ numpy.sum(retvals[elems]) for elems in self.proj_elem ]

        if asfunction:
            return [function.get(pbox_retvals, 0, self.f_index) for retval in retvals]
        else:
            return pbox_retvals

    @cached_property
    def boundary(self) -> 'Topology':
        return self.topology.boundary

    def sample(self, ischeme: str, degree: int) -> nutils.sample.Sample:
        return self.topology.sample(ischeme= ischeme, degree= degree)

    def coarsen_by_pbox(self, coarsen):
        if self.m != numpy.infty:
            self.basis_obj = None
            return self.coarsen_by_pbox_admissible_class(coarsen, self.m)
        else:
            raise f'not implemented'

    def refined_by_pbox(self, refine):
        # map refine to pboxes over every level
        # results in three arrays,
        # pbox_active_indices_per_level : the active p-boxes per level
        # pbox_refinened_indices_per_level : the p-boxes that are refined to a lower level.
        # pbox_offsets : given active p-box array index, is used to map to correct level p-box
        #
        # together the active and refined p-boxes define the full refinement domain.

        # check if elements to refine go above level
        splits = numpy.searchsorted(refine, self.pbox_offsets, side='left')
        if len(splits) > self.MaxL-1:
            refine = refine[:splits[self.MaxL-1]]

        if len(refine) == 0:
            return self

        # setup array
        if self.m != numpy.infty:
            return self.refined_by_pbox_admissible_class(refine, self.m)

        refine = numpy.array(refine)

        # find splits and extend the indices_per_level lists
        splits = numpy.searchsorted(refine, self.pbox_offsets, side='left')
        active_indices_per_level = list(map(list, self.pbox_active_indices_per_level)) + [[]]
        refined_indices_per_level = list(map(list, self.pbox_refined_indices_per_level)) + [[]]

        if len(splits)>self.MaxL + 1:
            splits = splits[0:self.MaxL + 1]
        if numpy.sum(splits) == 0:
            print(f"Elements to alter are beyond max level")
            return self.topology, False


        # looping over every level
        for ilevel, (start, stop) in enumerate(zip(splits[:-1], splits[1:])):
            # map active-p-box index to level index and add to refined-index-list
            pbox_ind_list_ilevel = tuple(map(active_indices_per_level[ilevel].pop, reversed(refine[start:stop] - self.pbox_offsets[ilevel])))
            refined_indices_per_level[ilevel].extend(pbox_ind_list_ilevel)

            # map p-box level index to refined indices
            pbox_vec_list_ilevel = tools_nutils.map_index_vec(pbox_ind_list_ilevel, self.pbox_count * 2 ** (ilevel))
            pbox_vec_list_ilevel_refined = refine_Vec(pbox_vec_list_ilevel)
            pbox_index_list_ilevel_refined = tools_nutils.map_vec_index(pbox_vec_list_ilevel_refined, self.pbox_count * 2 ** (ilevel + 1))

            # add new active levels to active-indices array
            active_indices_per_level[ilevel+1].extend( pbox_index_list_ilevel_refined )

        # make sure that last level array is non-empty
        if not active_indices_per_level[-1]:
            active_indices_per_level.pop()

        if not refined_indices_per_level[-1]:
            refined_indices_per_level.pop()

        # clean up
        self.pbox_active_indices_per_level = [numpy.unique(numpy.array(i, dtype=int)) for i in active_indices_per_level]
        self.pbox_refined_indices_per_level = [numpy.unique(numpy.array(i, dtype=int)) for i in refined_indices_per_level]
        self.pbox_offsets = numpy.cumsum([0, *map(len, active_indices_per_level)], dtype=int)
        self.L = len(self.pbox_active_indices_per_level)

        # update topology with nutils
        self.GenerateHierarchcialTopology()

        self.ElemSize = []

        # generate projection elements
        # self.GenerateProjectionElement()

        # reset basis
        self.basis_obj = None

        return self.topology, True

    def refined_by_pbox_admissible_class(self, refine, m):
        refine = numpy.array(refine)

        # refine the pboxes
        self.pbox_active_indices_per_level = THB_coarse_refine_functions.refine(self.pbox_active_indices_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree), refine, m)
        self.FindRefinedIndicesPerLevel()

        # recalculate offsets
        self.pbox_offsets = [len(list_elem) for list_elem in self.pbox_active_indices_per_level]
        self.pbox_offsets = numpy.insert(numpy.cumsum((self.pbox_offsets)), 0, 0)

        self.L = len(self.pbox_active_indices_per_level)

        # update topology with nutils
        self.GenerateHierarchcialTopology()

        self.ElemSize = []

        self.basis_obj = None

        # return self.topology, True
        #
        # PboxHierarchicalTopology(PboxBaseTopology):

        return PboxHierarchicalTopology( PboxBaseTopology = self, active_pboxes_per_level = self.pbox_active_indices_per_level)

    def coarsen_by_pbox_admissible_class(self, coarsen, m):
        coarsen = numpy.array(coarsen)

        # refine the pboxes
        self.pbox_active_indices_per_level = THB_coarse_refine_functions.coarsen(self.pbox_active_indices_per_level, self.pbox_offsets, self.pbox_count, [1] * len(self.degree), coarsen, m)

        for _ in range(len(self.pbox_active_indices_per_level)):
            if len(self.pbox_active_indices_per_level[-1]) == 0:
                self.pbox_active_indices_per_level.pop(-1)
            else:
                break

        self.FindRefinedIndicesPerLevel()

        # recalculate offsets
        self.pbox_offsets = [len(list_elem) for list_elem in self.pbox_active_indices_per_level]
        self.pbox_offsets = numpy.insert(numpy.cumsum((self.pbox_offsets)), 0, 0)

        self.L = len(self.pbox_active_indices_per_level)

        # update topology with nutils
        self.GenerateHierarchcialTopology()

        self.ElemSize = []

        self.basis_obj = None

        return self.topology, True


    def FindRefinedIndicesPerLevel(self):
        # finds a list of refined Pboxes

        prev_level_refined = numpy.array([],dtype=int)
        refined_pboxes = [[]] * len(self.pbox_active_indices_per_level)

        pbox_tot_count = numpy.prod(self.pbox_count)
        increase_factor = 2 ** len(self.pbox_count)

        for level, active_levels in enumerate(self.pbox_active_indices_per_level):
            level_pbox_tot_count = pbox_tot_count*increase_factor**level
            all_pbox_indices = numpy.arange(0, level_pbox_tot_count, dtype=int)
            # remove the active elements
            # remove the previous level active elements
            to_remove_indices = numpy.append(prev_level_refined,active_levels)

            all_pbox_indices = numpy.delete(all_pbox_indices, to_remove_indices)

            # all_pbox_indices[to_remove_indices] = []

            refined_pboxes[level] = all_pbox_indices

            # refine to_remove_indices for next level
            vec_to_refine = tools_nutils.map_index_vec(to_remove_indices, self.pbox_count * 2 ** (level))
            vec_refined = refine_Vec(vec_to_refine)
            prev_level_refined = numpy.array(
                tools_nutils.map_vec_index(vec_refined, self.pbox_count * 2 ** (level + 1)), dtype=int)

        self.pbox_refined_indices_per_level = refined_pboxes


    def GenerateHierarchcialTopology(self):
        # updates the topology by transforming the p-box structure to element structure.

        # generate data structure
        element_indices_per_level = [[] for _ in self.pbox_active_indices_per_level]

        # loop over levels
        for ilevel, pbox_indices in enumerate(self.pbox_active_indices_per_level):
            # generate elements for each p-box
            pbox_vec_list = tools_nutils.map_index_vec(pbox_indices, self.pbox_count * (2 ** (ilevel)))
            elem_vec_list = map_pboxVec_elemVec(pbox_vec_list, self.degree)
            elem_ind_list = (tools_nutils.map_vec_index(elem_vec_list, self.elem_count * (2 ** (ilevel))))
            element_indices_per_level[ilevel] = numpy.unique(elem_ind_list)

        # create topology (code snippit taken from nutils)
        # need refined boolean for initial refinement
        if self.refined and self.L > 1:
            self.topology = nutils.topology.HierarchicalTopology(self.topology.basetopo, ([numpy.unique(numpy.array(i, dtype=int)) for i in element_indices_per_level]) )
        elif self.refined and self.L == 1:
            # coarsened to initial basis topology
            self.refined = False
            self.topology = self.topology.basetopo
        else:
            self.refined  = True
            self.topology = nutils.topology.HierarchicalTopology(self.topology, ([numpy.unique(numpy.array(i, dtype=int)) for i in element_indices_per_level]))

    def find_well_behaved_border_p_box_level(self, level):
        refinement_domain = self.pbox_active_indices_per_level[level]
        if level < len(self.pbox_refined_indices_per_level):
            refinement_domain = numpy.append(refinement_domain,self.pbox_refined_indices_per_level[level])

        pbox_vec_list = tools_nutils.map_index_vec(refinement_domain, self.pbox_count * 2 ** level)

        well_behaved_border_level = []

        for i,pbox in enumerate(refinement_domain):  # loop over all pboxes
            # find nutils_pbox corner, which overlaps with previous level mesh. Use this corner to check the relevant edges
            corner_vec = pbox_vec_list[i, :] % 2
            corner_ind = corner_vec[0] + 2 * corner_vec[1]

            corner_normals = (-1 + 2 * corner_vec)
            indices = tools_nutils.combvec([numpy.array([0, offset]) for offset in corner_normals])[1:, :]
            # check relevant edges
            vec2check = pbox_vec_list[i, :] + indices
            ind2check = tools_nutils.map_vec_index(vec2check, self.pbox_count * (2 ** (level)))
            Neighbours = [element not in refinement_domain and element >= 0 for element in ind2check]

            if any(Neighbours):
                well_behaved_border_level.append(pbox)


        well_behaved_border_level_nested = [[[element],] for element in well_behaved_border_level]

        # generate the resulting higher level p-boxes contained in these well-behaved border elements
        for i, pbox in enumerate(well_behaved_border_level_nested):
            pbox_vec_list = tools_nutils.map_index_vec(pbox[0], self.pbox_count * 2 ** level)
            for lvl in range(level+1,self.L):
                pbox_vec_list = refine_Vec(pbox_vec_list)
                pbox_ind_list = tools_nutils.map_vec_index(pbox_vec_list, self.pbox_count * 2 ** (lvl))
                well_behaved_border_level_nested[i].append(pbox_ind_list)


        return well_behaved_border_level, well_behaved_border_level_nested

    def map_elem_pbox(self, elem_global_ind):
        elem_level_ind = {l : [] for l in range(self.L)}
        for elem in elem_global_ind:
            elem_level_index, level = self.ElemLevelIndex(elem)
            elem_level_ind[level].append(elem_level_index)

        pbox_list = []

        for level in range(self.L):
            elemVec = tools_nutils.map_index_vec(elem_level_ind[level], self.elem_count * 2 ** (level))
            pboxVec = map_elemVec_pboxVec(elemVec, self.degree)
            pbox_level_ind = tools_nutils.map_vec_index(pboxVec, self.pbox_count * 2 ** level)
            for pbox in pbox_level_ind:
                pbox_list.append(self.PboxGlobalIndex(pbox, level))

        return pbox_list

    def PboxGlobalIndex(self, pbox, level):
        return numpy.sum(self.pbox_active_indices_per_level[level]< pbox) + self.pbox_offsets[level]

    def PboxLevelIndex(self, pbox):
        level = numpy.sum(self.pbox_offsets <= pbox)-1
        return self.pbox_active_indices_per_level[level][pbox - self.pbox_offsets[level]], level

    def ElemGlobalIndex(self, elem, level):
        if self.refined:
            return numpy.sum(self.topology._indices_per_level[level]< elem) + self.topology._offsets[level]
        else:
            return elem

    def ElemLevelIndex(self, elem):
        if self.refined:
            level = numpy.sum(self.topology._offsets <= elem) - 1
            return self.topology._indices_per_level[level][elem - self.topology._offsets[level]], level
        else:
            return elem, 0

    def GenerateProjectionPbox(self):
        # generates a list [macro elements] of lists [the active pboxes making up the macro element]
        if self.m == 2:
            self.proj_pbox = [ [i] for i in range(self.pbox_offsets[-1]) ]
            return


        active_Pbox_proj_elem = [] # active Pboxes making up a projection element
        parent_Pbox_proj_elem = [] # the parent well-behaved border nutils_pbox

        def CoveredPbox(pbox, level):
            # check wether the nutils_pbox is contained in one of the active nutils_pbox proj elem
            for parent in parent_Pbox_proj_elem:
                parent_level = self.L - len(parent)
                if pbox in parent[level-parent_level]:
                    return True
            return False

        def CoveredPboxCount(pbox, level):
            # check wether the nutils_pbox is conained in one of the active nutils_pbox proj elem
            for i,parent in enumerate(parent_Pbox_proj_elem):
                parent_level = self.L - len(parent)
                if level < parent_level:
                    continue
                if pbox in parent[level-parent_level]:
                    # print("nutils_pbox",nutils_pbox)
                    # print("parent",parent)
                    # print("level",level)
                    # print("parent level",parent_level)
                    return True, i
            return False, -1

        for level in range(self.L):
            # get well-behaved-border-pboxes
            well_behaved_border_pbox_level, well_behaved_border_level_parent = self.find_well_behaved_border_p_box_level(level)

            # add all well-behaved border pboxes that are not already contained in a different active_pbox
            for i,wbb_pbox in enumerate(well_behaved_border_pbox_level):
                if not CoveredPbox(wbb_pbox,level):
                    if wbb_pbox in self.pbox_active_indices_per_level[level]:
                        active_Pbox_proj_elem.append([self.PboxGlobalIndex(wbb_pbox,level)])
                    else:
                        active_Pbox_proj_elem.append([])
                    parent_Pbox_proj_elem.append(well_behaved_border_level_parent[i])

        for i in range(self.pbox_offsets[-1]):
            pbox, level = self.PboxLevelIndex(i)
            Covered, loc =  CoveredPboxCount(pbox, level)
            if Covered and i not in active_Pbox_proj_elem[loc]:
                active_Pbox_proj_elem[loc].append(i)
            elif not any([i in pbox_proj_elem for pbox_proj_elem in active_Pbox_proj_elem]):
                active_Pbox_proj_elem.append([i])

        self.proj_pbox = active_Pbox_proj_elem

    def GenerateProjectionElement(self):
        self.GenerateProjectionPbox()

        offset = numpy.prod(self.degree)
        self.proj_elem = [numpy.zeros(offset * len(box), dtype = int) for box in self.proj_pbox]

        for i, proj_pbox in enumerate(self.proj_pbox):
            for j, pbox in enumerate(proj_pbox):
                pbox_index, level = self.PboxLevelIndex(pbox)
                pbox_vec = tools_nutils.map_index_vec(pbox_index, self.pbox_count * 2 ** level)
                elem_vec = map_pboxVec_elemVec(pbox_vec, self.degree)
                elem_index = tools_nutils.map_vec_index(elem_vec, self.elem_count * 2 ** level)
                elem = numpy.array(
                    [self.ElemGlobalIndex(elem_index_individual, level) for elem_index_individual in elem_index],
                    dtype=int)

                self.proj_elem[i][j * offset : j * offset + offset] = elem


        self.proj_count = len(self.proj_elem)

    def GetActivePboxElement(self):
        # get element indices of each active nutils_pbox

        # self.pbox_active_indices_per_level
        # number of active pboxes
        N = sum([len(sub_array) for sub_array in self.pbox_active_indices_per_level])

        offset = numpy.prod(self.degree)
        active_pbox_elem = [numpy.zeros(offset, dtype=int)] * N

        for pbox in range(N):
            pbox_index, level = self.PboxLevelIndex(pbox)
            pbox_vec = tools_nutils.map_index_vec(pbox_index, self.pbox_count * 2 ** level)
            elem_vec = map_pboxVec_elemVec(pbox_vec, self.degree)
            elem_index = tools_nutils.map_vec_index(elem_vec, self.elem_count * 2 ** level)
            elem = numpy.array(
                [self.ElemGlobalIndex(elem_index_individual, level) for elem_index_individual in elem_index],
                dtype=int)
            active_pbox_elem[pbox] = elem

        return active_pbox_elem


    def find_elem_size(self, geometry):
        J = function.J(geometry)
        elemSizeSparse = tools_nutils.integrate_elementwise_sparse(self.topology, [J], degree=0)
        indices, elemSize, shape = sparse.extract(elemSizeSparse[0])
        self.ElemSize = elemSize

    def elem_size(self, elem, geometry):
        if len(self.ElemSize) == 0:
            self.find_elem_size(geometry)
        return self.ElemSize[elem]
