import numpy, math
import nutils.topology
from nutils.expression_v2 import Namespace
from nutils import function, mesh, solver, numeric, export, types, sparse
from nutils import element, function, evaluable, _util as util, parallel, numeric, cache, transform, transformseq, warnings, types, points, sparse
from nutils.element import LineReference, TensorReference
from nutils._util import single_or_multiple
from functools import cached_property
from nutils.elementseq import References
from nutils.pointsseq import PointsSequence
from nutils.sample import Sample
import nutils_poly as poly
import typing


# contains code that are either somewhere in nutils that I have not yet found, or functions in nutils that have need to be extended for my use case.
def map_index_vec(index_list, counts):
    # reverse counts for simplicity and get cumprod
    index_list = numpy.array(index_list,dtype=int)
    counts = list(reversed(counts))
    counts.insert(0,1)
    CumProdCounts = list(reversed(numpy.cumprod(list(counts), dtype=int)))
    CumProdCounts.pop(0)

    # preallocate memory
    vec_list = numpy.zeros((index_list.size,len(counts)-1),dtype=int)

    # for cumprod counts, get the division as the j'th vector component
    # the remainder contains the remaining vector components
    index_altered = index_list
    for j in range(len(CumProdCounts)):
        vec_list[:,j], index_altered = numpy.divmod(index_altered, CumProdCounts[j])

    return vec_list


def map_vec_index(vec_list, counts, remove_non_existent = False):
    # create list of cumulative products to offset indices by
    cumProdCounts = numpy.cumprod(list(reversed(counts)),dtype=int)
    cumProdCounts = numpy.insert(cumProdCounts, 0, 1)

    # check vector index bounds
    for i, count in enumerate(counts):
        vec_list[ vec_list[:, i] < 0     , i] = -cumProdCounts[-1]
        vec_list[ vec_list[:, i] >= count, i] = -cumProdCounts[-1]

    # preallocate memory
    final_ind = numpy.zeros(vec_list.shape[0],dtype=int)

    # insert values indices, multiplied by offset counts
    for i, count in enumerate(reversed(cumProdCounts[:-1])):
        # print(i,count)
        final_ind = final_ind + count * vec_list[:,i]

    if remove_non_existent:
        final_ind = numpy.delete(final_ind,final_ind < 0)
    else:
        # mark non existent elements as -1
        final_ind[final_ind < 0] = -1

    return final_ind

def combvec(list):
    # input a list of n numpy lists
    # output a n x m array where all combinations are row vectors

    list1 = numpy.array([list.pop()]).T # get a sublist to add
    if not list:
        return list1 # if there are no remaining sub lists, return list1 as solution
    list2 = combvec(list) # else find the combinations of the remaining lists

    # find list sizes. 1D arrays require some clean up
    size1 = list1.shape
    size2 = list2.shape

    # preallocate
    output = numpy.zeros( (size1[0] * size2[0], size1[1] + size2[1]), dtype=int )

    # get combinations
    for i, elem in enumerate(list1):
        output[size2[0] * i:size2[0] * (i+1), -1 ] = elem
        output[size2[0] * i:size2[0] * (i+1), :-1] = list2

    return output


def integrate_elementwise_sparse(self, funcs, degree: int, asfunction: bool = False, ischeme: str = 'gauss',
                                 arguments=None):
    'element-wise integration'

    retvals = [retval for retval in self.sample(ischeme, degree).integrate_sparse(
        [function.kronecker(func, pos=self.f_index, length=len(self), axis=0) for func in funcs],
        arguments=arguments)]
    return retvals


def _get_poly_coeffs(reference, degree):
    assert len(degree) == reference.ndims
    if len(degree) == 1:
        assert isinstance(reference, LineReference)
        return reference.get_poly_coeffs('bernstein', degree[0].__index__())
    else:
        assert isinstance(reference, TensorReference)
        ref1 = reference.ref1
        ref2 = reference.ref2
        p1 = _get_poly_coeffs(ref1, degree[:ref1.ndims])
        p2 = _get_poly_coeffs(ref2, degree[ref1.ndims:])
        coeffs = poly.mul_different_vars(p1[:, None], p2[None, :], ref1.ndims, ref2.ndims)
        return coeffs.reshape(-1, coeffs.shape[-1])

def arb_basis_discontinuous(topology, degree):
    if topology.references.isuniform:
        coeffs = [_get_poly_coeffs(topology.references[0], degree)] * len(topology)
    else:
        coeffs = [_get_poly_coeffs(ref, degree) for ref in topology.references]

    return function.DiscontBasis(coeffs, topology.f_index, topology.f_coords)

class UniformDiscontBasis(function.Basis):
    '''A discontinuous basis with monotonic increasing dofs and the same coefficients for each element.

    Parameters
    ----------
    coeffs : :class:`numpy.ndarray`
        The coefficients of the basis functions for a single element.
    nelems : :class:`int`
        The number of elements.
    index : :class:`Array`
        The element index.
    coords : :class:`Array`
        The element local coordinates.
    '''

    def __init__(self, coeffs: numpy.ndarray, nelems: int, index: function.Array, coords: function.Array) -> None:
        self.coeffs = numpy.array(coeffs)
        if self.coeffs.ndim != 2:
            raise ValueError(f"expected a coeffs array with 2 dimensions but got {self.coeffs.ndim}")
        super().__init__(
            ndofs=nelems * len(self.coeffs),
            nelems=nelems,
            index=index,
            coords=coords,
        )

    def get_support(self, dof: typing.Union[int, numpy.ndarray]) -> numpy.ndarray:
        if isinstance(dof, int):
            return numpy.array([dof // len(self.coeffs)])
        else:
            return numpy.unique(dof // len(self.coeffs))

    def f_dofs_coeffs(self, index: evaluable.Array) -> typing.Tuple[evaluable.Array, evaluable.Array]:
        coeffs = evaluable.asarray(self.coeffs)
        ndofs_per_elem = evaluable.asarray(len(self.coeffs))
        dofs = evaluable.Range(ndofs_per_elem) + index * ndofs_per_elem
        return dofs, coeffs


def refine_Vec(pboxVec):
    dim = pboxVec.shape[1]
    combinations = combvec([[0, 1] for _ in range(dim)])
    offset = combinations.shape[0]

    output = numpy.zeros( (pboxVec.shape[0] * combinations.shape[0],dim), dtype=int)

    for i in range(pboxVec.shape[0]):
        index = i * offset
        output[index:index+offset,:] = 2 * pboxVec[i,:] + combinations

    return output
