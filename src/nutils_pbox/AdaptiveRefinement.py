import numpy
import nutils.topology
from nutils.expression_v2 import Namespace
from nutils_pbox.topology_pbox import PboxTopology
from nutils_pbox.THB_coarse_refine_functions import refine_THB, coarsen_THB
from nutils_pbox import debug_logger
import time
import nutils_pbox.debug_logger
import copy
class AdaptiveRefinement:
    def __init__(self, topology, degree = None, MaxL = numpy.infty, admissibility_class = numpy.infty, error_type = 'max', **kwargs):
        self.topology = topology
        self.degree = degree

        self.MaxL = MaxL
        self.admissibility_class = admissibility_class
        self.preserve_iterations = False
        self.preserve_error = False
        self.refine_all = False

        self.error_type = error_type

        self.data_logger = debug_logger.data_logger()

        self.type = 'regular'

        if isinstance(topology, PboxTopology):
            self.MaxL = self.topology.MaxL
            self.admissibility_class = self.topology.m
            self.type = 'nutils_pbox'
            print(f'admss {self.admissibility_class}')
        else:
            self.degree = degree

        self.old_topology = None
        self.old_basis = None
        self.old_coeffs = None

        # check kwargs
        if "preserve_iterations" in kwargs:
            self.preserve_iterations = True
        if "max_iterations" in kwargs:
            self.max_iterations = kwargs['max_iterations']
        else:
            self.max_iterations = numpy.infty
        if "preserve_error" in kwargs:
            self.preserve_error = kwargs["preserve_error"]
        if "refine_all" in kwargs:
            self.refine_all = kwargs["refine_all"]

    def get_topology(self):
        if self.type == 'nutils_pbox':
            return self.topology.nutils_topology
        else:
            return self.topology

    def Project(self, target_func, geometry, target_eps, theta_ref = 0.1, theta_coar = 0.25, method = 'bern', extra_output = debug_logger.data_logger()):
        iter_count = len(target_func)

        extra_output.add_multiple({"geometry":geometry})

        iter_output = debug_logger.data_logger()
        projected_args = self.Project_iter_refine(target_func[0], geometry, target_eps, theta=theta_ref, method = method, extra_output=iter_output)
        extra_output.add_multiple({"iteration": 0, "topology": copy.deepcopy(self.get_topology()), "args": projected_args})


        for iter in range(1,iter_count):
            iter_output = debug_logger.data_logger()
            projected_args = self.Project_iter_refine(target_func[iter], geometry, target_eps, theta=theta_ref, method=method, extra_output=iter_output)

            error = iter_output.data['error'][-1]
            marked_element_coar = self.DorflerMarkingCoarsening(error, theta_coar)

            self.CoarsenTopology(marked_element_coar)

            extra_output.add_multiple({"iteration":iter, "topology":self.get_topology(), "args":projected_args, 'iter_output':iter_output})

        return projected_args


    def Project_iter_refine(self, target_func, geometry, target_eps: float, marking_method = 'dorfler', theta=0.5, method = 'bern', extra_output = debug_logger.data_logger()):
        # iteratively projects target func onto topology and refines the topology where ever necessary

        # first iteration
        projected_args, times = self.ProjectIteration(target_func, geometry, method = method)
        eps, global_error, inf_error, error_list, DOFS = self.ErrorIteration(geometry, projected_args, target_func) # error_list_poss contains the error of the elements of levels MaxL-1 and lower
        iter_count = 0
        error_refinement = 1e10
        was_refined = True
        refinement_ratio = 1
        print(f"Starting error {eps:.1e}.")
        eps_old = eps
        while eps >= target_eps and len(error_list) > 0 and iter_count <= self.max_iterations and was_refined and refinement_ratio > 0.5: # refine only when target has not been reached, and there are still elements to refine
            extra_output.add_multiple({'topologies':self.get_topology(), 'max_error':eps,'error' : error_list,  'global_error':global_error, 'inf_error':inf_error, 'dofs':DOFS, 'geometry': geometry})
            iter_count += 1
            # print(f"Starting iteration {iter_count: 2} to reduce error below target {target_eps:.1e}.")
            # perform refinement
            # elements_to_refine = self.DorflerMarking(error_list_poss, theta)
            # print(error_list)
            elements_to_refine = self.Marking(error_list, marking_method = marking_method, theta = theta, target = target_eps)
            # print(elements_to_refine)
            error_refinement = (numpy.sum(error_list[elements_to_refine]))
            refinement_ratio = error_refinement/numpy.sum(error_list) / theta
            refinement_ratio = 1
            was_refined = self.RefineTopology(elements_to_refine)
            # project onto new topology
            projected_args, times = self.ProjectIteration(target_func, geometry, method = method)
            eps, global_error, inf_error, error_list, DOFS = self.ErrorIteration(geometry, projected_args, target_func)
            # eps = inf_error
            print(f"Iteration {iter_count: 2} reduced error to {eps:.1e} / {target_eps:.1e}.")

        extra_output.add_multiple({'topologies': self.get_topology(), 'max_error': eps, 'error': error_list, 'global_error': global_error, 'inf_error': inf_error, 'dofs': DOFS, 'geometry': geometry})

        return self.topology, projected_args['phi']


    def ProjectIteration(self, target_func : str, geometry, method = 'bern', use_old = False):
        # projects target func onto the current mesh, based on nutils_pbox or regular mesh
        ns = Namespace()
        ns.x = geometry
        ns.func = target_func
        if self.type == 'nutils_pbox':
            ns.basis = self.topology.basis('th-spline', degree=self.degree)
            if use_old:
                output = self.topology.project(ns.func, ns.basis, geometry=geometry, method=method, coarse_basis=self.old_basis, coarse_topology=self.old_topology, coeffs=self.old_coeffs, degree=16 * max(self.degree))
            else:
                output = self.topology.project(ns.func, ns.basis, geometry=geometry, method=method, degree= 4 * max(self.topology.degree) )
            self.old_topology = self.topology
            self.old_basis = ns.basis
            self.old_coeffs = output[0]
            return {'phi': output[0]}, output[1]
        else:
            ns.basis = self.topology.basis('th-spline', degree = self.degree)
            kwargs = {'solver': 'direct'}
            t0 = time.perf_counter()
            output = self.topology.project(ns.func, ns.basis, geometry, degree= 3 * max(self.degree), **kwargs)
            return {'phi' : output }, time.perf_counter()-t0

    def ErrorIteration(self, geometry, projected_args, target_func):
        # caluclates the nutils_pbox/element-wise error
        ns = Namespace()

        if self.type == 'nutils_pbox':
            ns.x = geometry
            ns.basis = self.topology.basis()
            Integration_topology = self.topology.nutils_topology  # use the topology attribute from the nutils_pbox class
            degree = self.topology.degree
        else:
            ns.x = geometry
            ns.basis = self.topology.basis('th-spline', degree = self.degree)
            Integration_topology = self.topology
            degree = self.degree

        ns.add_field('phi', ns.basis)
        ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))
        ns.func = target_func
        ns.sum = numpy.stack([1,1])

        # element_squared_error = (Integration_topology.integrate_elementwise(' (phi - func)^2 dV ' @ ns, degree= 16 * max(degree)+1, arguments=projected_args))
        # element_squared_error = (Integration_topology.integrate_elementwise('sum_i (∇_i(phi - func))^2 dV ' @ ns, degree= 16 * max(degree)+1, arguments=projected_args))
        element_squared_error = (Integration_topology.integrate_elementwise('( phi - func )^2 dV ' @ ns, degree= 2 * max(degree), arguments=projected_args))
        element_size = Integration_topology.integrate_elementwise(' 1 dV ' @ ns, degree= 2 * max(degree))
        print(max(element_squared_error))
        # element_scaled_error = element_squared_error / element_size
        element_scaled_error = element_squared_error
        global_error = numpy.sqrt(numpy.sum(element_squared_error))

        # inf error calculations
        bezier = Integration_topology.sample('gauss', 2 * max(degree))
        inf_error = numpy.max(numpy.abs(bezier.eval('(phi - func)' @ ns, degree= 2 * max(degree), **projected_args)))

        # print(element_squared_error)

        if self.type == 'nutils_pbox':
            # element_scaled_error = numpy.sqrt(self.topology._elementwise_pboxwise(element_scaled_error**2))
            element_scaled_error = numpy.sqrt(self.topology._elementwise_pboxwise(element_scaled_error))
        # print(element_scaled_error)
        else:
            element_scaled_error = numpy.sqrt(element_scaled_error)


        return max(element_scaled_error), global_error, inf_error, element_scaled_error, len(ns.basis)

    def LimitToActivePbox(self, element_error):
        active_pbox_elem = self.topology.GetActivePboxElement()
        active_pbox_error = numpy.zeros(len(active_pbox_elem))

        for i, elem_list in enumerate(active_pbox_elem):
            # active_pbox_error[i] = numpy.sqrt(numpy.sum(numpy.square(element_error[elem_list])))
            active_pbox_error[i] = max(element_error[elem_list])

        return active_pbox_error

    def LimitToMaxL(self, element_error):
        # reduces the element/nutils_pbox error to only those elements/pboxes of levels MaxL-1 and lower
        if self.type == 'nutils_pbox':
            max_index = self.topology.pbox_offsets[min(self.MaxL-1, len(self.topology.pbox_offsets) - 1)]

            return element_error[0:max_index]

        else:
            try:
                MaxNum = self.topology._offsets[min(len(self.topology._offsets) - 1, self.MaxL)]
            except:
                MaxNum = len(element_error) # no limiting necessary

            return element_error[0:MaxNum]


    def RefineTopology(self, elements_to_refine):
        # perform refinement based on 'elements_to_refine'

        t0 = time.time()

        if self.type == "nutils_pbox":
            # check if there are pboxes to refine with respect to max level
            # splits = numpy.split(elements_to_refine, self.topology.pbox_offsets)
            splits = numpy.searchsorted(elements_to_refine, self.topology.pbox_offsets, side='left')
            # print(splits,self.topology.MaxL)
            if isinstance(self.topology.MaxL, int):
                if numpy.sum(splits[:self.topology.MaxL]) == 0:
                    was_refined = False
                else:
                    was_refined = True
            else:
                was_refined = True


            self.topology = self.topology.refined_by(elements_to_refine)

        else:
            # no hierarchical topology, perform regular refinement without taking admissibility into account
            # self.topology = self.topology.refined_by(elements_to_refine)
            if not isinstance(self.topology, nutils.topology.HierarchicalTopology):
                self.topology = self.topology.refined_by(elements_to_refine)
                was_refined = True # to be implemented for regular nutils topologies
            else:
                self.topology = refine_THB(self.topology, list(self.topology.basetopo.shape), self.degree, elements_to_refine, m=self.admissibility_class)
                was_refined = True # to be implemented for regular nutils topologies

        return was_refined

    def CoarsenTopology(self, elements_to_coarsen):
        if self.type == "nutils_pbox":
            self.topology.coarsen_by_pbox(elements_to_coarsen)
        else:
            # no hierarchical topology, perform regular refinement without taking admissibility into account
            # self.topology = self.topology.refined_by(elements_to_refine)
            if isinstance(self.topology, nutils.topology.HierarchicalTopology):
                self.topology = coarsen_THB(self.topology, list(self.topology.basetopo.shape), self.degree, elements_to_coarsen, m=self.admissibility_class)

    def Marking(self, list_to_mark, marking_method = 'dorfler',**args):
        list_to_mark = self.LimitToMaxL(list_to_mark)
        # print(args)
        if marking_method == 'dorfler':
            return self.DorflerMarking(list_to_mark, args['theta'])
        elif marking_method == 'target_error':
            return self.TargetErrorMarking(list_to_mark, args['target'])
        else:
            raise Exception(f"non-existent marking method: {marking_method}. Choose 'dorfler' or 'target_error'.")

    def TargetErrorMarking(self, list_to_mark, target):
        return numpy.where(list_to_mark>target)[0]

    def DorflerMarking(self, list_to_mark, theta):
        # given list_to_mark and theta, returns the index list
        # of fewest elements that exceeds the theta fraction of list to mark
        # list_to_mark = numpy.square(list_to_mark)

        # find index list of largest to smallest entries
        indices_of_sorted_list = numpy.flip(numpy.argsort(list_to_mark))
        list_sorted = list_to_mark[indices_of_sorted_list]

        # find first index that exceeds target
        target = theta * numpy.nansum(list_sorted)
        list_sorted_cumsum = numpy.nancumsum(list_sorted)
        target_index = numpy.argmax(list_sorted_cumsum >= target)

        # return numpy.where(list_to_mark >= 1e-4)[0].astype(int)
        return numpy.sort(indices_of_sorted_list[0:(target_index + 1)]).astype(int)

    def DorflerMarkingCoarsening(self, list_to_mark, theta):
        # given list_to_mark and theta, returns the index list
        # of fewest elements that exceeds the theta fraction of list to mark
        # list_to_mark = numpy.square(list_to_mark)

        # find index list of smallest to largets entries
        indices_of_sorted_list = (numpy.argsort(list_to_mark))
        list_sorted = list_to_mark[indices_of_sorted_list]

        # find first index that exceeds target
        target = theta * numpy.sum(list_sorted)
        list_sorted_cumsum = numpy.cumsum(list_sorted)
        target_index = numpy.argmax(list_sorted_cumsum >= target)

        return numpy.sort(indices_of_sorted_list[0:(target_index + 1)]).astype(int)