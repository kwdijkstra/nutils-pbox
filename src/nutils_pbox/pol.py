import time

import numpy as np
import nutils.expression_v2
import scipy
from nutils import mesh, export
from itertools import product
import matplotlib.pyplot as plt
from nutils_pbox import mesh_pbox


from typing import Sequence

from nutils_pbox.jitclasses import from_list_of_arrays, FastDictFloatMatrix, FastDictFloatTensor
from numba import njit, prange

_ = np.newaxis


def _nutils_basis_coeffs_to_array(basis):
    basis_elem_wise = list(map(np.asarray, basis._coeffs))
    print(len(basis._coeffs))
    splits = [len(basis_elem) for basis_elem in basis_elem_wise]
    return np.concatenate(basis_elem_wise,dtype=np.float64), np.concatenate([[0], np.cumsum(splits)])


def _nutils_coeffs_to_numpy_coeffs(coeffs: np.ndarray):
    n = int(-1/2 + np.sqrt(0.25 + 2 * coeffs.shape[1]))
    p = n - 3

    indices = np.asarray([elem for elem in product(range(n), range(n)) if sum(elem) < n], dtype=int)

    array = np.zeros((coeffs.shape[0], n, n),dtype=np.float64)
    array[:, indices[:, 0], indices[:, 1]] = coeffs[:, ::-1]

    return array[:, :p+1, :p+1]


def check_partition_of_unity(coeffs: np.ndarray, dofs: Sequence[np.ndarray]) -> np.ndarray:
    ret = []
    lengths = [mydofs.shape[0] for mydofs in dofs]
    ranges = np.array([0, *lengths]).cumsum()
    for r0, r1 in zip(ranges, ranges[1:]):
        ret.append(coeffs[r0:r1].sum(0))
    return np.asarray(ret)


@njit(cache=True)
def _eval_pol_2D(weights, positions):
    # given:
    # - weights n x p x q
    # - positions m x 2
    # where
    # n : num dofs
    # m : num positions
    # p,q polynomial degrees
    #
    # returns
    # - evaluations n x m

    x, y = positions[:, 0].copy(), positions[:, 1].copy()
    p, q = weights.shape[1], weights.shape[2]
    X = x.reshape((len(x), 1)) ** np.arange(p).reshape((1, p))  # (npos, p)
    Y = y.reshape((len(y), 1)) ** np.arange(q).reshape((1, q))  # (npos, p)

    evals = (weights[:, _] * X[_, :, :, _] * Y[_, :, _, :])

    return evals.sum(-1).sum(-1)

def _leggauss_2D(p,q, elem_size):
    # generate the legendre gauss quadrature points and weights
    # note that due to np.polynomial, no jit version is possible.
    positions_x, integral_weights_x = np.polynomial.legendre.leggauss(p)
    positions_y, integral_weights_y = np.polynomial.legendre.leggauss(q)

    positions_x = (positions_x + 1) / 2
    positions_y = (positions_y + 1) / 2

    positions = np.stack(list(map(np.ravel, np.meshgrid(*[positions_x, positions_y]))), axis=1)

    integral_weights = np.stack(list(map(np.ravel, np.meshgrid(*[integral_weights_x, integral_weights_y]))), axis=1)
    integral_weights = np.prod(integral_weights, axis=1)

    return (positions, elem_size / 4 * integral_weights)

@njit(cache=True)
def _integrate_pol_2D(weights, quad):
    quad_positions = quad[0]
    quad_weigths = quad[1]

    evals =  quad_weigths[_, :] * _eval_pol_2D(weights, quad_positions)

    return evals.sum(-1)

@njit(cache=True)
def _integrate_mass_elem(weights, quad, elem_size):
    quad_positions = quad[0]
    quad_weigths = quad[1]

    eval_weights = _eval_pol_2D(weights, quad_positions)

    evals = quad_weigths[_,_,:] * eval_weights[:,_,:] * eval_weights[_,:,:]

    return np.prod(elem_size) * evals.sum(-1)


@njit(cache=True)
def _solve_macro(macro, func, dofs, dofs_macro, weights, quad, elem_origins, elem_size):
    M = np.zeros((dofs_macro.shape[0], dofs_macro.shape[0]), dtype=np.float64)
    b = np.zeros((dofs_macro.shape[0],), dtype=np.float64)
    w = np.zeros((dofs_macro.shape[0],), dtype=np.float64)
    #
    for ielem, e in enumerate(macro):
        dofs_e = dofs[e]
        dofs_e_macro = np.searchsorted(dofs_macro, dofs_e)

        M_e = _integrate_mass_elem(weights[e], quad, elem_size)
        for i, idof_e_macro in enumerate(dofs_e_macro):
            for j, jdof_e_macro in enumerate(dofs_e_macro):
                M[idof_e_macro, jdof_e_macro] += M_e[i, j]

        origin = elem_origins[:, ielem]

        b_e = _integrate_func_elem(weights[e], func, quad, origin, elem_size)
        for i, idof_e_macro in enumerate(dofs_e_macro):
            b[idof_e_macro] += b_e[i]

        w_e = _integrate_unit_elem(weights[e], quad, elem_size)
        for i, idof_e_macro in enumerate(dofs_e_macro):
            w[idof_e_macro] += w_e[i]

    return w * np.linalg.solve(M, b), w
@njit(cache=True)
def _integrate_mass_macro(macro, dofs, weigths, quad, elem_size):
    dofs_macro = dofs.get_multiple(macro)
    M = np.zeros((dofs_macro.shape[0],dofs_macro.shape[0]),dtype=np.float64)

    for e in macro:
        dofs_e = dofs[e]
        dofs_e_macro = np.searchsorted(dofs_macro, dofs_e)

        M_e = _integrate_mass_elem(weigths[e],quad, elem_size)
        for i, idof_e_macro in enumerate(dofs_e_macro):
            for j, jdof_e_macro in enumerate(dofs_e_macro):
                M[idof_e_macro, jdof_e_macro] += M_e[i,j]

    return M

# @njit(cache=True)
def _integrate_func_macro(macro, func, dofs, weigths, quad, elem_origins, elem_size):
    dofs_macro = dofs.get_multiple(macro)
    b = np.zeros((dofs_macro.shape[0],),dtype=np.float64)

    for j, e in enumerate(macro):
        dofs_e = dofs[e]
        dofs_e_macro = np.searchsorted(dofs_macro, dofs_e)

        b_e = _integrate_func_elem(weigths[e], func, quad, elem_origins[:,j], elem_size)
        for i, idof_e_macro in enumerate(dofs_e_macro):
                b[idof_e_macro] += b_e[i]

    return b
@njit(cache = True)
def func(positions):
    x, y = positions[:, 0].copy(), positions[:, 1].copy()
    return np.sin( np.pi * x ) * np.sin( np.pi * y )

@njit(cache = True)
def unit(positions):
    return np.ones((positions.shape[0],), dtype=np.float64)

@njit(cache = True)
def _integrate_func_elem(weights, func, quad, elem_origin, elem_size):
    quad_positions = quad[0]
    quad_weigths = quad[1]

    eval_weights = _eval_pol_2D(weights, quad_positions.copy())
    eval_func = func(elem_origin + elem_size * quad_positions.copy())

    evals = quad_weigths[_, :] * eval_weights[:, :] * eval_func[_, :]

    return np.prod(elem_size) * evals.sum(-1)

@njit(cache = True)
def _integrate_unit_elem(weights, quad, elem_size):
    quad_positions = quad[0]
    quad_weigths = quad[1]

    eval_weights = _eval_pol_2D(weights, quad_positions.copy())

    evals = quad_weigths[_, :] * eval_weights[:, :]

    return np.prod(elem_size) * evals.sum(-1)

@njit(cache = True, parallel = True)
def _generate_sparse_global_projection(func, dofs, weights, domain_data, quad):
    # generate the mass matrix in COO scipy sparse format

    indices_per_level = domain_data[0]
    elem_offsets = domain_data[1]
    base_shape = domain_data[2]
    elem_size_level0 = domain_data[3]

    total_dofs = np.max(dofs.vals)+1
    # compute how many elements are necessary, per element the square of the dofs with support

    elem_dofs = np.empty((dofs.length,), dtype=np.int64)
    for e in range(dofs.length):
        elem_dofs[e] = dofs.nentries(e)

    # compute the offsets per element in the array
    elem_dofs_squared = np.array([0, *np.square(elem_dofs)]).cumsum()

    elem_dofs = np.array([0, *elem_dofs]).cumsum()


    # preallocate arrays
    data = np.empty((elem_dofs_squared[-1],),dtype=np.float64)
    col  = np.empty((elem_dofs_squared[-1],),dtype=np.int64)
    row  = np.empty((elem_dofs_squared[-1],),dtype=np.int64)

    bvec = np.empty((elem_dofs[-1],),dtype=np.float64)
    brow = np.empty((elem_dofs[-1],),dtype=np.int64)

    for l,offset_l in enumerate(elem_offsets[0:-1]):
        # loop over elements
        elem_origins = _elem_origin_2D(indices_per_level[l], (2 ** l) * base_shape)
        level_elem_size = elem_size_level0 / 2 ** l
        for e in prange(offset_l,elem_offsets[l+1]):
            data[elem_dofs_squared[e]:elem_dofs_squared[e+1]] = np.ravel(_integrate_mass_elem(weights[e], quad, level_elem_size.copy() ) )

            elem_dof = _meshgrid_ravel_2D(dofs[e], dofs[e])
            col[elem_dofs_squared[e]:elem_dofs_squared[e + 1]] = elem_dof[:,0]
            row[elem_dofs_squared[e]:elem_dofs_squared[e + 1]] = elem_dof[:,1]

            bvec[elem_dofs[e]:elem_dofs[e + 1]] = _integrate_func_elem(weights[e],func, quad, elem_origins[:,e - elem_offsets[l]], level_elem_size.copy() )
            brow[elem_dofs[e]:elem_dofs[e + 1]] = dofs[e]

    return ((data, (col, row)), (total_dofs,total_dofs)), (bvec, brow)


@njit(cache = True, parallel = True)
def _project_thb(func, macros, dofs, weights, domain_data, quad, macro_indices_per_level, macro_offsets):

    indices_per_level = domain_data[0]
    elem_offsets = domain_data[1]
    base_shape = domain_data[2]
    elem_size_level0 = domain_data[3]

    total_dofs = np.max(dofs.vals) + 1

    coeffs = np.zeros((total_dofs,), dtype=np.float64)
    normalization = np.zeros((total_dofs,), dtype=np.float64)


    for l,offset_l in enumerate(macro_offsets[0:-1]):
        # loop over macro elements
        # elem_origins = _elem_origin_2D(indices_per_level[l], (2 ** l) * base_shape)
        elem_size = elem_size_level0 / 2 ** l

        for imacro in prange(offset_l,macro_offsets[l+1]):
            elem_origins = _elem_origin_2D(indices_per_level[l][macros[imacro] - elem_offsets[l]], base_shape * 2 ** l)
            macro_dofs = dofs.get_multiple(macros[imacro])

            coeff_macro, w_macro = _solve_macro(macros[imacro], func, dofs, macro_dofs, weights, quad, elem_origins, elem_size)

            coeffs[macro_dofs] += coeff_macro
            normalization[macro_dofs] += w_macro


    return coeffs / normalization

@njit(cache=True)
def _meshgrid_ravel_2D(array1_1D, array2_1D):
    n1 = array1_1D.shape[0]
    n2 = array2_1D.shape[0]
    n = n1 * n2

    meshgrid = np.empty((n,2))

    offset = 0
    for i in range(n1):
        meshgrid[offset:offset+ n1,0] = array2_1D
        meshgrid[offset:offset+ n1,1] = array1_1D[i]

        offset += n1

    return meshgrid

@njit(cache=True)
def _elem_origin_2D(level_index, level_shape):
    # get vector index of element
    vec = np.stack(np.divmod(level_index, level_shape[1]),axis=0)
    # divide by the shape to get the lower left conrner coordinate of the element
    return vec / level_shape[:,_]



if __name__ == '__main__':
    n = 8
    h = 1 / (n - 1)
    Nloop = 25
    # domain, geom = mesh.rectilinear([np.linspace(0, 1, n+1),np.linspace(0, 1, n+1)])

    domain, geom = mesh_pbox.pbox((3,3),(2,2))

    ns = nutils.expression_v2.Namespace()
    ns.x = geom

    def process_domain(domain):
        # process domain data
        elem_size_level0 = 1 / np.asarray(domain.topology.basetopo.shape,dtype=np.float64)
        indices_per_level = list(map(lambda x : np.asarray(x, dtype=np.int64), domain.topology._indices_per_level))
        offsets = np.asarray(domain.topology._offsets,dtype=np.int64)
        base_shape = np.asarray(domain.topology.basetopo.shape, dtype=np.int64)
        return (indices_per_level, offsets, base_shape, elem_size_level0)


    domain.refined_by_pbox([0,1,2,3])
    domain.refined_by_pbox([0,1,2,3,4,5,6,7,8,12,15])

    def updated_domain(domain):
        domain_process = process_domain(domain)
        basis = domain.basis('th-spline')

        coeffs,splits = _nutils_basis_coeffs_to_array(basis)
        arr = _nutils_coeffs_to_numpy_coeffs(coeffs)
        arr_fast = FastDictFloatTensor(arr, np.asarray(splits,dtype=np.int64))
        dofs = from_list_of_arrays(basis._dofs)

        p, q = arr[:len(dofs[0])].shape[1], arr[:len(dofs[0])].shape[2]
        quad = _leggauss_2D(2 * p, 2 * q, 1)

        domain.GenerateProjectionElement()
        macros = from_list_of_arrays(domain.proj_elem)

        return domain_process, basis, arr_fast, quad, dofs, macros

    domain_process, basis, arr_fast, quad, dofs, macros = updated_domain(domain)
    #
    # domain.GenerateProjectionElement()
    #
    # macros = [np.asarray(macro,dtype=np.int64) for macro in domain.proj_elem]
    #
    #
    # # macros = from_list_of_arrays(macros)
    #
    # print(macros.length)
    # print(domain.pbox_active_indices_per_level)
    # print(domain.pbox_offsets)
    #
    # indices_per_level = domain_process[0]
    # elem_offsets = domain_process[1]
    # base_shape = domain_process[2]
    # elem_size_level0 = domain_process[3]
    #
    # elem_size = domain_process[-1]
    # elem_origins = _elem_origin_2D(indices_per_level[0][macros[0] - elem_offsets[0]], base_shape)
    # macro_dofs = dofs.get_multiple(macros[0])
    #
    # elem_origins = np.asarray(elem_origins)
    # macro = np.asarray(macros[0])
    #
    # # coeff_macro, w_macro = _solve_macro(macro, func, dofs, arr_fast, quad, elem_origins, elem_size)
    #
    # # quit()
    #

    coeffs = _project_thb(func, macros, dofs, arr_fast, domain_process, quad, domain.pbox_active_indices_per_level, domain.pbox_offsets)


    # quit()

    M_coo, b_coo = _generate_sparse_global_projection(func, dofs, arr_fast, domain_process, quad)
    #
    # b = np.zeros((M_coo[1][0],),dtype = np.float64)
    # for val, index in zip(b_coo[0],b_coo[1]):
    #     b[index] += val


    domain.refined_by_pbox([0,1,3,4,5,6,8,10,11,12,23,24,25,26,27,28,30,31,32,33,34,37,38,39,48,50,51,52,53,56,59])
    domain_process, basis, arr_fast, quad, dofs, macros = updated_domain(domain)

    print(f'Start timing over {Nloop} loops')

    t0 = time.perf_counter()
    for i in range(Nloop):
        coeffs = _project_thb(func, macros, dofs, arr_fast, domain_process, quad, domain.pbox_active_indices_per_level, domain.pbox_offsets)
    print(f'new macro THB took {time.perf_counter() - t0}')

    t0 = time.perf_counter()
    for i in range(Nloop):
        M_coo, b_coo = _generate_sparse_global_projection(func, dofs, arr_fast, domain_process, quad)

        b = np.zeros((M_coo[1][0],),dtype = np.float64)
        for val, index in zip(b_coo[0],b_coo[1]):
            b[index] += val

        massMatrix = scipy.sparse.coo_matrix(M_coo[0], shape= M_coo[1])

        proj_coeff = scipy.sparse.linalg.spsolve(massMatrix, b)
    print(f'new global took {time.perf_counter() - t0}')

    t0 = time.perf_counter()
    for i in range(Nloop):
        domain.project(f'sin( {np.pi} x_0) sin( {np.pi} x_1)' @ ns , geom, method='thb')
    print(f'Old THB took {time.perf_counter() - t0}')


    # # print(b)

    ns.add_field('u', basis)
    bezier = domain.topology.sample('bezier', 9)
    args = {'u':coeffs}
    xsmp, usmp = bezier.eval(['x_i', 'u'] @ ns, **args)
    fsmp = func(xsmp)
    # print(xsmp.shape)
    fig, axs = plt.subplots(1,2)
    export.triplot(axs[0], xsmp, usmp, tri=bezier.tri, hull=bezier.hull)
    export.triplot(axs[1], xsmp, usmp - fsmp, tri=bezier.tri, hull=bezier.hull)
    plt.show()
