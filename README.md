# nutils-pbox-refinement-projecton
This is a suplementary package for nutils that implements a different type of mesh, called a pbox mesh.
This package is not an official nutils package.
Nevertheless, the main idea is to develop a different type of mesh that is advantageous for THB-splines.
This mesh consists bigger blocks of mesh elements, called p-boxes, over which the THB-splines are linearly independent.
This mesh can be seen as the equivalent local-linear-independence of THB-splines.

As of now, we are in early development and a lot can still change.

In Examples, some general scripts can be found.
In FigureScripts, the scripts that have been used to generate data for Converence talks and papers is given.

Known issues:
- when using python 3.9.8, the QR pseudo inverse does not work. Instead, the MoorePenrose pseudo inverse should be used. For this, use the option "solve_method='MoorePenrose'" in the pbox.project function.

