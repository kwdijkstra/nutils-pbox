import os

import numpy
import nutils.export
from matplotlib import pyplot as plt
from nutils_pbox import mesh_pbox
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from nutils_pbox.debug_logger import data_logger
import pickle
from mpltools import annotation

def main(   degree_list = [2,3],
            ndim = 3,
            MaxL_list = [5,4],
            r_list = [8,5],
            admissibility_class_list = [2,3,4],
            theta_list = [0.5],
            eps_target = 1e-3):

    '''
    This script generates adaptive refinement rates that can be compared to literature.
    For this, there are two loops,
    one over spline degree and associated maximum refinement level and starting p-box list, these are given by the lists:
    'degree_list, MaxL_list, r_list', these must have the same number of entries
    The last loop is over admissibility_class_list.
    '''

    generate_data = True # when generate_data is off, the code assumes that this data has already been generated before and can be loaded.
    plotting = True

    func = '1 - tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.3 ) / ( 0.05 sqrt(2) )  )'

    path = f"{os.getcwd()}\\OutputFiles\\"  # location where to store output (data and figures)
    try:
        os.mkdir(path)
    except FileExistsError:
        print(f"Folder already exists.")

    if generate_data:
        GenerateData(degree_list, ndim, admissibility_class_list, theta_list, r_list, MaxL_list, eps_target, func, path)

    if plotting:
        PlotData(degree_list, ndim, admissibility_class_list, theta_list, r_list, MaxL_list, eps_target, func, path)
        PlotMesh(degree_list, ndim, admissibility_class_list, theta_list, r_list, MaxL_list, eps_target, func, path)





def PlotData(degree_list, ndim, admissibility_class_list, theta_list, r_list, MaxL_list, eps_target, func, path):
    ## load data to compare to other methods
    # data directly taken form (Giust et al., 2020), table 1
    GiustDataPerDegree = {
        2: {"dofs": [324, 1012, 2076, 4648, 7248], "error": [1.684e-1, 2.213e-2, 2.016e-3, 1.688e-4, 9.103e-5]},
        3: {"dofs": [361, 1225, 2593, 4753], "error": [1.343e-1, 1.491e-2, 5.780e-4, 2.845e-5]}
    }

    # data from authors of (Dijkstra & Toshniwal, 2023), Figures 1.6a and 1.6b
    DijkstraData = {
        2: {
            0.5: {"dofs": [324, 436, 871, 1840, 3546, 6274, 9188],
                  "error": [1.7515e-1, 2.0372e-02, 7.9150e-03, 2.1910e-03, 8.0195e-04, 1.9559e-04, 8.1775e-05]},
            0.75: {"dofs": [324, 468, 805, 2326, 6739, 11855],
                   "error": [1.7515e-01, 3.9846e-02, 4.9439e-03, 1.4530e-03, 1.8710e-04, 5.0314e-05]},
            1: {"dofs": [324, 1156, 4356, 16900, 66564],
                "error": [1.6756e-01, 2.0680e-02, 1.8388e-03, 1.8345e-04, 3.8720e-05]}
        },
        3: {
            0.5: {"dofs": [361, 475, 706, 1765, 2854, 3907],
                  "error": [1.5137e-01, 7.1257e-02, 1.4657e-02, 5.3977e-04, 4.1875e-04, 6.9497e-05]},
            0.75: {"dofs": [361, 481, 1297, 3433], "error": [1.5137e-01, 7.1451e-02, 1.5547e-03, 9.9257e-05]},
            1: {"dofs": [361, 1225, 4489, 17161], "error": [1.5137e-01, 1.4749e-02, 4.5432e-04, 2.4922e-05]}
        }
    }
    theta = 0.5 # for any other theta, we have no comparable data from literature
    slope_dofs = [500, 5000]

    for i, degree in enumerate(degree_list):
        fig, axs = plt.subplots(1, 1)

        for admissibility_class in admissibility_class_list:
            name = NameGen(degree, admissibility_class, eps_target, theta)
            f = open(f"{path}{name}.pickle", 'rb')
            data = pickle.load(f)

            # print(data.data)
            dofs = data.data['dofs']
            inf_error = data.data['inf_error']

            axs.loglog(numpy.sqrt(dofs), inf_error)

        if ndim == 2:
            axs.loglog(numpy.sqrt(DijkstraData[degree][theta]["dofs"]), DijkstraData[degree][theta]["error"], '--o')
            axs.loglog(numpy.sqrt(GiustDataPerDegree[degree]["dofs"]), GiustDataPerDegree[degree]["error"], '--o')

        plot_error = numpy.sqrt([1e-1] * 2)
        plot_error[1] = plot_error[0] * (slope_dofs[0] / slope_dofs[1]) ** ((degree + 1) )

        if degree == 2:
            annotation.slope_marker((numpy.sqrt(1e3), 1e-1), (-3, 1), ax=axs, size_frac=0.5)
        else:
            annotation.slope_marker((numpy.sqrt(1e3), 1e-1), (-4, 1), ax=axs, size_frac=0.5)

        legend_data = [f"$class: {admissibility_class}$" for admissibility_class in admissibility_class_list]
        legend_data.append("$Dijkstra^1, class: 2$")
        legend_data.append("$Giust^2, class: 2$")

        axs.legend(legend_data, loc='lower left')
        axs.set_title(f"spline degree : {degree}")
        axs.set_xlabel(f"$dofs^\\frac{{1}}{{2}}$")
        axs.set_ylabel(f"error (L inf norm)")
        axs.grid(False, which="both")
        axs.set_xlim([1e1, 2e2])
        axs.set_ylim([2e-5, 4e-1])

        plt.savefig(f"{path}AdaptiveRefinement_degree_{degree}_dim{ndim}.pdf", bbox_inches='tight')
        plt.close(fig)

def PlotMesh(degree_list, ndim, admissibility_class_list, theta_list, r_list, MaxL_list, eps_target, func, path):
    ## load data to compare to other methods
    # data directly taken form (Giust et al., 2020), table 1



    theta = 0.5 # for any other theta, we have no comparable data from literature
    slope_dofs = [500, 5000]

    for i, degree in enumerate(degree_list):
        fig, axs = plt.subplots(1, 1)

        for admissibility_class in admissibility_class_list:
            name = NameGen(degree, admissibility_class, eps_target, theta)
            f = open(f"{path}{name}.pickle", 'rb')
            data = pickle.load(f)

            print(data.data.keys())

            # _, geom = nutils.mesh.rectilinear([numpy.linspace(0,1,4),]*2)
            ns = nutils.expression_v2.Namespace()
            ns.x = 2*data.data['geometry'][-1] - 1

            bezier = data.data['topologies'][-1].sample('bezier',2)
            x, zero = bezier.eval(['x_i', '0'] @ ns )

            fig, axs = plt.subplots(1,1)
            nutils.export.triplot(axs,x,zero,tri=bezier.tri, hull=bezier.hull,cmap = 'binary')

            plt.savefig(f"{path}AdaptiveRefinement_degree_{degree}_{admissibility_class}_dim{ndim}_mesh.pdf", bbox_inches='tight')
            plt.close(fig)



def NameGen(degree, admissibility_class, eps_target, theta):
    if not isinstance(degree, int): degree = max(degree)
    return f"data_degree_{degree}_admis_{admissibility_class}_error_target_{eps_target:.1e}_dorfler_theta_{theta}_altergeom"


def RunProblem(degree, r_elems, admissibility_class, eps_target, theta, MaxL, func, path):
    pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL=MaxL)

    output_data = data_logger()
    _ = AdaptiveRefinement(pbox).Project_iter_refine(target_func = func, geometry=geometry, target_eps = eps_target, theta = theta, extra_output=output_data, method='bern_numba')

    name = NameGen(degree, admissibility_class, eps_target, theta)
    pickle.dump(output_data, open(f"{path}{name}.pickle", "wb"))
    return

def GenerateData(degree_list,ndim,admissibility_class_list, theta_list,r_list,MaxL_list, eps_target, func, path):
    for i, degree in enumerate(degree_list):
        for admissibility_class in admissibility_class_list:
            for theta in theta_list:
                print(f"running degree : {degree}, admissibility class : {admissibility_class}, theta : {theta}")
                RunProblem((degree, )*ndim, (r_list[i],)*ndim, admissibility_class, eps_target, theta,MaxL_list[i], func, path)


if __name__ == "__main__":
    main()