import os
import numpy
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export
from nutils_pbox import mesh_pbox
from src.nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from src.nutils_pbox.debug_logger import data_logger

def main(r_elems = (6,6),
         degree = (2,2),
         theta_ref = 0.25,
         theta_coar = 0.05,
         admissibility_class = 2,
         eps_target = 3e-4,
         MaxL = 4,
         steps = 6):

    # generate the target functions
    max_offset = 0.2 # maximum offset from center in both x and y
    offset_list = numpy.linspace(-max_offset, max_offset, steps)
    offset_list = [f" - {abs(offset)} " if offset < 0 else f" + {offset} " for offset in offset_list]

    # generate the actual functions as strings
    func_list = [
        f'1 - tanh( ( sqrt( ( 2 ( x_0 {offset} ) - 1)^2 + ( 2 ( x_1 {offset} ) - 1 )^2  ) - 0.25 ) / ( 0.05 sqrt(2) )  )'
        for offset in offset_list]

    fig, axs = plt.subplots(2, 3)
    output = data_logger() # required to get the intermediate topologies
    pbox, geom = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL=MaxL)

    _ = AdaptiveRefinement(pbox).Project(func_list,geometry=geom, target_eps=eps_target, theta_ref=theta_ref, theta_coar=theta_coar,extra_output=output)

    path = f"{os.getcwd()}\\OutputFiles\\"

    for i in range(steps):
        topology = output.data['topology'][i]
        plot(axs[i // 3, i % 3], f"step {i + 1}", topology, geom, topology.basis('th-spline', degree=degree),"", degree, {})

    plt.savefig(f"{path}refinement_coarsening_six_steps.png", dpi=600)

def plot(axs, NAME:str, topology, geometry, basis, func, degree, args):
    ns = Namespace()
    ns.x = geometry
    ns.PI = numpy.pi
    ns.basis = basis
    # ns.add_field('phi', ns.basis)
    # ns.fun = func
    bezier = topology.sample('bezier', 4 * max(degree))
    x, zeroFunc = bezier.eval(['x_i', '0'] @ ns, **args)
    export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
    axs.set_title(f'{NAME}')
    axs.axis('off')

if __name__ == "__main__":
    main()


