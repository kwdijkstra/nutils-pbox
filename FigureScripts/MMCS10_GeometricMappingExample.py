import numpy
from nutils_pbox import mesh_pbox
from nutils.expression_v2 import Namespace
from VisualizationFunctions.Visualization2D import Plot
from matplotlib import pyplot as plt
from matplotlib.colors import LightSource

from nutils import export
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement


def main(pbox_count = (5,2),
         degree = (2,2)):

    pbox, geometry = mesh_pbox.pbox(degree, pbox_count, admissibility_class=2)

    # perform geometric mapping
    theta = (1-geometry[0]) * numpy.pi / 2

    r_min = 0.5
    r_max = 1

    r = (r_max - r_min) * geometry[1] + r_min

    geometry = numpy.stack([r*numpy.cos(theta), r*numpy.sin(theta)])

    # perform projection
    ns = Namespace()
    ns.x = geometry
    ns.PI = numpy.pi
    ns.basis = pbox.basis('th-spline', degree = degree)

    ns.fun = '0.5 - 0.5 tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.3 ) / ( 0.025 sqrt(2) )  )'

    args = {}
    pbox, args['phi'] = AdaptiveRefinement(pbox).Project_iter_refine(ns.fun, geometry,1e-4, theta = 0.5)
    print(args)
    # plot solution and final mesh
    ns.add_field('phi', pbox.basis())

    bezier = pbox.sample('bezier', 4 * max(degree))
    x, approx = bezier.eval(['x_i','phi'] @ ns, **args)
    fig = plt.figure(figsize = (12,6))
    axs0 = fig.add_subplot(121, projection='3d')
    axs1 = fig.add_subplot(122)

    axs0.plot_trisurf(*numpy.column_stack((x,approx)).T, triangles=bezier.tri, rasterized=True, antialiased=False, cmap = 'viridis')
    export.triplot(axs1, x, 0*approx, tri=bezier.tri, hull=bezier.hull, cmap = 'binary')

    axs0.set_title(f"Projection")
    axs0.axis("off")
    axs0.view_init(elev=30, azim=-120)

    axs1.set_title(f"Mesh")
    axs1.axis("off")
    plt.savefig(f"OutputFiles/geometric_map_projection.png", dpi=600)


if __name__ == "__main__":
    main()
