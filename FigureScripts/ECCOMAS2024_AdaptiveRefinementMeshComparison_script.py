import os
import numpy
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export, mesh
from nutils_pbox import mesh_pbox
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from nutils_pbox.debug_logger import data_logger


# this script generates the global error vs DOFS convergence plots for different admissibility classes
# it requires the data from precomputed projections.
#
# TODO: clean up the code for those projections and insert those in this script or some other standalone script


def main(r_elems = (8, 8),
         degree = (2, 2),
         theta = 0.5,
         admissibility_class = 2,
         eps_target = 1e-4,
         MaxL = 5):

    '''
    This script generates three meshes via adaptive refinement.
    Namely,
    - nutils_pbox mesh with local projection
    - nutils_pbox mesh with global projection
    - element mesh with global projection
    '''

    path = f"{os.getcwd()}\\OutputFiles\\"

    func = '1 - tanh( ( sqrt( ( 2 x_0 - 1)^2 + ( 2 x_1 - 1 )^2  ) - 0.3 ) / ( 0.05 sqrt(2) )  )'

    ## local nutils_pbox
    pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL=MaxL)
    args = {}
    final_topo, args['phi'] = AdaptiveRefinement(pbox).Project_iter_refine(func, geometry, eps_target, theta=theta)

    plot('pbox_local', path, final_topo, geometry, final_topo.basis(), func, degree, args)

    ## global nutils_pbox
    pbox, geometry = mesh_pbox.pbox(degree, r_elems, admissibility_class=admissibility_class, MaxL=MaxL)
    args = {}
    final_topo, args['phi'] = AdaptiveRefinement(pbox).Project_iter_refine(func, geometry, eps_target, theta=theta, method='global')

    plot('pbox_global', path, final_topo, geometry, final_topo.basis(), func, degree, args)

    ## global mesh
    extra_output = data_logger()
    topology, geometry = mesh.rectilinear([numpy.linspace(0, 1, r * degree[i] + 1) for i, r in enumerate(r_elems)])
    args = {}
    final_topo, args['phi'] = AdaptiveRefinement(topology, degree=degree, MaxL=MaxL,admissibility_class=admissibility_class).Project_iter_refine(func,geometry,eps_target,theta=theta,extra_output=extra_output)
    # args_class = {'phi': AdaptiveRefinement(topology, degree=degree, MaxL=MaxL,admissibility_class=admissibility_class).Project_iter_refine(func,geometry,eps_target,theta=theta,extra_output=extra_output)}
    topology = extra_output.data["topologies"][-1]

    plot('element_global', path, topology, geometry, topology.basis('th-spline', degree=degree), func, degree, args)


def plot(name:str, path, topology, geometry, basis, func, degree, args):
    fig, axs = plt.subplots(1, 1, figsize=(5, 5))
    fig.tight_layout(pad=0)
    axs.axis('off')
    ns = Namespace()
    ns.x = geometry
    ns.PI = numpy.pi
    ns.basis = basis
    ns.add_field('phi', ns.basis)
    ns.fun = func
    bezier = topology.sample('bezier', 4 * max(degree))
    x, error, zeroFunc = bezier.eval(['x_i', 'phi - fun', '0'] @ ns, **args)
    export.triplot(axs, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
    plt.margins(0)
    plt.savefig(f"{path}{name}_new.png", dpi=600)
    plt.close(fig)

if __name__ == "__main__":
    main()