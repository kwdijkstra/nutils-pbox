import time
import pickle
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors
import numpy as np
from nutils import solver, function, export
from nutils.expression_v2 import Namespace
from nutils_pbox import mesh_pbox, AdaptiveRefinement
import numpy
import matplotlib.pyplot as plt
import itertools

# implementation of [1] where the topology objects have been replaced with THB-splines.
# In addition, the a posteriori error estimator used is criterion 1, a residual based error estimator.
# we have extended this method to be timedependent, by adding the time derivative to the weak form and residual estimators.
#
# [1] B. Bastl and K. Slabá, “Adaptive refinement in incompressible fluid flow simulation based on THB-splines-powered isogeometric analysis,” Mathematics and Computers in Simulation, vol. 228, pp. 514–533, Feb. 2025, doi: 10.1016/j.matcom.2024.09.016.

def estimate(args, ns, topo, degree = 2):
    ns.Rm_i = f' ( u_i - uold_i ) / dt - ν ∇_j(∇_j(u_i)) + u_j ∇_j(u_i) + ∇_i(p) '
    ns.Rc   = '∇_i(u_i)'

    elementwise_volume = np.sqrt(topo.integrate_elementwise('dV' @ ns, degree=0))

    elem_error   = elementwise_volume * (np.sqrt(topo.integrate_elementwise(' ( Rm_i Rm_i ) dV ' @ ns, degree = 4 * (degree + 1), arguments = args))
                 + np.sqrt(topo.integrate_elementwise(' ( abs(Rc) ) dV ' @ ns, degree=2 * (degree + 1), arguments = args)))

    args['error'] = np.log10(elem_error)

    # map element error to pbox error
    pbox_error = np.sqrt(topo._elementwise_pboxwise(np.square(elem_error)))

    if topo.L == topo.MaxL:
        pbox_error[topo.pbox_offsets[topo.MaxL-1]:] = 0
    return args, pbox_error

def refine(topo, pbox_error, dorfler_theta = 0.25):
    refine_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarking(numpy.square(pbox_error), dorfler_theta)

    return topo.refined_by_pbox(refine_pbox)

def coarsen(topo, pbox_error, dorfler_theta = 0.02):
    coarse_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarkingCoarsening(numpy.square(pbox_error), dorfler_theta)
    return topo.coarsen_by_pbox(coarse_pbox)

# def adaptive_refine(ns, topo, args):
#     # initial setup
#     ns_ref, problem = setup(ns, topo)
#
#     # set initial guess for uPrev as previous solution of u
#     argsUprev0 = topo.project('uold_0' @ ns, onto = ns_ref.ubasisComp, arguments = args, geometry= geom, method='thb')[0]
#     argsUprev1 = topo.project('uold_1' @ ns, onto = ns_ref.ubasisComp, arguments = args, geometry= geom, method='thb')[0]
#     args = {'uPrev': np.hstack([argsUprev0,argsUprev1]), 'uold': args['uold']}
#
#     args = solve(topo, args, problem)
#     args, error = estimate(args, ns_ref, topo)
#     iter = 1
#
#     while max(error) >= error_target_ref:
#         print(f'Refinement iter {iter:3d}, reduced error to {max(error):.1e} / {error_target_ref:.1e}')
#         topo_new = refine(topo, error)
#
#         # set initial guess for uPrev as previous solution of u
#         # perform projection onto new topology before ns is updated
#         argsUprev0 = topo_new.project('u_0' @ ns, onto=topo_new.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', coarse_topology=topo)[0]
#         argsUprev1 = topo_new.project('u_1' @ ns, onto=topo_new.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', coarse_topology=topo)[0]
#
#         ns_ref, problem = setup(ns_ref, topo_new)
#
#         args = {'uPrev': np.hstack([argsUprev0, argsUprev1]), 'uold': args['uold']}
#
#         args = solve(topo_new, args, problem)
#         args, error = estimate(args, ns_ref, topo_new)
#
#         iter += 1
#         topo = topo_new
#
#
#     print(f'Refinement iter {iter:3d}, reduced error to {max(error):.1e} / {error_target_ref:.1e}')
#     return ns_ref, topo, args

# def solve_time(ns, topo, N, uinitial = None):
#
#     # setup initial condition
#     if uinitial is None:
#         ns.uoldbasis = function.vectorize([topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
#         args = {'uold': np.zeros(len(ns.uoldbasis))}
#     else:
#         raise f"uinitial must be none, not implemented"
#
#     ns, topo, args = adaptive_refine(ns, topo, args)
#
#     plot(f'Images/TH_image{0}.png', ns, topo, args)
#
#     for t in range(N):
#         args, pbox_error = estimate(args, ns, topo)
#
#         print(f'start coarsening!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
#         coarse_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarkingCoarsening(numpy.square(pbox_error), theta_coarsening)
#
#         topo_coarse = topo.coarsen_by_pbox(coarse_pbox)
#
#         argsuold0 = topo_coarse.project('u_0' @ ns, onto=topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
#         argsuold1 = topo_coarse.project('u_1' @ ns, onto=topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
#
#         ns.uoldbasis = function.vectorize([topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
#         args = {'uold': np.hstack([argsuold0, argsuold1])}
#
#         ns, topo, args = adaptive_refine(ns, topo_coarse, args)
#         plot(f'Images/TH_image{t+1}.png', ns, topo, args)
#
#     return ns, topo, args

# def solve_ss(ns, topo, N, uinitial = None):
#     # setup initial condition
#     if uinitial is None:
#         ns.uoldbasis = function.vectorize([topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity)), ] * 2)
#         args = {'uold': np.zeros(len(ns.uoldbasis))}
#     else:
#         raise f"uinitial must be none, not implemented"
#
#     ns, topo, args = adaptive_refine(ns, topo, args)
#
#     plot(f'Images/TH_image{0}.png', ns, topo, args)
#
#
#     return ns, topo, args
#




def setup(ns, topo, degree, regularity, dim):
    # setup basis
    ns.ubasisScalar = topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity))
    ns.ubasis = function.vectorize([ns.ubasisScalar, ] * dim)
    ns.pbasis = topo.basis('th-spline', degree=(degree, degree), continuity=(regularity, regularity))[1:]

    ns.α = 0.5 # crank nickelson parameter

    # set boundary conditions
    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top'), 0])
    ns.uwall = numpy.stack([topo.topology.boundary.indicator('top') * (1 - topo.topology.boundary.indicator('left')) * (1 - topo.topology.boundary.indicator('right')), 0])

    # setup fields
    ns.add_field(['u', 'U', 'uold', 'v'], ns.ubasis)  # create extra field U for picard iterations
    ns.add_field(['p', 'pold', 'q'], ns.pbasis)

    ns.ucn = ns.α * ( ns.u + ns.uold )
    ns.Ucn = ns.α * ( ns.U + ns.uold )
    ns.pcn = ns.α * ( ns.p + ns.pold )

    cons_res = topo.boundary.integral(' ( sum_i ( u_i - uwall_i )^2 ) dS ' @ ns, degree=2 * (degree + 1))
    cons_res += topo.boundary.integral(' ( sum_i ( v_i )^2 ) dS ' @ ns, degree=2 * (degree + 1))
    cons = solver.optimize('u,v,', cons_res, droptol=1e-15)

    # use crank nickelson
    usolve = 'ucn'
    Usolve = 'Ucn'
    psolve = 'pcn'

    res = topo.integral(f' ν ( ∇_i({usolve}_j) ∇_i(v_j) ) dV ' @ ns, degree=2 * (degree + 1))
    res += topo.integral(f' ( {Usolve}_i ∇_i({usolve}_j) v_j ) dV ' @ ns, degree=3 * (degree + 1))
    res -= topo.integral(f' ( {psolve} ∇_i(v_i) ) dV ' @ ns, degree=2 * (degree + 1))
    res += topo.integral(f' ( q ∇_i(u_i) ) dV ' @ ns, degree=2 * (degree + 1))

    res += topo.integral(f' ( ( u_i - uold_i ) v_i / dt ) dV ' @ ns, degree=2 * (degree + 1))

    return ns, (cons, res)

def solve_picard(topo, ns, args, problem, error_target = 1e-10, degree = 2):
    cons, res = problem
    if 'U' not in args.keys():
        args['U'] = np.zeros(len(ns.ubasis))
    def picard_error_estimate(): # calculate H1 error between picard iterates
        elementwise_volume = np.sqrt(topo.integrate_elementwise('dV' @ ns, degree=0))
        uerror = elementwise_volume * topo.integrate_elementwise(' ( ∇_i( u_j - U_j ) ∇_i( u_j - U_j ) ) dV ' @ ns, degree=2 * degree, arguments=args)
        return np.sqrt(np.sum(uerror))

    for picard_iter in itertools.count(): # start picard iterate loop
        args = solver.solve_linear('u:v,p:q', res, arguments=args, constrain=cons)
        uerror = picard_error_estimate()

        print(f'Picard iter: {picard_iter:3d}, picard step size: {uerror:.1e} / {error_target:.1e}')

        if uerror < error_target: # break if error within tol
            break

        args['U'] = args['u']

    return args

def adaptive_refine(topo, geom, ns, args = {}, error_target = 1e-10, degree = 2, regularity = 1, dim = 2, dorfler_theta = 0.25):
    if 'uold' not in args.keys():
        ns, problem = setup(ns, topo, degree, regularity, dim)
        args['uold'] = np.zeros(len(ns.ubasis))
    if 'pold' not in args.keys():
        ns, problem = setup(ns, topo, degree, regularity, dim)
        args['pold'] = np.zeros(len(ns.pbasis))
    def project(topo, ns, topo_refined, ns_refined):
        newbasis = topo_refined.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity))
        # set initial guess for U as previous solution of u
        argsU0 = topo_refined.project('u_0' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        argsU1 = topo_refined.project('u_1' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        # project uold onto new basis
        argsuold0 = topo_refined.project('uold_0' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        argsuold1 = topo_refined.project('uold_1' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        # project pold onto new basis
        newpbasis = topo_refined.basis('th-spline', degree=(degree, degree), continuity=(regularity, regularity))
        argsp = topo_refined.project('pold' @ ns, onto=newpbasis, arguments=args, geometry=geom, method='thb')[0]
        return { 'U' : np.hstack([argsU0, argsU1]), 'u' : np.hstack([argsU0, argsU1]), 'uold' : np.hstack([argsuold0,argsuold1]), 'pold' : argsp[1:] }

    for refinement_iter in itertools.count():
        # solve problem and estimate error
        ns, problem = setup(ns, topo, degree, regularity, dim)
        args = solve_picard(topo, ns, args, problem, error_target, degree=degree)
        args, error = estimate(args, ns, topo, degree=degree)

        print(f'Refinement iter {refinement_iter:3d}, reduced error to {max(error):.1e} / {error_target:.1e}')

        if max(error) < error_target: # break if hit error tolerance
            break

        # refine, project and update solution
        topo_ref = refine(topo, error, dorfler_theta = dorfler_theta)
        args = project(topo, ns, topo_ref, None)
        topo = topo_ref

    return ns, topo, args

def solve_timesteps(topo, geom, ns, args = {}, error_target = 1e-10, degree = 2, regularity = 1, dim = 2, dorfler_refine_theta = 0.25, dorfler_coarse_theta = 0.02, N_time = 1):
    def project(topo, ns, topo_coarse, ns_refined):
        # project u onto new space, and set initial guesses for u,U
        newubasis = topo_coarse.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity))
        argsU0 = topo_coarse.project('u_0' @ ns, onto=newubasis, arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
        argsU1 = topo_coarse.project('u_1' @ ns, onto=newubasis, arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
        newpbasis = topo_coarse.basis('th-spline', degree=(degree, degree), continuity=(regularity, regularity))
        argsp = topo_coarse.project('p' @ ns, onto=newpbasis, arguments=args, geometry=geom, method='thb', refined_topology=topo)[0]
        return { 'U' : np.hstack([argsU0, argsU1]), 'u' : np.hstack([argsU0, argsU1]), 'uold' : np.hstack([argsU0, argsU1]), 'pold' : argsp[1:] }

    for time_step in range(N_time):
        ns, topo, args = adaptive_refine(topo, geom, ns, args=args, error_target=error_target, degree=degree, regularity=regularity, dim=dim, dorfler_theta=dorfler_refine_theta)
        args, error = estimate(args, ns, topo, degree=degree)

        print(f'Solved time step {time_step+1:3d} / {N_time:d}, with error {max(error):.1e}.')

        plot(time_step+1, ns, topo, args, degree)

        if time_step + 1 == N_time: break

        # coarsen and project
        topo_coa = coarsen(topo, error, dorfler_theta=dorfler_coarse_theta)
        args = project(topo, ns, topo_coa, None)
        topo = topo_coa

    return ns, topo, args


def plot(iteration, ns, topo, args, degree):
    ns.add_field('error',topo.basis('discont', degree=(0,0)))

    bezier = topo.sample('bezier', 2*degree + 1)

    x, u, error = bezier.eval(['x_i', 'sqrt(u_i u_i)', 'error'] @ ns, **args)
    fig, ax = plt.subplots(1, 2)
    fig.set_size_inches(9,4)

    colormapList1 = np.array([[255,247,236],
                    [254,232,200],
                    [253,212,158],
                    [253,187,132],
                    [252,141,89],
                    [239,101,72],
                    [215,48,31],
                    [179,0,0],
                    [127,0,0]])/256

    colormapList2 = np.flipud(np.array([[215,48,39],
                                        [252,141,89],
                                        [254,224,144],
                                        [224,243,248],
                                        [145,191,219],
                                        [69,117,180]]))/256


    cm1 = LinearSegmentedColormap.from_list('my_cmap', colormapList1, N=256)
    imVel = export.triplot(ax[0], x, u, tri=bezier.tri, hull=bezier.hull, cmap=cm1, clim=(0, 1))

    cm2 = LinearSegmentedColormap.from_list('my_cmap', colormapList2, N=256)
    imError = export.triplot(ax[1], x, error, tri=bezier.tri, cmap = cm2, hull=bezier.hull,clim=(min(error),max(error)))

    fig.colorbar(imVel, label='velocity')
    ax[0].set_title('velocity magnitude')
    ax[1].set_title('elementwise error')
    cbarError =fig.colorbar(imError, label='error',ticks=np.arange(-15,15))
    cbarError.set_ticklabels([f'10e{tick}' for tick in cbarError.get_ticks()]) # set ticks to log scale

    # fig.colorbar(cset1, ax=axs[0])

    plt.savefig(f'TaylorHood_time_new_{iteration}.png',dpi=300)
    # plt.show()
    plt.close(fig)

    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(4, 4)

    mesh = export.triplot(ax, x, 0*u, tri=bezier.tri, hull=bezier.hull, cmap='binary', clim=(0, 1))
    plt.savefig(f'TaylorHood_mesh_new_{iteration}.png', dpi=300)
    plt.close(fig)



def main(   degree: int = 2,
            regularity: int = 1,
            pbox_count: int = 3,
            max_L: int = 4,
            admissibility: int = 2,
            error_target: float = 1e-3,
            dorfler_refine_theta: float = 0.5,
            dorfler_coarse_theta: float = 0.05,
            reynolds: float = 1e3,
            final_t: float = 10.0,
            dt: float = 0.25):

    dim = 2
    assert degree > regularity

    topo, geom = mesh_pbox.pboxNew((degree + 1,) * dim, (pbox_count,) * dim, MaxL=max_L, admissibility_class=admissibility)

    ns = Namespace()
    ns.sum = function.ones([topo.ndims])
    ns.x = geom
    ns.ν = 1/reynolds
    ns.dt = dt
    ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))

    # ns, problem = setup(ns,topo, degree)
    # ns, topo, args = adaptive_refine(topo, geom, ns, error_target=error_target, degree = degree, regularity=regularity, dim = dim,dorfler_theta = dorfler_theta)
    ns, topo, args = solve_timesteps(topo, geom, ns, error_target=error_target, degree = degree, regularity=regularity, dim = dim,dorfler_refine_theta=dorfler_refine_theta, dorfler_coarse_theta=dorfler_coarse_theta,N_time=int(final_t/dt))

main()