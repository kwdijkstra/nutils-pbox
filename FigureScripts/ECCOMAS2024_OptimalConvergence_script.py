import os
import sys

## bad code, but this is necessary to import other files in other directories
# use the following line on delftblue, since the scripts are called from the main folder with a batch file
path = os.getcwd()
sys.path.insert(1, path)

import numpy
from nutils_pbox import mesh_pbox
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
import pickle
import nutils_pbox.tools_nutils as tools_nutils
from mpltools import annotation

def main(   num_dim: int = 2,
            degree_list: list[int] = [1,2,3,4,4,4,4],
            continuity_list: list[int] = [0,1,2,3,2,1,0],
            r_list_start: int = 2,
            target_element_size: float = 5e-2,
            method: str = 'bern_numba'):

    '''
    Generates convergence rate and timing plots based on inputs:

    num_dim : set number of dimensions
    degree_list : set list of degrees to use
    r_list_start : start number of p-boxes per dimensions
    target_element_size : decrease the element size till this target is hit.
    method : method to use, choose 'bern', 'thb' or 'global' for either local or global projection

    The main code does three things,
    1. setup the associated data and subcases
    2. for each subcase, run Run_Case function
        In this function the actual projection happens.
    3. plotting

    This code likewise is used to run performance tests.
    However, as of 31-5-2024, for best performance, the latest version of nutils should be directly installed from github.
    This can be done as follows:
    python3 -m pip install --user https://github.com/evalf/nutils/archive/master.zip
    The performance tests were done with nutils version 9a29
    '''

    # setup target function
    func = ''.join([f' sin( 3.14 x_{i} ) ' for i in list(range(num_dim))])

    path = f"{os.getcwd()}\\OutputFiles\\" # location where to store output (data and figures)
    try:
        os.mkdir(path)
    except FileExistsError:
        print(f"Folder already exists.")
    name = f"OptimalConvergence_{method}_dim{num_dim}_delftblue_large"

    # choose what to generate
    generate_data = True # when generate_data is off, the code assumes that this data has already been generated before and can be loaded.
    plotting = True

    # generate the cases required
    cases_list = list(zip(degree_list, continuity_list))
    results = {f"{degree},{cont}" : {} for degree, cont in cases_list}
    for i, (degree, cont) in enumerate(cases_list):
        target_pbox_size = target_element_size * degree
        upper_bound_pbox = numpy.ceil(1 / target_pbox_size)

        r_list_i = 2 * (numpy.arange(numpy.floor(r_list_start / 2), numpy.ceil(upper_bound_pbox / 2) + 1, dtype=int))

        results[f"{degree},{cont}"]["r_list"] = r_list_i
        results[f"{degree},{cont}"]["error"] = numpy.zeros(len(r_list_i))
        results[f"{degree},{cont}"]["time"] = numpy.zeros(len(r_list_i))

    if generate_data:
        # loop over degrees and nutils_pbox counts to perform projection
        for i, (degree, cont) in enumerate(cases_list):
            for j, r_count in enumerate(results[f"{degree},{cont}"]['r_list']):
                # run case
                print(f"solving degree : {degree}, continuity : {cont} and pbox count : {r_count} with element size / target size {1 / (r_count * degree):.1e}/{target_element_size:.1e}")
                results[f"{degree},{cont}"]["error"][j], results[f"{degree},{cont}"]["time"][j] = Run_Case(func=func, degree=(degree,) * num_dim, cont=(cont,) * num_dim, r_elems=(r_count,) * num_dim, method=method)

        pickle.dump(results, open(f"{path}data_{name}.pickle", "wb"))

    if plotting:
        # the remainder plots the associated data
        fig, axs = plt.subplots(1, 1)
        # results = pickle.load(open(f"{path}data_{name}_dim3.pickle", 'rb'))
        results = pickle.load(open(f"{path}data_{name}.pickle", 'rb'))
        degree_list = results.keys()

        # plot data
        for i, (degree, cont) in enumerate(cases_list):
            elem_list = 1 / (results[f"{degree},{cont}"]['r_list'] * degree)
            results[f"{degree},{cont}"]['error'] = numpy.sqrt(results[f"{degree},{cont}"]['error'])
            axs.loglog(elem_list, results[f"{degree},{cont}"]["error"])

        legend_data = [f"degree : {degree}, continuity : {cont}" for degree, cont in cases_list]

        # plot slope triangles
        y_loc = [2e-3, 8e-5, 6e-6, 3e-7, 3e-7, 3e-7, 3e-7, 3e-7, 3e-7] # values fitted for method = 'bern'
        for i, (degree, cont) in enumerate(cases_list):
            # if degree == 4:
            #     break
            print(degree)
            annotation.slope_marker((8e-2, y_loc[i]), (degree+1, 1), ax=axs, size_frac=0.15, invert=True)

        axs.legend(legend_data)
        axs.set_title(f"Convergence Rate")
        axs.set_xlabel(f"element size")
        axs.set_ylabel(f"error (L2 norm)")
        axs.grid(False, which="both")

        plt.savefig(f"{path}{name}.png", dpi=600)
        plt.close(fig)

        fig, axs = plt.subplots(1, 1)
        plt.gca().set_prop_cycle(None)
        for i, (degree, cont) in enumerate(cases_list):
            elem_list = (results[f"{degree},{cont}"]['r_list'] * degree) ** 2
            axs.loglog(elem_list, results[f"{degree},{cont}"]['time'])

        legend_data = [f"degree : {degree}, continuity : {cont}" for degree, cont in cases_list]

        dofs_slope = [1e3, 4e3]
        first_y_value = 1e-2
        if method == 'bern':
            annotation.slope_marker((1e3, 2e-1), (1, 1), ax=axs, size_frac=0.15)
        else:
            annotation.slope_marker((2e3, 2e-1), (3, 1), ax=axs, size_frac=0.15)

        axs.legend(legend_data)
        axs.set_title(f"Time")
        axs.set_xlabel(f"dofs")
        axs.set_ylabel(f"time [seconds]")
        axs.grid(False, which="both")

        axs.set_xlim([1e2, 3e4])
        axs.set_ylim([1e-4, 1e3])

        plt.savefig(f"{path}{name}_timing.png", dpi=600)
        plt.close(fig)


def Run_Case(func, degree, cont, r_elems, method = 'bern_numba'):
    '''
    Runs the specified case to project the target func onto the THB-spline space
    'func' str : target function
    'degree' tuple[int] : spline degree (tuple for each dimension)
    'r_elems' tuple[int] : number of p-boxes in each dimension
    'method' str : method to use, choose from 'bern', 'thb' or 'global'.
    '''

    if any([r_elem % 2 == 1 for r_elem in r_elems]):
        raise f"For lower quadrant refinement, choose each entry of 'r_elems'={r_elems} to be even."

    pbox, geometry = mesh_pbox.pbox(degree, r_elems)

    # get lower quadrant nutils_pbox indices:
    refinement_domain_vector = tools_nutils.combvec([numpy.arange(0, r_elem / 2, dtype=int) for r_elem in r_elems])
    refinement_domain_index = tools_nutils.map_vec_index(refinement_domain_vector, r_elems)
    pbox.refined_by(refinement_domain_index)

    ns = Namespace()
    ns.x = geometry
    ns.fun = func

    # project func onto nutils_pbox mesh
    ns.basis = pbox.basis('th-spline', degree=degree, continuity = cont)
    kwargs = {'solver': 'direct'}
    proj_coeffs, timing = pbox.project(ns.fun, ns.basis, geometry, method=method, **kwargs)
    args = {'phi' : proj_coeffs}

    # calculate L^2 error via standard nutils integration
    ns.define_for('x', jacobians=('dV', 'dS'))
    ns.add_field('phi', ns.basis)
    error = pbox.integrate('(phi - fun)^2 dV' @ ns, degree = (max(degree)+1)*4, arguments = args)

    return error, timing[0]

if __name__ == '__main__':
    main()

