import os
import math
import numpy
import numpy as np
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export, mesh

import nutils_pbox.debug_logger
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from nutils_pbox import mesh_pbox

def main(degree : list[int] = (2,2),
         pbox_count : list[int] = (3,3),
         theta : float = 0.75,
         admissibility_class : int = 2,
         error_target : float = 5e-3,
         Max_L : int = 5,
         angle_deg : float = 22.5 ):

    angle = angle_deg * np.pi / 180

    rotated_distance = f' {np.cos(angle)} x_0 - {np.sin(angle)} x_1'
    func = f'arctan( 20 ( {rotated_distance} ) )'
    func = f' abs( {rotated_distance} )'
    func = f' sign( {rotated_distance} )'

    fig, axs = plt.subplots(2,1)

    pbox, geometry = mesh_pbox.pbox(degree, pbox_count, admissibility_class=admissibility_class, MaxL = Max_L)
    print(pbox)
    geometry = 2 * geometry - 1
    pbox, coeffs = AdaptiveRefinement(pbox).Project_iter_refine(func, geometry, error_target, theta=theta, method='bern', marking_method='dorfler')
    args = {'phi' : coeffs }

    ns = Namespace()
    ns.x = geometry
    ns.basis = pbox.basis()
    ns.add_field('phi', ns.basis)
    ns.fun = func
    bezier = pbox.nutils_topology.sample('bezier', 4 * max(degree))
    x, error, zeroFunc = bezier.eval(['x_i', 'phi - fun', '0'] @ ns, **args)
    export.triplot(axs[0], x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
    export.triplot(axs[1], x, error, tri=bezier.tri, hull=bezier.hull, cmap='binary')

    plt.savefig(f'LineRefinement_angle_{angle_deg}.png',dpi=600)

for angle in [0,10,22.5,np.pi * 10, 45]:
    main(angle_deg= angle)
