import os
import math
import numpy
import numpy as np
from nutils.expression_v2 import Namespace
from matplotlib import pyplot as plt
from nutils import export, mesh, function

import nutils_pbox.debug_logger
from nutils_pbox.AdaptiveRefinement import AdaptiveRefinement
from nutils_pbox import mesh_pbox



topo, geom = mesh.rectilinear([numpy.linspace(0,1,5),]*2)
geom = 2 * geom - 1
degree = (2,2)
problemType = '_mesh_'

ns = Namespace()
ns.x = geom
ns.rho = numpy.arctan2( ns.x[0], ns.x[1] )
ns.r = 'sqrt( x_0^2 + x_1^2 )'
angle = 31.415 * np.pi / 180
ns.dist =  f' {np.cos(angle)} x_0 - {np.sin(angle)} x_1'

# ns.func = ' abs(dist)^(2 / 3)'
ns.func = ' r^(2 / 3)'

args = {}

adapt = AdaptiveRefinement(topo, degree=degree)
output_mesh = nutils_pbox.debug_logger.data_logger()
topo, args['phi'] = adapt.Project_iter_refine(ns.func, geom, 1e-5, theta = 0.9, method='bern', extra_output=output_mesh)
#
# fig,axs = plt.subplots(1,1)
# axs.loglog(numpy.sqrt(output.data['dofs']),output.data['global_error'])
#
# plt.xlabel(r'$dofs^{1/2}$')
# plt.ylabel(r'global $L^2$ error')
# plt.grid()
# plt.savefig(f'Images/DofsAdmissibilityLessRegular{problemType}.png', dpi=2400)
#
# plt.close(fig)
#
# figPLT, axPLT = plt.subplots(1, 1)
# ns.basis = topo.basis('th-spline', degree= degree)
# ns.add_field('phi', ns.basis)
# bezier = topo.sample('bezier', 4 * max(degree))
# x, error, zeroFunc = bezier.eval(['x_i', 'phi - func', '0'] @ ns, **args)
# # export.triplot(ax[0], x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
# export.triplot(axPLT, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
# # plt.show()
#
# plt.savefig(f'Images/DofsAdmissibilityLessRegular{problemType}Mesh.png', dpi=2400)
#
# quit()



def main(degree : list[int] = (2, 2),
         pbox_count : list[int] = (2, 2),
         theta : float = 0.9,
         admissibility_class : int = 2,
         error_target : float = 1e-5,
         Max_L : int = 9,
         plotting : bool = False):

    # func = ' 1 / ( sqrt( x_0^2 + x_1^2 ) ) '

    # func = ' ( sqrt( x_0^2 + x_1^2 )^( 2 / 3 ) ) sin( 2 arctan2( x_0  x_1 ) / 3)'



    pbox, geometry = mesh_pbox.pbox(degree, pbox_count, admissibility_class=admissibility_class, MaxL = Max_L)
    # geometry = 2 * geometry - 1  + np.array([1/3,1/3])
    geometry = 2 * geometry - 1

    ns = Namespace()
    ns.x = geometry
    ns.rho = numpy.arctan2( ns.x[0], ns.x[1] )
    ns.r = 'sqrt( x_0^2 + x_1^2 )'
    angle = 31.415 * np.pi / 180
    ns.dist =  f' {np.cos(angle)} x_0 - {np.sin(angle)} x_1'

    # ns.func = ' abs(dist)^(2 / 3)'
    ns.func = ' r^(2 / 3)'

    args = {}
    adapt = AdaptiveRefinement(pbox)
    output = nutils_pbox.debug_logger.data_logger()
    pbox, args['phi'] = adapt.Project_iter_refine(ns.func, geometry, error_target, theta = theta, method='bern', extra_output=output)

    if plotting:
        figPLT, axPLT = plt.subplots(1, 1)
        ns.basis = pbox.basis()
        ns.add_field('phi', ns.basis)
        bezier = pbox.sample('bezier', 4 * max(degree))
        x, error, zeroFunc = bezier.eval(['x_i', 'phi - func', '0'] @ ns, **args)
        # export.triplot(ax[0], x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap='binary')
        export.triplot(axPLT, x, zeroFunc, tri=bezier.tri, hull=bezier.hull, cmap = 'binary')
        # plt.show()

        plt.savefig(f'Images/DofsAdmissibilityLessRegular{problemType}Mesh.png', dpi=2400)
        plt.close(figPLT)


    axs.loglog(numpy.sqrt(output.data['dofs']),output.data['global_error'])
    # plt.grid()


problemType = ''

fig, axs = plt.subplots(1,1)
max_admis = 6
for admissibility_class in range(2,max_admis):
    main(admissibility_class=admissibility_class, plotting = admissibility_class + 1 == max_admis)

axs.loglog(numpy.sqrt(output_mesh.data['dofs']),output_mesh.data['global_error'])

plt.figure(fig)
legend_list = list([f'admissibility class {i}' for i in range(2,max_admis)])
legend_list.append(r'admissibility class $\infty$ (mesh elements)')
plt.legend(legend_list)
plt.xlabel(r'$dofs^{1/2}$')
plt.ylabel(r'global $L^2$ error')
plt.grid()
# plt.show()
plt.savefig(f'Images/DofsAdmissibilityLessRegular{problemType}.png',dpi = 2400)
plt.close(fig)