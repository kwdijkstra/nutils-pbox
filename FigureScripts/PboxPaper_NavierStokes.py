import time
import pickle
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors
import numpy as np
from nutils import solver, function, export
from nutils.expression_v2 import Namespace
from nutils_pbox import mesh_pbox, AdaptiveRefinement
import numpy
import matplotlib.pyplot as plt
import itertools

# implementation of [1] where the topology objects have been replaced with THB-splines.
# In addition, the a posteriori error estimator used is criterion 1, a residual based error estimator.
#
# [1] B. Bastl and K. Slabá, “Adaptive refinement in incompressible fluid flow simulation based on THB-splines-powered isogeometric analysis,” Mathematics and Computers in Simulation, vol. 228, pp. 514–533, Feb. 2025, doi: 10.1016/j.matcom.2024.09.016.

def estimate(args, ns, topo, degree = 2):
    ns.Rm_i = f' - ν ∇_j(∇_j(u_i)) + u_j ∇_j(u_i) + ∇_i(p) '
    ns.Rc   = '∇_i(u_i)'

    elementwise_volume = np.sqrt(topo.integrate_elementwise('dV' @ ns, degree=0))

    elem_error   = elementwise_volume * (np.sqrt(topo.integrate_elementwise(' ( Rm_i Rm_i ) dV ' @ ns, degree = 4 * (degree + 1), arguments = args))
                 + np.sqrt(topo.integrate_elementwise(' ( abs(Rc) ) dV ' @ ns, degree=2 * (degree + 1), arguments = args)))

    args['error'] = np.log10(elem_error)

    # map element error to pbox error
    pbox_error = np.sqrt(topo._elementwise_pboxwise(np.square(elem_error)))

    if topo.L == topo.MaxL:
        pbox_error[topo.pbox_offsets[topo.MaxL-1]:] = 0
    return args, pbox_error

def refine(topo, pbox_error, dorfler_theta = 0.25):
    refine_pbox = AdaptiveRefinement.AdaptiveRefinement(topo).DorflerMarking(numpy.square(pbox_error), dorfler_theta)

    return topo.refined_by_pbox(refine_pbox)

def setup(ns, topo, degree, regularity, dim):
    # setup basis
    ns.ubasisScalar = topo.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity))
    ns.ubasis = function.vectorize([ns.ubasisScalar, ] * dim)
    ns.pbasis = topo.basis('th-spline', degree=(degree, degree), continuity=(regularity, regularity))[1:]

    # set boundary conditions
    ns.uwall = numpy.stack([topo.boundary.indicator('top'), 0])
    ns.uwall = numpy.stack([topo.boundary.indicator('top') * (1 - topo.boundary.indicator('left')) * (1 - topo.boundary.indicator('right')), 0])

    # setup fields
    ns.add_field(['u', 'U', 'v'], ns.ubasis)  # create extra field U for picard iterations
    ns.add_field(['p', 'q'], ns.pbasis)

    cons_res = topo.boundary.integral(' ( sum_i ( u_i - uwall_i )^2 ) dS ' @ ns, degree=2 * (degree + 1))
    cons_res += topo.boundary.integral(' ( sum_i ( v_i )^2 ) dS ' @ ns, degree=2 * (degree + 1))
    cons = solver.optimize('u,v,', cons_res, droptol=1e-15)

    res = topo.integral(' ν ( ∇_i(u_j) ∇_i(v_j) ) dV ' @ ns, degree=2 * (degree + 1))
    res += topo.integral(' ( U_i ∇_i(u_j) v_j ) dV ' @ ns, degree=3 * (degree + 1))
    res -= topo.integral(' ( p ∇_i(v_i) ) dV ' @ ns, degree=2 * (degree + 1))
    res += topo.integral(' ( q ∇_i(u_i) ) dV ' @ ns, degree=2 * (degree + 1))

    return ns, (cons, res)

def solve_picard(topo, ns, args, problem, error_target = 1e-10, degree = 2):
    cons, res = problem
    if 'U' not in args.keys():
        args['U'] = np.zeros(len(ns.ubasis))
    def picard_error_estimate(): # calculate H1 error between picard iterates
        elementwise_volume = np.sqrt(topo.integrate_elementwise('dV' @ ns, degree=0))
        uerror = elementwise_volume * topo.integrate_elementwise(' ( ∇_i( u_j - U_j ) ∇_i( u_j - U_j ) ) dV ' @ ns, degree=2 * degree, arguments=args)
        return np.sqrt(np.sum(uerror))

    for picard_iter in itertools.count(): # start picard iterate loop
        args = solver.solve_linear('u:v,p:q', res, arguments=args, constrain=cons)
        uerror = picard_error_estimate()

        print(f'Picard iter: {picard_iter:3d}, picard step size: {uerror:.1e} / {error_target:.1e}')

        if uerror < error_target: # break if error within tol
            break

        args['U'] = args['u']

    return args

def adaptive_refine(topo, geom, ns, args = {}, error_target = 1e-10, degree = 2, regularity = 1, dim = 2, dorfler_theta = 0.25):

    def project(topo, ns, topo_refined, ns_refined):
        # set initial guess for U as previous solution of u
        newbasis = topo_refined.basis('th-spline', degree=(degree + 1, degree + 1), continuity=(regularity, regularity))
        argsU0 = topo_refined.project('u_0' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        argsU1 = topo_refined.project('u_1' @ ns, onto=newbasis, arguments=args, geometry=geom, method='thb')[0]
        return { 'U' : np.hstack([argsU0, argsU1]), 'u' : np.hstack([argsU0, argsU1])}

    for refinement_iter in itertools.count():
        # solve problem and estimate error
        ns, problem = setup(ns, topo, degree, regularity, dim)
        args = solve_picard(topo, ns, args, problem, error_target, degree=degree)
        args, error = estimate(args, ns, topo, degree=degree)

        print(f'Refinement iter {refinement_iter:3d}, reduced error to {max(error):.1e} / {error_target:.1e}')
        plot(f'_RefinementIter_{refinement_iter}', ns, topo, args, degree =degree, show=False)

        if max(error) < error_target: # break if hit error tolerance
            break

        # project and update solution
        topo_ref = refine(topo, error, dorfler_theta = dorfler_theta)
        args = project(topo, ns, topo_ref, None)
        topo = topo_ref

    return ns, topo, args

def plot(name = '', ns = None, topo = None, args = None, degree : int = None, show=False):
    ns.add_field('error',topo.basis('discont', degree=(0,0)))

    bezier = topo.sample('bezier', 2*degree + 1)

    x, u, error = bezier.eval(['x_i', 'sqrt(u_i u_i)', 'error'] @ ns, **args)

    colormapList1 = np.array([[255,247,236],
                    [254,232,200],
                    [253,212,158],
                    [253,187,132],
                    [252,141,89],
                    [239,101,72],
                    [215,48,31],
                    [179,0,0],
                    [127,0,0]])/256

    colormapList2 = np.flipud(np.array([[103,0,31],
                    [178,24,43],
                    [214,96,77],
                    [244,165,130],
                    [253,219,199],
                    [247,247,247],
                    [209,229,240],
                    [146,197,222],
                    [67,147,195],
                    [33,102,172],
                    [5,48,97]]))/256
    colormapList2 = np.flipud(np.array([[215,48,39],
                                        [252,141,89],
                                        [254,224,144],
                                        [224,243,248],
                                        [145,191,219],
                                        [69,117,180]]))/256

    fig1, axs1 = plt.subplots(1,1)

    cm1 = LinearSegmentedColormap.from_list('my_cmap', colormapList1, N=256)
    imVel = export.triplot(axs1, x, u, tri=bezier.tri, hull=bezier.hull, cmap=cm1, clim=(0, 1))

    fig2, axs2 = plt.subplots(1, 1)

    cm2 = LinearSegmentedColormap.from_list('my_cmap', colormapList2, N=256)
    imError = export.triplot(axs2, x, error, tri=bezier.tri, cmap = cm2, hull=bezier.hull,clim=(min(error),max(error)))

    fig1.colorbar(imVel, label='velocity')
    axs1.set_title('velocity magnitude')
    axs2.set_title('elementwise error')
    cbarError =fig2.colorbar(imError, label='error',ticks=np.arange(-15,15))
    cbarError.set_ticklabels([f'10e{tick}' for tick in cbarError.get_ticks()]) # set ticks to log scale

    # fig.colorbar(cset1, ax=axs[0])

    if show:
        plt.show()

    plt.savefig(f'TaylorHood{name}_2.png', dpi=300)
    plt.close(fig2)
    plt.savefig(f'TaylorHood{name}_1.png', dpi=300)
    plt.close(fig1)

def main(   degree: int = 2,
            regularity: int = 1,
            pbox_count: int = 3,
            max_L: int = 5,
            admissibility: int = 2,
            error_target: float = 2e-4,
            dorfler_theta: float = 0.75,
            reynolds: float = 1e3):

    dim = 2
    assert degree > regularity

    topo, geom = mesh_pbox.pbox((degree + 1,) * dim, (pbox_count,) * dim, MaxL=max_L, admissibility_class=admissibility)
    topo = topo.refined_by_pbox(range(len(topo)))

    ns = Namespace()
    ns.sum = function.ones([topo.ndims])
    ns.x = geom
    ns.ν = 1/reynolds
    ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))

    # ns, problem = setup(ns,topo, degree)
    ns, topo, args = adaptive_refine(topo, geom, ns, error_target=error_target, degree = degree, regularity=regularity, dim = dim,dorfler_theta = dorfler_theta)
    plot('_Final',ns,topo,args, degree, show=True)

main()