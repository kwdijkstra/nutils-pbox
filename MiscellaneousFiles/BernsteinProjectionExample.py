import nutils
from nutils import function, sparse, evaluable, types
from nutils.expression_v2 import Namespace
import numpy
from matplotlib import pyplot as plt
from src.nutils_pbox.topology_pbox import Pbox
from src.nutils_pbox import evaluable_pboxOLD
from macroelemwise_projection import project_onto_basis_macroelemwise

'''This file was used for development. It is no longer maintained. This will be deleted at some point.'''

def GetElementBezierExtractionMatrix(topo, geom, bern_basis, THB_basis, arguments = {}):
    # Create a sample on `topo` with gauss quadrature points for exact
    # integration of functions of degree `degree`.
    # smpl = topo.sample('gauss', degree)
    nelems = bern_basis.nelems

    # We use an evaluable loop to evaluate the projection for each element.
    # `ielem` is the element number, used a.o. to obtain the local basis
    # coefficients and dofs and the quadrature weights. `lower_args` is a
    # representation of the local elemement with index `ielem`, used to lower
    # `function.Array` to `evaluable.Array`.
    ielem = evaluable.loop_index('elems', nelems)
    # print(ielem)
    # ipelem = evaluable.loop_index('elems', 6)
    # lower_args = smpl.get_lower_args(ielem)

    # Define the approximate element integral `elem_int` using the quadrature
    # scheme from `smpl`, scaled with the geometry `geom`.
    # weights = smpl.get_evaluable_weights(ielem) * function.jacobian(geom, topo.ndims).lower(lower_args)
    # elem_int = lambda integrand: evaluable.einsum('A,AB->B', weights, integrand)

    # Obtain the local dofs and coefficients from `basis` at element `ielem`.
    THB_dofs, THB_basis_coeffs = THB_basis.f_dofs_coeffs(ielem)
    Bern_dofs, Bern_basis_coeffs = bern_basis.f_dofs_coeffs(ielem)

    # print(f"dofs : {numpy.asarray(THB_dofs.eval(elems = 0))}")
    # print(f"dofs : {numpy.asarray(THB_basis._coeffs[0])}")

    elem_index = numpy.linspace(1, THB_basis.nelems,THB_basis.nelems)
    elem_index = [[elem]*int(elem) for elem in elem_index ]
    # print(elem_index)
    elem_index_array = tuple([types.arraydata(elem) for elem in elem_index])
    # print((elem_index_array))

    eval_elem_index_array = evaluable.Elemwise(data = elem_index_array,index = ielem,dtype = float)

    # print(eval_elem_index_array.eval(elems = 1))

    # elem_index_array = evaluable.Array(elem_index_array,shape = tuple(42), dtype=float)


    # for i in range(42): print(THB_basis_coeffs.eval(elems=i).shape)

    # inverse BernCoef
    def CalcC_BP_inv(BernBasis):
        C_BP = numpy.array(BernBasis.get_coefficients(0))

        active_col = [any(C_BP[:, i] != 0) for i in range(C_BP.shape[1])]
        active_row = [i for i in range(C_BP.shape[0])]
        # if C_BP_inv is None:
        C_BP_inv = numpy.zeros(C_BP.shape).transpose()
        C_BP_inv[numpy.ix_(active_col, active_row)] = numpy.linalg.inv(C_BP[:, active_col])

        return C_BP_inv

    C = evaluable.constant(CalcC_BP_inv(bern_basis))
    # print(f"C:{C.eval(elems = 0).shape}")

    bezier_extraction = evaluable.einsum('ji,ik->kj', THB_basis_coeffs, C)
    # bezier_extraction = evaluable.einsum('Ai,Bj->ABij', eval_elem_index_array, eval_elem_index_array)
    # local_bezier_extraction = evaluable.einsum('ji,jk->ik', C, C)
    # local_bezier_extraction = evaluable.einsum('ij,kj->ik', THB_basis_coeffs, THB_basis_coeffs)

    # Sample the local basis in the local coordinates. The first axes of
    # `shapes` correspond to `weights`, the last axis has length `basis.ndofs`
    # shapes = evaluable.Polyval(basis_coeffs, topo.f_coords.lower(lower_args))

    # Compute the local mass matrix and right hand side.
    # mass = elem_int(evaluable.einsum('Ai,Aj->Aij', shapes, shapes))
    # rhs = elem_int(evaluable.einsum('Ai,AB->AiB', shapes, fun.lower(lower_args)))

    # Solve the local least squares problem.
    # local_proj_coeffs = evaluable.einsum('ij,jB->Bi', evaluable.inverse(mass), rhs)

    # Scatter the local projection coefficients to the global coefficients and
    # do this for every element in the topology.
    # proj_coeffs = evaluable.Inflate(local_proj_coeffs, dofs, evaluable.asarray(basis.ndofs))
    # proj_coeffs = evaluable.loop_sum(proj_coeffs, ielem)
    # proj_coeffs = evaluable.Transpose.from_end(proj_coeffs, 0)

    # print(evaluable.asarray([THB_basis.ndofs,bern_basis.ndofs]))
    # print(evaluable.transpose(THB_dofs))
    # print(THB_dofs)
    # print(THB_dofs.func)
    # print(THB_dofs.length)

    # bezier_extraction = evaluable.sum(bezier_extraction,axis=0)
    bezier_extraction = evaluable.Inflate(bezier_extraction, THB_dofs, evaluable.asarray(THB_basis.ndofs))
    bezier_extraction = evaluable.transpose(bezier_extraction, trans=[1, 0])
    bezier_extraction = evaluable.Inflate(bezier_extraction, Bern_dofs, evaluable.asarray(bern_basis.ndofs))
    bezier_extraction = evaluable.transpose(bezier_extraction, trans=[1, 0])
    # bezier_extraction = evaluable.Inflate(local_bezier_extraction, [THB_dofs, Bern_dofs, evaluable.asarray(( THB_basis.ndofs, bern_basis.ndofs) ))
    # bezier_extraction = evaluable.Transpose.from_end(bezier_extraction,0)
    # bezier_extraction = evaluable.einsum('ij->ji', bezier_extraction)
    bezier_extraction = evaluable.appendaxes(bezier_extraction, tuple([evaluable.constant(1)]))
    print(bezier_extraction.shape)
    # bezier_extraction = evaluable.Inflate(local_bezier_extraction, Bern_dofs, evaluable.asarray(bern_basis.ndofs))
    # bezier_extraction = evaluable.loop_sum(bezier_extraction, ielem)

    # print(evaluable.asarray(bezier_extraction).shape)
    bezier_extraction = evaluable.loop_concatenate(bezier_extraction, ielem)
    bezier_extraction = evaluable.transpose(bezier_extraction, trans=[2, 0, 1])



    # print(bezier_extraction.as_evaluable_array )


    # Evaluate.
    # return sparse.toarray(evaluable.eval_sparse((bezier_extraction,), **arguments)[0])
    return evaluable.eval_sparse((bezier_extraction,), **arguments)[0]

# setup domain and pick degrees (note that they can be different)
N_elems = 2
degree = numpy.array([3,2])
# topology, geometry = mesh.rectilinear([np.linspace(0,1,N_elems + 1),np.linspace(0,1,N_elems + 1)])
# topology = topology.refined_by([0])

r_elems = (N_elems,N_elems)
# degree = (2,2)

pbox = Pbox(degree, r_elems)
pbox.refined_by_pbox([0])




ns = Namespace()
ns.x = pbox.geometry
ns.Pi = numpy.pi

# ns.basis = arb_basis_discontinuous(nutils_pbox.topology, degree)
ns.BERNbasis = pbox.basis('discont', degree=degree)
ns.THBbasis = pbox.basis('th-spline', degree=degree)
print(f"nelems          : {ns.BERNbasis.nelems}")
print(f"Bern ndofs      : {ns.BERNbasis.ndofs}")
print(f"Bern ndofs elem : {ns.BERNbasis.ndofs/ns.BERNbasis.nelems}")
print(f"THB ndofs       : {ns.THBbasis.ndofs}")
ns.fun = 'sin( 2 Pi x_0 ) cos( 2 Pi x_1 )'

# x = local_projection.project_onto_discontinuous_basis(nutils_pbox.topology, nutils_pbox.geometry, ns.BERNbasis, ns.fun, max(degree) * 4 + 1)


thb_basis = ns.THBbasis
pbox.GenerateProjectionElement()

bezier_extraction_elem_matrices = GetElementBezierExtractionMatrix(pbox.topology, pbox.geometry, ns.BERNbasis, ns.THBbasis)

elem_proj_map = numpy.zeros(thb_basis.nelems,dtype = int)
for proj, elems in enumerate(pbox.proj_elem):
    for elem in elems:
        elem_proj_map[elem] = proj

proj_elem_elemmap_inv = [numpy.zeros(thb_basis.nelems,dtype=int) for elems in pbox.proj_elem]
for n, elems in enumerate(pbox.proj_elem):
    for local_index, elem in enumerate(elems):
        proj_elem_elemmap_inv[n][elem] = local_index

proj_proj_dofmaps = [thb_basis.get_dofs(elems) for elems in pbox.proj_elem]
proj_elem_dofmaps = [proj_proj_dofmaps[elem_proj_map[elem]] for elem in range(thb_basis.nelems)]

proj_proj_coefs = [numpy.zeros((numpy.prod(degree + 1) * len(pbox.proj_elem[n]),len(dofs))) for n, dofs in enumerate(proj_proj_dofmaps)]
proj_elem_coefs = [numpy.zeros((numpy.prod(degree + 1),len(proj_proj_dofmaps[elem_proj_map[elem]]))) for elem in range(thb_basis.nelems)]

# print(proj_proj_dofmaps)
inv_dof_map = [ _ for _ in range(pbox.proj_count)]

for n in range(pbox.proj_count):
    inv_dof_map[n] = numpy.zeros(len(thb_basis), dtype = int)
    for loc_index, global_index in enumerate(proj_proj_dofmaps[n]):
        inv_dof_map[n][global_index] = loc_index

index, value, shape = sparse.extract(bezier_extraction_elem_matrices)

weight = numpy.zeros(thb_basis.ndofs, dtype=float)

for i in range(len(value)):
    elem = index[0][i]
    proj = elem_proj_map[elem]
    loc_bern_index = index[1][i] % numpy.prod(degree + 1)
    loc_THB_index = inv_dof_map[proj][index[2][i]]
    local_elem_index = proj_elem_elemmap_inv[proj][elem] * numpy.prod(degree + 1)
    proj_proj_coefs[proj][loc_bern_index + local_elem_index, loc_THB_index] = value[i]
    proj_elem_coefs[elem][loc_bern_index, loc_THB_index] = value[i]

    weight[index[2][i]] += value[i] * pbox.elem_size(elem)

proj_weight = [proj_proj_coefs[proj].sum(axis=0) * pbox.elem_size(pbox.proj_elem[proj][0]) for proj in range(pbox.proj_count)]

print(proj_proj_coefs[0].shape)

# elem_index = numpy.linspace(1, THB_basis.nelems,THB_basis.nelems)
# elem_index = [[elem]*int(elem) for elem in elem_index ]
# print(elem_index)
elem_coef_array = tuple([types.arraydata(proj_elem_coefs[elem]) for elem in range(thb_basis.nelems)])
proj_coef_array = tuple([types.arraydata(proj_proj_coefs[proj]) for proj in range(pbox.proj_count)])
proj_weight_array = tuple([types.arraydata(proj_weight[proj]) for proj in range(pbox.proj_count)])
proj_elem_dofmaps_array = tuple([types.arraydata(proj_elem_dofmaps[elem]) for elem in range(thb_basis.nelems)])
# print((elem_index_array))





# next code taken from local_projection.project_onto_discontinuous_basis
smpl = pbox.topology.sample('gauss', 2*degree+1)
ielem = evaluable_pbox.loop_index('elems', smpl.nelems)

C_elem = evaluable_pbox.Elemwise(data = elem_coef_array, index = ielem, dtype = float)


lower_args = smpl.get_lower_args(ielem)
weights = smpl.get_evaluable_weights(ielem) * function.jacobian(pbox.geometry, pbox.topology.ndims).lower(lower_args)
elem_int = lambda integrand: evaluable.einsum('A,AB->B', weights, integrand)

dofs, basis_coeffs = ns.BERNbasis.f_dofs_coeffs(ielem)
thb_dofs, thb_basis_coeffs = ns.THBbasis.f_dofs_coeffs(ielem)

dofmap_thb = evaluable_pbox.Elemwise(data = proj_elem_dofmaps_array, index = ielem, dtype = int)

shapes = evaluable_pbox.Polyval(basis_coeffs, pbox.topology.f_coords.lower(lower_args))
mass = elem_int(evaluable_pbox.einsum('Ai,Aj->Aij', shapes, shapes))
rhs = elem_int(evaluable_pbox.einsum('Ai,AB->AiB', shapes, ns.fun.lower(lower_args)))
local_proj_coeffs = evaluable_pbox.einsum('ij,jB->Bi', evaluable_pbox.inverse(mass), rhs)
local_proj_coeffs = evaluable_pbox.einsum('ji,jB->Bi', C_elem, local_proj_coeffs)
# proj_coeffs = evaluable.Inflate(local_proj_coeffs, dofs, evaluable.asarray(ns.BERNbasis.ndofs))
proj_coeffs = evaluable_pbox.Inflate(local_proj_coeffs, dofmap_thb, evaluable_pbox.asarray(ns.THBbasis.ndofs))
proj_coeffs = evaluable_pbox.appendaxes(proj_coeffs, tuple([evaluable_pbox.constant(1)]))
proj_coeffs = evaluable_pbox.loop_concatenate(proj_coeffs, ielem)
proj_coeffs = evaluable_pbox.Transpose.from_end(proj_coeffs, 0)


bern_coeffs = sparse.toarray(evaluable_pbox.eval_sparse((proj_coeffs,))[0])

print(bern_coeffs.shape)
# print(bern_coeffs[0,:])
# print(proj_elem_dofmaps[0])

bern_proj_coeffs = [numpy.zeros(thb_basis.ndofs,dtype = float) for n in range(pbox.proj_count)]

for n in range(pbox.proj_count):
    elems = pbox.proj_elem[n]
    for elem in elems:
        bern_proj_coeffs[n] += bern_coeffs[elem,:]

    bern_proj_coeffs[n] = bern_proj_coeffs[n][proj_proj_dofmaps[n]]


bern_proj_coeffs_array = tuple([types.arraydata(bern_proj_coeffs[n]) for n in range(pbox.proj_count)])
proj_proj_dofmaps_array = tuple([types.arraydata(proj_proj_dofmaps[n]) for n in range(pbox.proj_count)])

pelem = evaluable_pbox.loop_index('pelems', pbox.proj_count)

bern_proj_coeffs_array =  evaluable_pbox.Elemwise(data = bern_proj_coeffs_array, index = pelem, dtype = float)
C_proj = evaluable_pbox.Elemwise(data = proj_coef_array, index = pelem, dtype = float)
weight_proj = evaluable_pbox.Elemwise(data = proj_weight_array, index = pelem, dtype = float)

proj_proj_dofmaps =  evaluable_pbox.Elemwise(data = proj_proj_dofmaps_array, index = pelem, dtype = int)

CTC = evaluable_pbox.einsum('ji,jk->ik', C_proj, C_proj)

proj_elem_proj_coef = evaluable_pbox.einsum('ij,jB->Bi', evaluable_pbox.inverse(CTC), bern_proj_coeffs_array)

proj_elem_proj_coef = evaluable_pbox.multiply(weight_proj, proj_elem_proj_coef)

proj_proj_coef = evaluable_pbox.Inflate(proj_elem_proj_coef, proj_proj_dofmaps, evaluable_pbox.asarray(thb_basis.ndofs))

THB_coef = evaluable_pbox.loop_sum(proj_proj_coef, pelem)


THB_coef = sparse.toarray(evaluable_pbox.eval_sparse((THB_coef,))[0]) / weight

# nutils_pbox.elem_size(0)


# print(THB_coef)
# print(weight)

# Collect all dofs that have support on a macroelement for all
# macroelements in `macro_dofs` and a mapping of element index to
# macroelement index in `elem_to_macro`.


topo = pbox.topology
geom = pbox.geometry
thbbasis = pbox.basis('th-spline',degree = pbox.degree)
bernbasis = pbox.basis('discont',degree = pbox.degree)
weights = 'integral'
macroelems = pbox.proj_elem
fun = ns.fun

macro_dofs = []
elem_to_macro = [None] * len(topo)
for ielems in macroelems:
    dofs = [thbbasis.get_dofs(ielem) for ielem in ielems]
    unique = numpy.unique(numpy.concatenate(dofs))
    macro_dofs.append(unique)
    for ielem, dofs in zip(ielems, dofs):
        elem_to_macro[ielem] = numpy.searchsorted(unique, dofs)
        assert (unique[elem_to_macro[ielem]] == dofs).all()

# print(len(elem_to_macro))
# print(elem_to_macro)

# Create a sample on `topo` with gauss quadrature points for exact
# integration of functions of degree `degree`.
smpl = topo.sample('gauss', 4*max(degree)+1)

# We use an evaluable loop to evaluate the projection for each macro
# element. `imacro` is the macro element number.
imacro = evaluable_pbox.loop_index('macro', len(macroelems))

# For each macro element we define an integral by summing integrals over
# the real elements that belong to the macro element. The summation is
# implemented using `loop_sum`. The loop index `imacroelem` is the local
# element index w.r.t. the macroelement. The index `ielem` is the mapping
# of `imacroelem` to the elements w.r.t. `topo`.
ielems = evaluable_pbox.Elemwise(tuple(map(nutils.types.arraydata, macroelems)), imacro, int)
imacroelem = evaluable_pbox.loop_index('elem', ielems.shape[0])
ielem = evaluable_pbox.Take(ielems, imacroelem)
lower_args = smpl.get_lower_args(ielem)
elem_int_weights = smpl.get_evaluable_weights(ielem) * function.jacobian(geom, topo.ndims).lower(lower_args)

elem_int = lambda integrand: evaluable.einsum('A,AB->B', smpl.get_evaluable_weights(ielem) * function.jacobian(geom,topo.ndims).lower(lower_args), integrand)
macro_int = lambda integrand: evaluable.loop_sum(elem_int(integrand), imacroelem)

## elem bezier integrals
# get dofs for each basis type
bern_dofs, bern_basis_coeffs = bernbasis.f_dofs_coeffs(ielem)
thb_dofs, thb_basis_coeffs = thbbasis.f_dofs_coeffs(ielem)

# get macro dofs for THB-splines.
dofs = evaluable_pbox.Elemwise(tuple(map(nutils.types.arraydata, macro_dofs)), imacro, int)
elem_to_macro = evaluable_pbox.Elemwise(tuple(map(nutils.types.arraydata, elem_to_macro)), ielem, int)

# Sample the local basis in the local coordinates. The first axes of
# `shapes` correspond to `weights`, the last axis has length `basis.ndofs`
bern_shapes = evaluable_pbox.Polyval(bern_basis_coeffs, topo.f_coords.lower(lower_args))

# Compute the local mass matrix and right hand side.
mass = elem_int(evaluable_pbox.einsum('Ai,Aj->Aij', bern_shapes, bern_shapes))
rhs = elem_int(evaluable_pbox.einsum('Ai,AB->AiB', bern_shapes, fun.lower(lower_args)))

# Solve the local least squares problem.
local_bern_proj_coeffs = evaluable_pbox.einsum('ij,jB->Bi', evaluable_pbox.inverse(mass), rhs)

# obtain elem Bezier extraction matrix
def CalcC_BP_inv(BernBasis):
    C_BP = numpy.array(BernBasis.get_coefficients(0))

    active_col = [any(C_BP[:, i] != 0) for i in range(C_BP.shape[1])]
    active_row = [i for i in range(C_BP.shape[0])]
    # if C_BP_inv is None:
    C_BP_inv = numpy.zeros(C_BP.shape).transpose()
    C_BP_inv[numpy.ix_(active_col, active_row)] = numpy.linalg.inv(C_BP[:, active_col])

    return C_BP_inv

# inverse of bernbasis monomial extraction matrix
C = evaluable_pbox.constant(CalcC_BP_inv(bernbasis))
# get thb - bezier extraction matrix
elem_bezier_extraction = evaluable_pbox.einsum('ji,ik->kj', thb_basis_coeffs, C)

# print(elem_to_macro)


# for (C^T C)^-1 C^T b, perform C^T b:
elem_thb_pre_projection = evaluable_pbox.einsum('kj,kB->Bj', elem_bezier_extraction, local_bern_proj_coeffs)
elem_thb_pre_projection = evaluable_pbox.Inflate(elem_thb_pre_projection, elem_to_macro, dofs.shape[0])
thb_pre_projection = evaluable_pbox.loop_sum(elem_thb_pre_projection, imacroelem)

# find (C^T C) by summing over elements
elem_CTC = evaluable_pbox.einsum('kj,ki->ji', elem_bezier_extraction, elem_bezier_extraction)
elem_CTC = evaluable_pbox.Inflate(elem_CTC, elem_to_macro, dofs.shape[0])
elem_CTC = evaluable_pbox.einsum('ij->ji', elem_CTC)
elem_CTC = evaluable_pbox.Inflate(elem_CTC, elem_to_macro, dofs.shape[0])
macro_CTC = evaluable_pbox.loop_sum(elem_CTC, imacroelem)

print( macro_CTC.eval(macro=2).shape )
print( numpy.log10(numpy.linalg.cond(macro_CTC.eval(macro=2)) ))


macro_proj_coeffs = evaluable_pbox.einsum('ij,jB->Bi', evaluable_pbox.inverse(macro_CTC), thb_pre_projection)

# quit()


# Multiply the local projection coefficients with the weights for this
# element.

thb_shapes = evaluable_pbox.Polyval(thb_basis_coeffs, topo.f_coords.lower(lower_args))
thb_shapes = evaluable_pbox.Inflate(thb_shapes, elem_to_macro, dofs.shape[0])

if weights == 'uniform':
    weights = evaluable_pbox.ones(thb_shapes.shape[1:])
elif weights == 'integral':
    weights = macro_int(thb_shapes)
else:
    raise ValueError(f'unknown weights method: {weights}, supported: uniform, integral')
macro_proj_coeffs *= evaluable_pbox.prependaxes(weights, macro_proj_coeffs.shape[:-1])

# Scatter the local projection coefficients to the global coefficients ...
proj_coeffs = evaluable_pbox.Inflate(macro_proj_coeffs, dofs, evaluable_pbox.asarray(thbbasis.ndofs))
# ... sum over all elements ...
proj_coeffs = evaluable_pbox.loop_sum(proj_coeffs, imacro)
# ... and normalize by the sum of the weights.
# FIXME: The `.eval()` should not be necessary, but causes an unnecessary
# explicit inflation.
summed_weights = evaluable_pbox.loop_sum(evaluable_pbox.Inflate(weights, dofs, evaluable_pbox.asarray(thbbasis.ndofs)), imacro)
proj_coeffs /= evaluable_pbox.prependaxes(summed_weights.eval(), proj_coeffs.shape[:-1])
# `Inflate` scatters the last axis, but we want this axis to be the first
# axis, so we move the last axis to the front.
proj_coeffs = evaluable_pbox.Transpose.from_end(proj_coeffs, 0)

# Evaluate.
x = sparse.toarray(evaluable_pbox.eval_sparse((proj_coeffs,))[0])
print(x)
# quit()

x = local_projection.project_two_step(topo=pbox.topology,
                                      geom = pbox.geometry,
                                      thbbasis = pbox.basis('th-spline',degree=pbox.degree),
                                      bernbasis = pbox.basis('discont',degree=pbox.degree),
                                      weights = 'integral',
                                      macroelems = pbox.proj_elem,
                                      fun = ns.fun,
                                      degree = pbox.degree)

print(pbox.proj_elem)


# topo: Topology,
#         geom: function.Array,
#         basis: typing.Union[function.Basis],
#         weights: str,
#         macroelems: typing.Sequence[np.ndarray],
#         fun: function.Array,
#         degree: int,
#         arguments = {}) -> np.ndarray:

ns.thbbasis = pbox.basis('th-spline', degree = degree)

pbox.GenerateProjectionElement()

print(pbox.proj_elem)
y = project_onto_basis_macroelemwise(topo = pbox.topology, geom = pbox.geometry, basis = ns.thbbasis, weights="integral", macroelems=pbox.proj_elem, fun=ns.fun, degree=degree)

pbox.project(ns.fun, 'THB')

# plot solution and find maximal error

args = {"bern":x, "thb":y}
ns.add_field('bern', ns.thbbasis)
ns.add_field('thb', ns.thbbasis)
bezier = pbox.topology.sample('bezier', 4 * max(degree))
x, approx, error, thb_error = bezier.eval(['x_i','bern', 'bern - fun', 'thb - fun'] @ ns, **args)
print(f"bern max error over full domain : {max(error)}")
print(f"thb  max error over full domain : {max(thb_error)}")
plt.tripcolor(x[:, 0], x[:, 1], bezier.tri, error, shading='gouraud', rasterized=True)

plt.colorbar()
plt.show()