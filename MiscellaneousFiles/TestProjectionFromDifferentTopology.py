from nutils import mesh
from nutils.expression_v2 import Namespace
import numpy

degree = 3

# topo Construction are intermediate topologies
topoConstuction, geom = mesh.rectilinear([numpy.linspace(0,1,3),]*2)
topoConstuction = topoConstuction.refined_by([1])
topoCoarse = topoConstuction.refined_by([2])
topoRefined = topoCoarse.refined_by([3,9])

ns = Namespace()
ns.x = geom
ns.define_for('x', jacobians=('dV','dS'))

ns.basisRefined = topoRefined.basis('th-spline',degree = (degree,)*2)
ns.basisCoarse = topoCoarse.basis('th-spline',degree = (degree,)*2)
ns.add_field('p',ns.basisRefined)

args1 = {'p' : numpy.ones((len(ns.basisRefined),))}

int1 = topoRefined.integrate('p dV' @ ns, degree = 6 * degree, arguments = args1)
print(int1)
topoRefined.projection(ns.p, ns.basisCoarse, geom, degree = 2 * degree, ischeme = 'gauss', arguments = args1)


# int2 = topoCoarse.integrate('p dV' @ ns, degree = 6 * degree, arguments = args1)


# print( int2)