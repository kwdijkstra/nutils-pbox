from matplotlib import pyplot as plt
from nutils import export





def Plot(topology, degree, ns, plot : str, args):
    bezier = topology.sample('bezier', 4 * max(degree))
    x, plot = bezier.eval(['x_i',plot] @ ns, **args)
    fig, axs = plt.subplots()
    export.triplot(axs, x, plot, tri=bezier.tri, hull=bezier.hull)
    plt.show()


